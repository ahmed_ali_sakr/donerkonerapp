﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Helpers
{
    public static class EnumHelper
    {
        #region Enum Helper
        public static string GetDisplayName(this Enum en)
        {
            if (en == null)
                return "";

            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DisplayAttribute), true);
                DisplayAttribute attr = null;
                if (attrs != null && attrs.Length > 0)
                    attr = attrs[0] as DisplayAttribute;

                if (attr != null)
                    return attr.Name;
            }

            return en.ToString();
        }
        public static string GetText(this Enum en)
        {
            if (en == null)
                return "";

            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DisplayNameAttribute), true);
                DisplayNameAttribute attr = null;
                if (attrs != null && attrs.Length > 0)
                    attr = attrs[0] as DisplayNameAttribute;

                if (attr != null)
                    return attr.DisplayName;
            }

            return en.ToString();
        }
        public static Array GetEnumValues(Type enumType)
        {
            return Enum.GetValues(enumType);
        }
        public static Array GetEnumValues<T>(Type enumType, Predicate<T> predicate) where T : Attribute
        {
            Array arr = GetEnumValues(enumType);
            List<object> list = new List<object>();
            foreach (var en in arr)
            {
                Type type = en.GetType();
                MemberInfo[] memInfo = type.GetMember(en.ToString());
                if (memInfo != null && memInfo.Length > 0)
                {
                    object[] attrs = memInfo[0].GetCustomAttributes(typeof(T), false);
                    T attr = null;
                    if (attrs != null && attrs.Length > 0)
                        attr = attrs[0] as T;

                    if (predicate == null || predicate(attr))
                        list.Add(en);
                }
            }

            return list.ToArray();
        }
        public static IList<SelectListItem> GetEnumList(Type enumType)
        {
            //return EnumHelper.GetSelectList(enumType);
            return GetEnumList(enumType, null, (Attribute t) => true);
        }
        public static IList<SelectListItem> GetEnumList(Type enumType, Enum value)
        {
            //return EnumHelper.GetSelectList(enumType, value);
            return GetEnumList(enumType, value, (Attribute t) => true);
        }
        public static IList<SelectListItem> GetEnumList<T>(Type enumType, Predicate<T> predicate) where T : Attribute
        {
            return GetEnumList<T>(enumType, null, predicate);
        }
        public static IList<SelectListItem> GetEnumList<T>(Type enumType, Enum value, Predicate<T> predicate) where T : Attribute
        {
            Array arr = GetEnumValues<T>(enumType, predicate);
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var en in arr)
            {
                SelectListItem item = new SelectListItem();
                item.Value = string.Format("{0:d}", en);
                item.Text = (en as Enum).GetText();
                item.Selected = en.Equals(value);

                list.Add(item);
            }

            return list;
        }
        public static IList<int> GetIntegerEnumList<T>(Type enumType, Predicate<T> predicate) where T : Attribute
        {
            Array arr = GetEnumValues<T>(enumType, predicate);
            List<int> list = new List<int>();
            foreach (var en in arr)
            {
                list.Add((int)en);
            }

            return list;
        }

        //public static T GetEnumAttributes<T>(Type enumType, string value, int attributeIndex = 0)
        //{
        //    var type = enumType;
        //    var memInfo = type.GetMember(value);
        //    var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
        //    return ((T)attributes[attributeIndex]);
        //}
        public static T GetEnumAttributes<T>(this Enum en, Type enumType, int attributeIndex = 0)
        {
            var type = enumType;
            var memInfo = type.GetMember(en.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return ((T)attributes[attributeIndex]);
        }
        #endregion
    }
}