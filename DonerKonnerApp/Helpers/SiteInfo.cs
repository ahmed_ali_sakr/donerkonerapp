﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;


namespace DonerKonner.Helpers
{
    public static class SiteInfo
    {
        public static HttpSessionState Session { get { return HttpContext.Current.Session; } }

        public static bool IsLogged
        {
            get { return LoggedUserID > 0 && LoggedSecurityRoleID > 0; }
        }


        public static int LoggedUserID
        {
            get { return GetInt("LoggedUserID", -1); }
            set { Session["LoggedUserID"] = value; }
        }
       
        
        public static int LoggedSecurityRoleID
        {
            get { return GetInt("LoggedSecurityRoleID", -1); }
            set { Session["LoggedSecurityRoleID"] = value; }
        }
        public static string LoggedSecurityRoleName
        {
            get { return GetString("LoggedSecurityRoleName"); }
            set { Session["LoggedSecurityRoleName"] = value; }
        }
        public static string LoggedUserName
        {
            get { return GetString("LoggedUserName"); }
            set { Session["LoggedUserName"] = value; }
        }

        public static int LoggedBranchNo
        {
            get { return GetInt("LoggedBranchNo",-1); }
            set { Session["LoggedBranchNo"] = value; }
        }
        public static string LoggedBranchName
        {
            get { return GetString("LoggedBranchName"); }
            set { Session["LoggedBranchName"] = value; }
        }


        public static string LoggedLoginName
        {
            get { return GetString("LoggedLoginName"); }
            set { Session["LoggedLoginName"] = value; }
        }


        #region Methods
        static string GetString(string key)
        {
            try
            {
                return (Session[key]).ToString();
            }
            catch { return null; }
        }

        static int? GetInt(string key)
        {
            try
            {
                return Session[key] as int?;
            }
            catch { return null; }
        }

        static int GetInt(string key, int defaultValue)
        {
            try
            {
                return Convert.ToInt32(Session[key]);
            }
            catch { return defaultValue; }
        }


        static bool GetBool(string key, bool defaultValue)
        {
            try
            {
                return Convert.ToBoolean(Session[key]);
            }
            catch { return defaultValue; }
        }
        #endregion
    }
}