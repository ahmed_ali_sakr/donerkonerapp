﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Admin;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class SecurityRolesController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: SecurityRoles
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");
            return View();
        }
        public ActionResult GetSecurityRole()
        {
            try
            {
                var securityRole = db.securityRoles.Select(a => new
                {
                    SecurityRoleName = a.Name,
                    Notes = a.Notes,
                    SecurityRoleNo = a.ID,
                    IsActive = a.IsActive ? "Active": "Not Active"

                }).ToList();

                return Json(new { data = securityRole }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }
        }

        public ActionResult AddRole()
        {
            ViewBag.isAdd = true;
            var systemFunctions = db.SystemFunctions.OrderBy(s => s.Code).Where(a => a.IsHidden == false && a.ParentID == null).ToList();
            return Json(new { IsValid = false, page = RenderRazorViewToString("_TreePopUp", systemFunctions) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddRole(string Name, string Notes, bool IsActive, List<int> systemfunnctions)
        {
            try
            {
                var SecuriteyRole = new SecurityRole
                {
                    Name = Name,
                    Notes = Notes,
                    IsActive = IsActive,
                    CB = SiteInfo.LoggedUserID,
                    CD = db.GetServerDate()
                };
                db.securityRoles.Add(SecuriteyRole);
                db.SaveChanges();
                var sc = db.securityRoles.Where(s => s.Name == Name).FirstOrDefault();
                for (int i = 0; i <= systemfunnctions.Count; i++)
                {
                    if (systemfunnctions[i] > 0)
                    {
                        var securityRoleFun = new SecurityRoleFunction
                        {
                            SecurityRoleID = sc.ID,
                            CB = SiteInfo.LoggedUserID,
                            CD = db.GetServerDate(),
                            SystemFunctionID = systemfunnctions[i]
                        };
                        db.SecurityRoleFunctions.Add(securityRoleFun);
                        db.SaveChanges();
                    }
                }

                return Json(new { IsValid = false });
            }
            catch (Exception ex)
            {
                return Json(new { IsValid = false });
            }

        }

        public ActionResult GetEditRole(int securityRoleID)
        {
            var securityrole = db.securityRoles.Where(s => s.ID == securityRoleID).FirstOrDefault();
            ViewBag.isAdd = false;
            var securityrolefunc = db.SecurityRoleFunctions.Where(s => s.SecurityRoleID == securityRoleID).Select(a => a.SystemFunctionID).ToList();
            var systemFunctions = db.SystemFunctions.Where(a => a.IsHidden == false && a.ParentID == null).ToList();

            //var selectedsystemfunclist = db.SystemFunctions.Where(s => securityrolefunc.Contains(s.ID)).Select(o => o.ID).ToList();
            var selectedsystemfunclist = db.SystemFunctions.Where(s => securityrolefunc.Contains(s.ID)).ToList();
            var slsfn = selectedsystemfunclist.Where(o => o.ParentID != o.DependOnID ).Select(o => o.ID).ToList();
            ViewBag.list = slsfn;
            return Json(new { data = new { id = securityRoleID, securityrole.Name, securityrole.Notes, securityrole.IsActive }, IsValid = false, page = RenderRazorViewToString("_TreePopUp", systemFunctions) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditRole(string Name, string Notes, bool IsActive, int SecurityRoleId, List<int> systemfunnctions)
        {
            try
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    var securityRole = db.securityRoles.Find(SecurityRoleId);
                    securityRole.ID = SecurityRoleId;
                    securityRole.Name = Name;
                    securityRole.Notes = Notes;
                    securityRole.IsActive = IsActive;
                    db.SaveChanges();
                    var securityRoleFunction = db.SecurityRoleFunctions.Where(x => x.SecurityRoleID == SecurityRoleId).ToList();
                    db.SecurityRoleFunctions.RemoveRange(securityRoleFunction);
                    for (int i = 0; i < systemfunnctions.Count; i++)
                    {
                        if (systemfunnctions[i] > 0)
                        {

                            var securityRoleFun = new SecurityRoleFunction
                            {
                                SecurityRoleID = SecurityRoleId,
                                CB = SiteInfo.LoggedUserID,
                                CD = db.GetServerDate(),
                                SystemFunctionID = systemfunnctions[i]
                            };
                            db.SecurityRoleFunctions.Add(securityRoleFun);
                            db.SaveChanges();
                        }
                    }
                    trans.Commit();
                }
                return Json(new { IsValid = false });
            }
            catch (Exception ex)
            {
                return Json(new { IsValid = false });
            }
        }

        [HttpGet]
        public ActionResult GetDeletePopup(int securityRoleID)
        {
            return Json(new { IsValid = false, page = RenderRazorViewToString("_DeletePopup", null), ID = securityRoleID }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteRole(int securityRoleID)
        {
            try
            {
                SecurityRole securityRoles = db.securityRoles.FirstOrDefault(s => s.ID == securityRoleID);
                db.securityRoles.Remove(securityRoles);
                db.SaveChanges();
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
    }
}