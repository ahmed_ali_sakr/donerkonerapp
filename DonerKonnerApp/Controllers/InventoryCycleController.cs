﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.ViewModel;
using DonerKonner.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class InventoryCycleController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: InventoryCycle
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            ViewBag.DateTime = db.GetServerDate();
            return View();   
        }
        public ActionResult Search(string fromDate, string toDate)
        {
            try
            {
                DateTime fDate = DateTime.Now;
                DateTime tDate = DateTime.Now;
                DateTime.TryParse(fromDate, out fDate);
                DateTime.TryParse(toDate, out tDate);

                var BranchID = SiteInfo.LoggedBranchNo;
                List<InventoryCycleViewModel> inventoryCycleViewModel = db.InventoryCycleReport(fDate, tDate, BranchID);


                return Json(new { data = inventoryCycleViewModel }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InvCyclePrintReport(string FromDate , string ToDate)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            FormCollection formCollection = new FormCollection();
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("Branch_Name", SiteInfo.LoggedBranchName.ToString());
            formCollection.Add("BranchID", SiteInfo.LoggedBranchNo.ToString());
            formCollection.Add("FromDate", FromDate);
            formCollection.Add("ToDate", ToDate);
            Session["parameters"] = formCollection;
            return RedirectToAction("InventoryCycleReport", "Reports");
        }
        public ActionResult ExportToExcel(string FromDate,string ToDate)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            DateTime fromdate;
            DateTime.TryParse(FromDate, out fromdate);
            DateTime todate;
            DateTime.TryParse(ToDate, out todate);
            var BranchID = SiteInfo.LoggedBranchNo;
            List<InventoryCycleViewModel> FileData = db.InventoryCycleReport(fromdate, todate , BranchID).ToList();
            try
            {

                DataTable Dt = new DataTable();
                Dt.Columns.Add("SN", typeof(int));
                Dt.Columns.Add("ItemGroup", typeof(string));
                Dt.Columns.Add("ItemCode", typeof(string));
                Dt.Columns.Add("ItemName", typeof(string));
                Dt.Columns.Add("Opening", typeof(string));
                Dt.Columns.Add("Recieved", typeof(int));
                Dt.Columns.Add("Balance", typeof(int));
                Dt.Columns.Add("Used", typeof(int));
                Dt.Columns.Add("Sold", typeof(int));
                Dt.Columns.Add("Difference", typeof(int));




                int count = 1;
                foreach (var data in FileData)
                {
                    DataRow row = Dt.NewRow();
                    row[0] = count;
                    row[1] = data.ItemGroup;
                    row[2] = data.ItemCode;
                    row[3] = data.ItemName;
                    row[4] = data.Opening;
                    row[5] = data.Recieved;
                    row[6] = data.Balance;
                    row[7] = data.Used;
                    row[8] = data.Sold;
                    row[9] = data.Difference;
                    count++;
                    Dt.Rows.Add(row);

                }
                var memoryStream = new MemoryStream();
                using (var excelPackage = new ExcelPackage(memoryStream))
                {
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Sheet1");

                    count += 6;
                    worksheet.Cells["B6"].LoadFromDataTable(Dt, true);
                    worksheet.DefaultRowHeight = 20;
                    worksheet.Column(1).Width = 1.5;

                    #region Header
                    int RowIndex = 0;
                    int ColIndex = 10;

                    Image img = Image.FromFile(Server.MapPath("\\Images\\DonerKonner.png"));
                    ExcelPicture pic = worksheet.Drawings.AddPicture("DonerKonner.png", img);
                    pic.SetPosition(RowIndex, 0, ColIndex, 10);
                    pic.SetSize(103, 50);

                    var mercell2 = worksheet.Cells["B1:C1"];
                    mercell2.Merge = true;
                    mercell2.Value = "Donner Konner";
                    mercell2.Style.Font.Size = 13;
                    mercell2.Style.Font.Bold = true;
                    mercell2.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                    var mercell3 = worksheet.Cells["D1:H1"];
                    mercell3.Merge = true;
                    mercell3.Value = "Branch Name: " + " " + SiteInfo.LoggedBranchName;
                    mercell3.Style.Font.Size = 13;
                    mercell3.Style.Font.Bold = true;
                    mercell3.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells["B2:K2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells["B2:K2"].Style.Border.Bottom.Color.SetColor(Color.DarkGray);

                    var mercell = worksheet.Cells["D4:H4"];
                    mercell.Merge = true;
                    mercell.Style.Font.Bold = true;
                    mercell.Style.Font.Size = 15;
                    mercell.Value = "Inventory Cycle Report";
                    mercell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    #endregion

                    #region Table
                    worksheet.Cells["B6:K6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                    worksheet.Cells["B6:K6"].Style.Font.Bold = true;
                    worksheet.Cells["B6:K6"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells["B6:K6"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(173, 216, 230));
                    worksheet.Cells["B6:K" + count].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:K" + count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:K" + count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:K" + count].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B7:K" + count].Style.Border.Top.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:K" + count].Style.Border.Bottom.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:K" + count].Style.Border.Right.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:K" + count].Style.Border.Left.Color.SetColor(Color.DarkGray);

                    worksheet.Column(2).Width = 10;
                    worksheet.Column(8).Width = 13;
                    worksheet.Column(9).Width = 13;
                    worksheet.Column(10).Width = 13;
                    worksheet.Column(11).Width = 13;

                    worksheet.Cells["B7:K" + count].Style.Numberformat.Format = "#,###,##";

                    worksheet.Cells["B6:K" + count].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


                    #endregion

                    worksheet.DefaultColWidth = 18;
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();


                    Session["DownloadExcel_InventoryCycleReport"] = excelPackage.GetAsByteArray();
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public ActionResult Download()
        {

            if (Session["DownloadExcel_InventoryCycleReport"] != null)
            {
                byte[] data = Session["DownloadExcel_InventoryCycleReport"] as byte[];
                return File(data, "application/octet-stream", "InventoryCycleReport.xlsx");
            }
            else
            {
                return new EmptyResult();
            }
        }
    }
}