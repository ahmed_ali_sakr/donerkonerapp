﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.ViewModel;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Data;
using System.IO;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing;
using System.Net;
using System.Drawing;

namespace Agi.DonerKonner.Controllers
{

    public class CustodyFollowUpController : BasicCRUDController
    {
        // ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        private DonerKonnerContext db = new DonerKonnerContext();



        // GET: CustodyFollowUp
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            ViewBag.DateTime = db.GetServerDate();
            return View();
        }
        public ActionResult Search(string fromDate, string toDate)
        {
            try
            {
                DateTime fDate = DateTime.Now;
                DateTime tDate = DateTime.Now;
                DateTime.TryParse(fromDate, out fDate);
                DateTime.TryParse(toDate, out tDate);

                var BranchID = SiteInfo.LoggedBranchNo;
                List<CustodyFollowUpReportViewModel> custodyFollowUpReportViewModel = db.CustodyFollowupReport(fDate, tDate, BranchID);


                return Json(new { data = custodyFollowUpReportViewModel }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult CustodyFollowUpReport(string FromDate, string ToDate)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            FormCollection formCollection = new FormCollection();
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("Branch_Name", SiteInfo.LoggedBranchName.ToString());
            formCollection.Add("FromDate", FromDate.ToString());
            formCollection.Add("ToDate", ToDate.ToString());
            formCollection.Add("BranchID", SiteInfo.LoggedBranchNo.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("CustodyFollowUpReport", "Reports");
        }
        [HttpPost]
        public ActionResult ExcelExport(string fromDate, string toDate, string CurBalance, string preBalance, string Payments, string NewCustody)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            DateTime fDate = DateTime.Now;
            DateTime tDate = DateTime.Now;
            DateTime.TryParse(fromDate, out fDate);
            DateTime.TryParse(toDate, out tDate);

            var BranchID = SiteInfo.LoggedBranchNo;
            List<CustodyFollowUpReportViewModel> FileData = db.CustodyFollowupReport(fDate, tDate, BranchID).ToList();
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("SN", typeof(int));
                dt.Columns.Add("date", typeof(string));
                dt.Columns.Add("Custodyitem", typeof(string));
                dt.Columns.Add("Amount", typeof(double));
                dt.Columns.Add("Discription", typeof(string));
                int counter = 1;
                foreach (var data in FileData)
                {
                    DataRow row = dt.NewRow();
                    row[0] = counter;
                    row[1] = data.TrnsDate.ToString("dd/MM/yyyy");
                    row[2] = data.CustodyItem;
                    row[3] = data.CurrentAmount;
                    row[4] = data.Description;
                    counter++;
                    dt.Rows.Add(row);
                }
                var memoryStream = new MemoryStream();
                using (var excelPackage = new ExcelPackage(memoryStream))
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                    //header
                    // ExcelRange Range;

                    counter += 5;

                    //get the image from disk
                    using (System.Drawing.Image image =
                    System.Drawing.Image.FromFile(Server.MapPath("~/Images/DonerKonner.png")))
                    {
                        var excelImage = worksheet.Drawings.AddPicture("DonerKonner.png", image);

                        excelImage.SetPosition(0, 3, 6, 9);
                        excelImage.SetSize(100, 40);
                    }
                    worksheet.Cells["G1:G2"].Merge = true;
                    worksheet.Cells["G1:G2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    worksheet.Cells["B1:C1"].Merge = true;
                    worksheet.Cells["B1:C1"].Value = "Doner Konner";
                    worksheet.Cells["B1:C1"].Style.Font.Bold = true;
                    worksheet.Cells["B1:C1"].Style.Font.Size = 14;
                    worksheet.Cells["D1:E1"].Merge = true;
                    worksheet.Cells["D1:E1"].Value = "Branch Name : " + SiteInfo.LoggedBranchName;
                    worksheet.Cells["D1:E1"].Style.Font.Bold = true;
                    worksheet.Cells["D1:E1"].Style.Font.Size = 14;
                    worksheet.Cells["D3:E3"].Merge = true;
                    worksheet.Cells["D3:E3"].Value = "Custody FollowUp Report ";
                    worksheet.Cells["D3:E3"].Style.Font.Size = 14;
                    worksheet.Cells["D3:E3"].Style.Font.Bold = true;
                    worksheet.Cells["B2:G2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                    //table
                    worksheet.Cells["B5"].LoadFromDataTable(dt, true);
                    worksheet.DefaultRowHeight = 18;
                    worksheet.DefaultColWidth = 19;
                    double smalwidth = 1.5;
                    worksheet.Column(1).Width = smalwidth;
                    double Snwidth = 4.71;
                    worksheet.Column(2).Width = Snwidth;
                    double disWidth = 30;
                    worksheet.Column(7).Width = disWidth;
                    double fwidt = 20;
                    worksheet.Column(6).Width = fwidt;

                    worksheet.Cells["F5:G5"].Merge = true;
                    worksheet.Cells["B5:BD5"].Style.Font.Bold = true;
                    worksheet.Cells["E6:E" + counter].Style.Numberformat.Format = "$#,###,##";
                    worksheet.Cells["B5:G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["B5:G5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(173, 216, 230));
                    worksheet.Cells["B5:G" + counter].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells["B5:G" + counter].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells["B5:G" + counter].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells["B5:G" + counter].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells["G6:G" + counter].Style.Border.Left.Style = ExcelBorderStyle.None;
                    worksheet.Cells["F6:F" + counter].Style.Border.Right.Style = ExcelBorderStyle.None;
                    worksheet.Column(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(4).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(5).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(7).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    // table summery
                    counter += 2;
                    worksheet.Cells["B" + counter + ":G" + (counter + 1)].Merge = true;
                    worksheet.Cells["B" + counter + ":G" + (counter + 1)].Value =
                        "Privious Balance: " + preBalance +
                        "           New Custody: " + NewCustody +
                        "           Payments: " + Payments +
                        "           Currunt Balance: " + CurBalance

                        ;
                    worksheet.Cells["B" + counter + ":G" + (counter + 1)].Style.Border.BorderAround(ExcelBorderStyle.MediumDashDot, Color.FromArgb(173, 216, 230));
                    worksheet.Cells["B" + counter + ":G" + (counter + 1)].Style.Font.Bold = true;
                    worksheet.Cells["B" + counter + ":G" + (counter + 1)].Style.Font.Size = 12;
                    worksheet.Cells["B" + counter + ":G" + (counter + 1)].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells["B" + counter + ":G" + (counter + 1)].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;




                    //footer

                    using (System.Drawing.Image image =
                    System.Drawing.Image.FromFile(Server.MapPath("~/Images/agi_Logo.png")))
                    {
                        var excelImage = worksheet.Drawings.AddPicture("agi_Logo.png", image);

                        excelImage.SetPosition(counter + 1, counter + 1, 1, 1);
                        excelImage.SetSize(40, 20);
                    }

                    worksheet.Cells["C" + (counter + 2) + ":G" + (counter + 2)].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;

                    worksheet.Cells["B" + (counter + 3) + ":D" + (counter + 3)].Merge = true;

                    worksheet.Cells["B" + (counter + 3) + ":D" + (counter + 3)].Value = "Printed by:" + SiteInfo.LoggedUserName;
                    worksheet.Cells["B" + (counter + 3) + ":D" + (counter + 3)].Style.Font.Size = 10;
                    worksheet.Cells["B" + (counter + 3) + ":D" + (counter + 3)].Style.Font.Bold = true;

                    worksheet.Cells["G" + (counter + 3)].Value = "Printed on: " + db.GetServerDate();
                    worksheet.Cells["G" + (counter + 3)].Style.Font.Size = 10;
                    worksheet.Cells["G" + (counter + 3)].Style.Font.Bold = true;















                    Session["DownloadExcel_CustodyFollowUpReport"] = excelPackage.GetAsByteArray();
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public ActionResult Download()
        {

            if (Session["DownloadExcel_CustodyFollowUpReport"] != null)
            {
                byte[] data = Session["DownloadExcel_CustodyFollowUpReport"] as byte[];
                return File(data, "application/octet-stream", "CustodyFollowUpReport.xlsx");
            }
            else
            {
                return new EmptyResult();
            }
        }

    }
}