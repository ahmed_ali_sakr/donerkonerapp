﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Custody;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class DeliveryController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Delivery
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            CustodyDelivey Cdelivery = new CustodyDelivey();
            var serverdate = db.GetServerDate();
            Cdelivery.Date = serverdate;
            return View(Cdelivery);
        }
        public ActionResult GetCDelivery()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            
            try
            {
                var CDeliveries = db.CustodyDeliveys.Select(a => new
                {
                    CDeliveryid = a.ID,
                    CDeliveryDate = a.Date,
                    CDeliveryAmount = a.Amount,
                    CDeliveryBranchID=a.BranchID,
                    CDeliveryBranchName= a.Branch,
                    Notes = a.Notes

                }).ToList().Where(x => x.CDeliveryBranchID == SiteInfo.LoggedBranchNo).Select(a=> new  {
                    CDeliveryid = a.CDeliveryid,
                    CDeliveryDate = a.CDeliveryDate.ToString("dd/MM/yyyy"),
                    CDeliveryAmount = a.CDeliveryAmount,
                    CDeliveryBranchID=a.CDeliveryBranchID,
                    CDeliveryBranchName=a.CDeliveryBranchName,
                    Notes = a.Notes
                });

                return Json(new { data = CDeliveries } , JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }
        public ActionResult Save(int CDeliveryid, DateTime CDeliveryDate, decimal CDeliveryAmount, String CDeliveryNote)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.CustodyDeliveys.Any(u => u.ID == CDeliveryid))
                {
                    CustodyDelivey custodyDelivey = db.CustodyDeliveys.FirstOrDefault(u => u.ID == CDeliveryid);
                    custodyDelivey.Date = CDeliveryDate;
                    custodyDelivey.Amount = CDeliveryAmount;
                    custodyDelivey.Notes = CDeliveryNote;
                    custodyDelivey.MB = SiteInfo.LoggedUserID;
                    custodyDelivey.BranchID = SiteInfo.LoggedBranchNo;
                    custodyDelivey.MD = db.GetServerDate();
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    CustodyDelivey custodyDelivey = new CustodyDelivey
                    {
                        Date = CDeliveryDate,
                        Amount = CDeliveryAmount,
                        Notes = CDeliveryNote,
                        CB = SiteInfo.LoggedUserID,
                        BranchID = SiteInfo.LoggedBranchNo,
                        CD = db.GetServerDate()

                    };
                    db.CustodyDeliveys.Add(custodyDelivey);
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult EditCDelivery(int CDeliveryid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                var custodyDelivey = db.CustodyDeliveys.FirstOrDefault(u => u.ID == CDeliveryid);

                return Json(new { data = new { custodyDelivey.ID, Date = custodyDelivey.Date.ToString("dd/MM/yyyy") , custodyDelivey.Amount, custodyDelivey.Notes } }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult DeleteCDelivery(int CDeliveryid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                CustodyDelivey custodyDelivey = db.CustodyDeliveys.FirstOrDefault(a => a.ID == CDeliveryid);
                db.CustodyDeliveys.Remove(custodyDelivey);
                db.SaveChanges();
                return Json(new { data = custodyDelivey }, JsonRequestBehavior.AllowGet);

            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }
        public ActionResult CustodyDeliveryReport(FormCollection formCollection)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("CustodyDeliveryReport", "Reports");
        }
    }
}