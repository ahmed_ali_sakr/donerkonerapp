﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Enum;
using Agi.DonerKonner.DataAccess.Inventory;
using Agi.DonerKonner.DataAccess.ViewModel;
using Agi.DonerKonner.Globalization;
using Agi.DonerKonner.Helpers;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Agi.DonerKonner.Controllers
{
    public class ItemsController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();

        // GET: Items
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            List<Item> items = db.Items.ToList();
            return View(items);
        }


        public ActionResult getitems(int? HasStock, int? IsBundle, int? ISRecpy, int? IsDummy)
        {
            try
            {
                if (!SiteInfo.IsLogged)
                    return RedirectToAction("Login", "Home");
                var items = db.Items.ToList()
                    .Where(a =>
                    (HasStock == -1 || (HasStock == 1 ? a.HasStock==true : a.HasStock==false)) &&
                   ( IsBundle == -1 || (IsBundle == 1 ? a.IsBundle == true : a.IsBundle == false) )&&
                    (ISRecpy == -1 || (ISRecpy == 1 ? a.IsPOSItem == true : a.IsPOSItem == false) )&&
                    (IsDummy == -1 || (IsDummy == 1 ? a.IsDummy == true : a.IsDummy == false)))
                    .Select(a => new
                    {
                        ID = a.ID,
                        Code = a.Code,
                        ItemName = a.Name,
                        Group = a.ItemGroup.Name,
                        UnitName = a.Unit.Name,
                        Notes = a.Notes,
                        ISActive = a.IsActive,
                        Category=((CategoryEnum)a.CategoryItemId).GetText()
                    });
                return Json(new { data = items }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult DeleteItem(int ID)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                Item item = db.Items.Remove(db.Items.FirstOrDefault(a => a.ID == ID));
                db.SaveChanges();
                return Json(new { data = item }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public void BindDlls(Item item)
        {
            var CategoryID = EnumHelper.GetEnumList(typeof(CategoryEnum));
            if (item.ID != 0)
            {
                ViewBag.UnitID = new SelectList(db.Units.ToList(), "ID", "Name", item.UnitID);
                ViewBag.GroupIDMST = new SelectList(db.ItemGroups.ToList(), "ID", "Name", item.ItemGroupID);
                ViewBag.CategoryID = new SelectList(CategoryID, "Value", "Text", item.CategoryItemId);
            }
            else
            {
                ViewBag.UnitID = new SelectList(db.Units.ToList(), "ID", "Name", null);
                ViewBag.GroupIDMST = new SelectList(db.ItemGroups.ToList(), "ID", "Name", null);
                 Session["GridData"] = new List<GridData>();
                ViewBag.CategoryID = new SelectList(CategoryID, "Value", "Text", null);
            }
            ViewBag.GroupID = new SelectList(db.ItemGroups.ToList(), "ID", "Name", null);
        }

        public ActionResult AddItem()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            ViewBag.title = Strings.Add + " " + Strings.Item;
            ViewBag.pageheader = Strings.Homw + " > " + Strings.Item + " > " + Strings.Add + " " + Strings.Item;

            BindDlls(new Item { IsActive = true });
            return View(new Item { IsActive = true });
        }

        public ActionResult BindItemsDll(int? groupId, bool ISComponent, bool PostItem)
        {
            try
            {
                List<Item> list;
                if (ISComponent)
                    list = db.Items.Where(o => o.ItemGroupID == groupId && o.IsDummy == false && o.IsBundle == false).ToList();
                else
                    list = db.Items.Where(o => o.ItemGroupID == groupId && o.HasStock == true && o.IsDummy == false && o.IsBundle == false && o.IsPOSItem == false).ToList();

                //return Json(list);
                return Json(new { data = new SelectList(list, "Code", "Name", null) }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult BindItemsDataDll(string itemId)
        {
            
            try
            {
                if (itemId == null)
                    return Json(new { isValid = false });
                string unit = db.Items.FirstOrDefault(a => a.Code == itemId).Unit.Name;
                string name = db.Items.FirstOrDefault(a => a.Code == itemId).Name;
                string code = db.Items.FirstOrDefault(a => a.Code == itemId).Code;
                //var list = db.Units.Where(o => o.ID == db.Items.FirstOrDefault(a=>a.ID==itemId).UnitID).ToList();

                //return Json(list);
                return Json(new { code, name, unit }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult BindAddToGrid(int groupId, string itemCode, string ItemName, decimal Quantity, string Notes)
        {
            
            try
            {
                List<GridData> l = new List<GridData>();
                if (Session["GridData"] == null || ((List<GridData>)Session["GridData"]).Count < 1)
                {
                    l.Add(new GridData
                    {
                        GroupID = groupId,
                        ItemCode = itemCode,
                        ItemName = ItemName,

                        Quantity = Quantity,
                        Notes = Notes
                    });
                    Session["GridData"] = l;
                }
                else
                {
                    l = (List<GridData>)Session["GridData"];
                    if (!l.Select(i => i.ItemCode).Contains(itemCode))
                    {
                        l.Add(new GridData
                        {
                            GroupID = groupId,
                            ItemCode = itemCode,
                            ItemName = ItemName,
                            Quantity = Quantity,
                            Notes = Notes
                        });
                    }
                    else
                    {
                        foreach (var item in l)
                        {
                            if (item.ItemCode == itemCode)
                            {
                                item.Quantity = Quantity;
                                item.Notes = Notes;
                            }
                        }


                    }
                    Session["GridData"] = l;
                }

                return Json(new { data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult fillDaataTable()
        {
           
            if (Session["GridData"] == null)
            {
                return Json(new { data = new List<GridData>() }, JsonRequestBehavior.AllowGet);
            }

            List<GridData> l = (List<GridData>)Session["GridData"];
            return Json(new { data = l }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Save(string Code, string Name, int GroupID, int UnitID, bool HasStock, bool ISActive, bool ISComponent, bool PostItem, bool Dummy, string Notes,int Category)
        {
            
            try
            {
                var vv = ModelState.IsValid;

                using (var trans = db.Database.BeginTransaction())
                {
                    Item item;
                    if (db.Items.Any(async => async.Code == Code))
                    {
                        List<GridData> list = (List<GridData>)Session["GridData"];
                        item = db.Items.FirstOrDefault(async => async.Code == Code);
                        if (item.IsBundle)
                            db.ItemComponents.RemoveRange(db.ItemComponents.Where(a => a.ItemID == item.ID));
                        //db.ItemComponents.RemoveRange(db.ItemComponents.Where(a => a.ComponentID ==
                        // db.ItemComponents.FirstOrDefault(b => b.ComponentID == db.Items.FirstOrDefault(c => list.Select(l => l.ItemCode).Contains(c.Code)).ID).ID
                        //));
                        if (item.IsPOSItem)
                            db.ItemRecipes.RemoveRange(db.ItemRecipes.Where(a => a.ItemID == item.ID));
                        item.UnitID = UnitID;
                        item.IsActive = ISActive;
                        item.HasStock = HasStock;
                        item.IsBundle = ISComponent;
                        item.IsPOSItem = PostItem;
                        item.IsDummy = Dummy;
                        item.CategoryItemId=Category;
                        item.MB = SiteInfo.LoggedUserID;
                        item.MD = DateTime.Now;
                        item.Notes = Notes;

                        db.SaveChanges();


                        if (ISComponent)
                        {
                            List<ItemComponent> itemComponents = new List<ItemComponent>();
                            foreach (var l in list)
                            {
                                //if (db.ItemComponents.Any(b => b.ComponentID == db.Items.FirstOrDefault(a => a.Code == l.ItemCode).ID))
                                //{
                                //    ItemComponent itemComponent = db.ItemComponents.FirstOrDefault(b => b.ComponentID == db.Items.FirstOrDefault(a => a.Code == l.ItemCode).ID);
                                //    itemComponent.Qty = (int)l.Quantity;
                                //    itemComponent.Notes = l.Notes;
                                //    itemComponent.MB = SiteInfo.LoggedUserID;
                                //    itemComponent.MD = DateTime.Now;
                                //    db.SaveChanges();
                                //}
                                //else
                                //{
                                ItemComponent itemComponent = new ItemComponent
                                {
                                    ItemID = item.ID,
                                    ComponentID = db.Items.FirstOrDefault(a => a.Code == l.ItemCode).ID,
                                    Qty = (int)l.Quantity,
                                    Notes = l.Notes,
                                    
                                    CB = SiteInfo.LoggedUserID,
                                    CD = DateTime.Now,
                                };
                                itemComponents.Add(itemComponent);
                                //}

                            }
                            db.ItemComponents.AddRange(itemComponents);
                            db.SaveChanges();
                        }
                        else if (PostItem)
                        {
                            List<ItemRecipe> itemRecipes = new List<ItemRecipe>();
                            foreach (var l in list)
                            {
                                ItemRecipe itemRecipe = new ItemRecipe
                                {
                                    ItemID = item.ID,
                                    RecipeID = db.Items.FirstOrDefault(a => a.Code == l.ItemCode).ID,
                                    Qty = (int)l.Quantity,
                                    Notes = l.Notes,
                                    CB = SiteInfo.LoggedUserID,
                                    CD = DateTime.Now,
                                };
                                itemRecipes.Add(itemRecipe);
                            }
                            db.ItemRecipes.AddRange(itemRecipes);
                            db.SaveChanges();


                        }
                    }
                    else
                    {

                        item = new Item
                        {
                            Code = Code,
                            Name = Name,
                            ItemGroupID = GroupID,
                            UnitID = UnitID,
                            IsActive = ISActive,
                            HasStock = HasStock,
                            IsBundle = ISComponent,
                            IsPOSItem = PostItem,
                            IsDummy = Dummy,
                            CB = SiteInfo.LoggedUserID,
                            CD = DateTime.Now,
                            Notes = Notes,
                            CategoryItemId = Category,
                            
                        };
                        db.Items.Add(item);
                        db.SaveChanges();
                        List<GridData> list = (List<GridData>)Session["GridData"];

                        if (ISComponent)
                        {
                            List<ItemComponent> itemComponents = new List<ItemComponent>();
                            foreach (var l in list)
                            {
                                ItemComponent itemComponent = new ItemComponent
                                {
                                    ItemID = item.ID,
                                    ComponentID = db.Items.FirstOrDefault(a => a.Code == l.ItemCode).ID,
                                    Qty = (int)l.Quantity,
                                    Notes = l.Notes,
                                    CB = SiteInfo.LoggedUserID,
                                    CD = DateTime.Now,
                                };
                                itemComponents.Add(itemComponent);
                            }
                            db.ItemComponents.AddRange(itemComponents);
                            db.SaveChanges();
                        }
                        else if (PostItem)
                        {
                            List<ItemRecipe> itemRecipes = new List<ItemRecipe>();
                            foreach (var l in list)
                            {
                                ItemRecipe itemRecipe = new ItemRecipe
                                {
                                    ItemID = item.ID,
                                    RecipeID = db.Items.FirstOrDefault(a => a.Code == l.ItemCode).ID,
                                    Qty = (int)l.Quantity,
                                    Notes = l.Notes,
                                    CB = SiteInfo.LoggedUserID,
                                    CD = DateTime.Now,
                                };
                                itemRecipes.Add(itemRecipe);
                            }
                            db.ItemRecipes.AddRange(itemRecipes);
                            db.SaveChanges();



                        }
                    }


                    trans.Commit();
                    Session["GridData"] = null;
                    return Json(new { data = item }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult UpdateitemsGrid(string ItemID)
        {
            
            try
            {
                if (Session["GridData"] != null)
                {
                    GridData gridDatas = ((List<GridData>)Session["GridData"]).FirstOrDefault(it => it.ItemCode == ItemID);
                    ViewBag.GroupID = new SelectList(db.ItemGroups.ToList(), "ID", "Name", null);
                    return Json(new { data = gridDatas }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {

            }
            return Json(new { isValid = false });
        }

        public void DeleteFromGrid(string ItemID)
        {
            if (Session["GridData"] != null)
            {
                List<GridData> gridDatas = (List<GridData>)Session["GridData"];
                gridDatas.Remove(gridDatas.FirstOrDefault(it => it.ItemCode == ItemID));
                Session["GridData"] = gridDatas;
            }
        }
        public ActionResult UpdateTrans(int Transno)
        {
            
            try
            {
                InvOpening v = db.InvOpenings.FirstOrDefault(a => a.TransNo == Transno);
                List<InvOpeningItem> list = db.InvOpeningItems.Where(a => a.InvOpeningID == v.ID).ToList();
                List<GridData> l = new List<GridData>();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        var result = db.Items.FirstOrDefault(a => a.ID == item.ItemID);
                        l.Add(new GridData
                        {
                            GroupID = result.ItemGroupID,
                            ItemCode = result.Code,
                            ItemName = item.Item.Name,
                            Price = item.UnitPrice,
                            Unit = item.Unit.Name,
                            Quantity = item.Qty,
                            Notes = item.Notes,
                        });
                    }
                    Session["InvOpenningGridData"] = l;
                }


                return Json(new { isValid = true });
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult UpdateItem(int ID)
        {
            
            try
            {
                Session["GridData"] = null;
                Item itm = db.Items.FirstOrDefault(a => a.ID == ID);
                if (itm.IsBundle)
                {
                    List<ItemComponent> list = db.ItemComponents.Where(a => a.ItemID == ID).ToList();
                    List<GridData> l = new List<GridData>();
                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            var result = db.Items.FirstOrDefault(a => a.ID == item.ItemID);
                            var v = db.Items.FirstOrDefault(a => a.ID == item.ComponentID);
                            l.Add(new GridData
                            {
                                GroupID = v.ItemGroup.ID,
                                ItemCode = v.Code,
                                ItemName = v.Name,
                                Quantity = item.Qty,
                                Notes = item.Notes,
                            });
                        }
                        Session["GridData"] = l;
                    }
                }
                else if (itm.IsPOSItem)
                {
                    List<ItemRecipe> list = db.ItemRecipes.Where(a => a.ItemID == ID).ToList();
                    List<GridData> l = new List<GridData>();
                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            var result = db.Items.FirstOrDefault(a => a.ID == item.ItemID);
                            var v = db.Items.FirstOrDefault(a => a.ID == item.RecipeID);
                            l.Add(new GridData
                            {
                                GroupID = v.ItemGroup.ID,
                                ItemCode = v.Code,
                                ItemName = v.Name,
                                Quantity = item.Qty,
                                Notes = item.Notes,
                            });
                        }
                        Session["GridData"] = l;
                    }
                }



                return Json(new { isValid = true });
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult UpdateItwmView()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            Item item;
            int itemID = int.Parse(Request.QueryString["itemID"].ToString());
            item = db.Items.FirstOrDefault(a => a.ID == itemID);
            ViewBag.title = Strings.Edit + " " + Strings.Item;
            ViewBag.pageheader = Strings.Homw + " > " + Strings.Item + " > " + Strings.Edit + " " + Strings.Item;



            BindDlls(item);
            return View("AddItem", item);
        }

        public ActionResult resetSession(string type)
        {
            try
            {
                Session["GridData"] = null;
                return Json(new { isValid = true });
            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult getCode(int GroupID)
        {
            try
            {
                string groupCode = db.ItemGroups.FirstOrDefault(a => a.ID == GroupID).Code;
                string itemCode;
                if (db.Items.Where(a => a.Code.StartsWith(groupCode)).Count() == 0)
                    itemCode = groupCode + "-001";
                else
                {
                    var items = db.Items.Where(a => a.Code.StartsWith(groupCode)).ToList();
                    itemCode = groupCode + "-" + (items.Max(a => int.Parse(a.Code.Replace(groupCode + "-", ""))) + 1).ToString().PadLeft(3, '0');
                }

                //string itemCode = db.Items.Where(a => a.Code.StartsWith(groupCode)).Count() == 0 ? groupCode + "-001" : groupCode +( db.Items.Where(a => a.Code.StartsWith(groupCode)).Max(a => int.Parse(a.Code.Replace(groupCode + "-", ""))) + 1).ToString().PadLeft(3,'0');
                return Json(new { itemCode });
            }
            catch (Exception ex)
            {
                return Json(new { isValid = true });
            }
        }

        public ActionResult checkeditcolumn(int itemId)
        {
            try
            {
                bool enStock, enBundle, enRecpe, enDummy;
                enStock = enBundle = enRecpe = enDummy = false;
                Item item = db.Items.FirstOrDefault(a => a.ID == itemId);
                if (db.ItemComponents.Any(a => a.ComponentID == item.ID))
                {
                    enDummy = enBundle = true;
                }
                if (db.ItemRecipes.Any(a => a.RecipeID == item.ID))
                {
                    enDummy = enBundle = enRecpe = enStock = true;
                }
                if (db.InvOpeningItems.Any(a => a.ItemID == item.ID))
                {
                   enStock = true;
                }

                var data = new { enStock = enStock, enBundle = enBundle, enRecpe = enRecpe, enDummy = enDummy };
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isValid = true });
            }
        }
        public ActionResult TransReport(FormCollection formCollection, int ItemID)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("ItemID", ItemID.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("ItemData", "Reports");
        }
    }
}