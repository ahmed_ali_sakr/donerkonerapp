﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Custody;
using Agi.DonerKonner.DataAccess.ViewModel;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;




namespace Agi.DonerKonner.Controllers
{
    public class PaymentController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Payment
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            return View();
        }
        public ActionResult ExpensesList()
        {
            var data = (from ce in db.CustodyExpensess
                        join csi in db.CustodyExpensesItems on ce.ID equals csi.CustodyExpensesID
                        group ce by new { ce.ID, ce.Date, ce.Notes, ce.BranchID } into grouped
                        select new
                        {
                            ID = grouped.Key.ID,
                            Date = grouped.Key.Date,
                            Count = grouped.Count(),
                            Notes = grouped.Key.Notes,
                            BranchID = grouped.Key.BranchID,

                        }).ToList().Where(d => d.BranchID == SiteInfo.LoggedBranchNo).Select(a => new
                        {
                            ID = a.ID,
                            Date = a.Date.ToString("dd/MM/yyyy"),
                            Count = a.Count,
                            Notes = a.Notes
                        });
            return Json(new { data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEXCustody()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            ViewBag.isAdd = true;

            ViewBag.ExPDate = db.GetServerDate();
            return View("ExpensesItem");
        }

        [HttpPost]
        public ActionResult AddEXCustody(Boolean AddandNew, DateTime ExPDate, string ExNotes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.CustodyExpensess.Any(it => it.Date == ExPDate && it.BranchID == SiteInfo.LoggedBranchNo) || ExPDate > db.GetServerDate())
                {

                    return Json(new { IsValid = false, message = "Date is InValid" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    CustodyExpenses custodyExpenses = new CustodyExpenses
                    {
                        BranchID = SiteInfo.LoggedBranchNo,
                        Date = ExPDate,
                        Notes = ExNotes,
                        CB = SiteInfo.LoggedUserID,
                        CD = db.GetServerDate()

                    };
                    db.CustodyExpensess.Add(custodyExpenses);
                    db.SaveChanges();
                    var CExpenses = db.CustodyExpensess.FirstOrDefault(i => i.Date == ExPDate);
                    List<Expenses> list = (List<Expenses>)Session["ExCustodyGridData"];
                    List<CustodyExpensesItem> custodyExpensesItems = new List<CustodyExpensesItem>();
                    foreach (var item in list)
                    {
                        custodyExpensesItems.Add(new CustodyExpensesItem
                        {

                            CustodyExpensesID = CExpenses.ID,
                            CustodyItemID = item.CustodyId,
                            Amount = item.Amount,
                            Notes = item.Notes,
                            CB = SiteInfo.LoggedUserID,
                            CD = db.GetServerDate()

                        });

                    }
                    db.CustodyExpensesItems.AddRange(custodyExpensesItems);
                    db.SaveChanges();
                    Session["ExCustodyGridData"] = null;

                    if (AddandNew)
                    {
                        ViewBag.ExPDate = db.GetServerDate();

                        return Json(new { isValid = true });
                    }
                    else
                    {
                        return Json(new { isValid = true });
                    }

                }
            }
            catch (Exception ex)
            {
                return Json(new { isValid = true });
            }

        }

        public ActionResult EditEXCustody(int ID)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                ViewBag.isAdd = false;
                CustodyExpenses custodyExpenses;

                custodyExpenses = db.CustodyExpensess.FirstOrDefault(a => a.ID == ID);
                ViewBag.ExPDate = custodyExpenses.Date;
                List<CustodyExpensesItem> list = db.CustodyExpensesItems.Where(a => a.CustodyExpensesID == custodyExpenses.ID).ToList();
                List<Expenses> itemList = new List<Expenses>();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        Guid guid = Guid.NewGuid();
                        String guidst = guid.ToString();
                        itemList.Add(new Expenses
                        {
                            Id = guidst,
                            Amount = item.Amount,
                            CustodyName = item.CustodyItem.Name,
                            CustodyId = item.CustodyItem.ID,
                            Notes = item.Notes
                        });
                    }
                    Session["ExCustodyGridData"] = itemList;
                }

                ViewBag.AddandNew = false;
                return View("ExpensesItem", custodyExpenses);
            }
            catch (Exception ex)
            {
                return View("ExpensesItem");
            }

        }
        [HttpPost]
        public ActionResult EditEXCustody(int Id, Boolean AddandNew, DateTime ExPDate, string ExNotes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.CustodyExpensess.Any(it => it.Date == ExPDate && it.BranchID == SiteInfo.LoggedBranchNo) || ExPDate > db.GetServerDate())
                {
                    CustodyExpenses custodyExpenses1 = db.CustodyExpensess.Where(it => it.Date == ExPDate).FirstOrDefault();
                    if (custodyExpenses1.ID != Id)
                    {
                        return Json(new { IsValid = false, message = "Date is InValid" }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        CustodyExpenses custodyExpenses = db.CustodyExpensess.Where(it => it.Date == ExPDate).FirstOrDefault();
                        custodyExpenses.BranchID = SiteInfo.LoggedBranchNo;
                        custodyExpenses.Date = ExPDate;
                        custodyExpenses.Notes = ExNotes;
                        custodyExpenses.MB = SiteInfo.LoggedUserID;
                        custodyExpenses.MD = db.GetServerDate();

                        db.CustodyExpensesItems.RemoveRange(db.CustodyExpensesItems.Where(it => it.CustodyExpensesID == custodyExpenses.ID));

                        db.SaveChanges();

                        List<Expenses> list = (List<Expenses>)Session["ExCustodyGridData"];
                        List<CustodyExpensesItem> custodyExpensesItems = new List<CustodyExpensesItem>();
                        foreach (var item in list)
                        {
                            custodyExpensesItems.Add(new CustodyExpensesItem
                            {

                                CustodyExpensesID = custodyExpenses.ID,
                                Amount = item.Amount,
                                CustodyItemID = item.CustodyId,
                                Notes = item.Notes,
                                CB = SiteInfo.LoggedUserID,
                                CD = db.GetServerDate()

                            });

                        }
                        db.CustodyExpensesItems.AddRange(custodyExpensesItems);
                        db.SaveChanges();
                        Session["ExCustodyGridData"] = null;

                        if (AddandNew)
                        {
                            ViewBag.ExPDate = db.GetServerDate();

                            return Json(new { isValid = true });
                        }
                        else
                        {
                            return Json(new { isValid = true });
                        }
                    }
                }


                return Json(new { isValid = true });
            }
            catch (Exception ex)
            {
                return Json(new { isValid = true });
            }



        }

        public ActionResult Additem()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }

            ViewBag.Citems = new SelectList(db.CustodyItems.ToList(), "ID", "Name");
            ViewBag.iscreate = true;
            Guid guid = Guid.NewGuid();
            String guidst = guid.ToString();
            ViewBag.idval = guidst;

            return Json(new { IsValid = false, page = RenderRazorViewToString("AEpopup", null) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddItem(string Id, Boolean saveandnewFlag, int CustodyId, string CustodyName, decimal Amount, string Notes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }

            try
            {
                List<Expenses> itemlist = (List<Expenses>)Session["ExCustodyGridData"];
                itemlist.Add(new Expenses
                {
                    Id = Id,
                    Amount = Amount,
                    CustodyId = CustodyId,
                    CustodyName = CustodyName,
                    Notes = Notes,
                });

                Session["ExCustodyGridData"] = itemlist;
                ViewBag.Citems = new SelectList(db.CustodyItems.ToList(), "ID", "Name");
                return Json(new { IsValid = true, saveandnewFlag }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { IsValid = false, saveandnewFlag }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult Edititem(string Id)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))


                Id = (Id ?? "").Trim().ToLower();
            ViewBag.Citems = new SelectList(db.CustodyItems.ToList(), "ID", "Name");
            ViewBag.iscreate = false;
            Expenses expenses = ((List<Expenses>)Session["ExCustodyGridData"]).Where(it => !string.IsNullOrWhiteSpace(Id) && (it.Id ?? "").Trim().ToLower() == Id).FirstOrDefault();
            ViewBag.idval = Id;
            decimal Amount = expenses.Amount;
            string CustodyName = expenses.CustodyName.ToString();
            int CustodyId = expenses.CustodyId;
            string notes = expenses.Notes.ToString();

            return Json(new { IsValid = false, Amount, CustodyName, CustodyId, notes, page = RenderRazorViewToString("AEpopup", null) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edititem(string Id, Boolean saveandnewFlag, int CustodyId, string CustodyName, decimal Amount, string Notes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }


            if (saveandnewFlag && ((List<Expenses>)Session["ExCustodyGridData"]).Any(it => it.Id == Id))
            {
                Expenses expenses = ((List<Expenses>)Session["ExCustodyGridData"]).FirstOrDefault(it => it.Id == Id);
                expenses.Id = Id;

                expenses.Amount = Amount;
                expenses.CustodyName = CustodyName.ToString();
                expenses.CustodyId = CustodyId;
                expenses.Notes = Notes.ToString();
                Guid guid = Guid.NewGuid();
                String guidst = guid.ToString();

                return Json(new { IsValid = false, saveandnewFlag = false, Id = guidst }, JsonRequestBehavior.AllowGet);
            }
            else if (!saveandnewFlag && ((List<Expenses>)Session["ExCustodyGridData"]).Any(it => it.Id == Id))
            {
                Expenses expenses = ((List<Expenses>)Session["ExCustodyGridData"]).FirstOrDefault(it => it.Id == Id);
                expenses.Id = Id;

                expenses.Amount = Amount;
                expenses.CustodyName = CustodyName.ToString();
                expenses.CustodyId = CustodyId;
                expenses.Notes = Notes.ToString();

                return Json(new { IsValid = true, saveandnewFlag = false }, JsonRequestBehavior.AllowGet);
            }
            else if (!saveandnewFlag && !((List<Expenses>)Session["ExCustodyGridData"]).Any(it => it.Id == Id))
            {
                List<Expenses> itemlist = (List<Expenses>)Session["ExCustodyGridData"];
                itemlist.Add(new Expenses
                {
                    Id = Id,
                    Amount = Amount,
                    CustodyId = CustodyId,
                    CustodyName = CustodyName,
                    Notes = Notes,
                });
                Session["ExCustodyGridData"] = itemlist;
                return Json(new { IsValid = true, saveandnewFlag = false }, JsonRequestBehavior.AllowGet);
            }
            else if (saveandnewFlag && !((List<Expenses>)Session["ExCustodyGridData"]).Any(it => it.Id == Id))
            {
                List<Expenses> itemlist = (List<Expenses>)Session["ExCustodyGridData"];
                itemlist.Add(new Expenses
                {
                    Id = Id,
                    Amount = Amount,
                    CustodyId = CustodyId,
                    CustodyName = CustodyName,
                    Notes = Notes,
                });
                Guid guid = Guid.NewGuid();
                String guidst = guid.ToString();

                Session["ExCustodyGridData"] = itemlist;
                return Json(new { IsValid = true, saveandnewFlag = false, Id = guidst }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { IsValid = true, saveandnewFlag = true }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ConfirmationDeleteEXPopup(int ID)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            ViewBag.guid = "1";
            ViewBag.ID = ID;
            ViewBag.del = false;
            return Json(new { IsValid = false, page = RenderRazorViewToString("DeletePopup", null) }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ConfirmationDeletePopup(string guid)
        {
            ViewBag.guid = guid;
            ViewBag.ID = 1;
            ViewBag.del = true;
            return Json(new { IsValid = false, page = RenderRazorViewToString("DeletePopup", null) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteEXItem(int ID)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }

            try
            {
                CustodyExpenses custodyExpenses = db.CustodyExpensess.FirstOrDefault(it => it.ID == ID);
                db.CustodyExpensess.Remove(custodyExpenses);
                db.SaveChanges();
                return Json(new { }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        [HttpPost]
        public ActionResult DeleteItem(string guid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }

            try
            {
                guid = (guid ?? "").Trim().ToLower();

                List<Expenses> list = (List<Expenses>)Session["ExCustodyGridData"];

                Expenses expenses = list.Where(it => !string.IsNullOrWhiteSpace(guid) && (it.Id ?? "").Trim().ToLower() == guid).FirstOrDefault();

                list.Remove(expenses);

                Session["ExCustodyGridData"] = list;
                return Json(new { data = list }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult FillGrid()
        {

            List<Expenses> EXCustodytList;

            if (Session["ExCustodyGridData"] == null)
            {
                Session["ExCustodyGridData"] = new List<Expenses>();

                EXCustodytList = (List<Expenses>)Session["ExCustodyGridData"];

                return Json(new { data = EXCustodytList }, JsonRequestBehavior.AllowGet);
            }
            EXCustodytList = (List<Expenses>)Session["ExCustodyGridData"];
            return Json(new { data = EXCustodytList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PaymentReport(FormCollection formCollection, int ID)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("ID", ID.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("PaymentReport", "Reports");
        }
    }
}
