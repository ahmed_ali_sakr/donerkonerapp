﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Admin;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{

    public class BranchesController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Branches
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            return View();
        }
        public ActionResult Getbranches()
        {
            
            try
            {
                var branches = db.Branches.Select(a => new
                {
                    BranchName = a.Name,
                    Notes = a.Notes,
                    BranchNo = a.ID

                }).ToList();

                return Json(new { data = branches }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }
        [HttpPost]
        public ActionResult DeleteBranch(int BranchNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                Branch branch = db.Branches.FirstOrDefault(a => a.ID == BranchNo);
                db.Branches.Remove(branch);
                db.SaveChanges();
                return Json(new { data = branch }, JsonRequestBehavior.AllowGet);

            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        [HttpPost]
        public ActionResult Save(int BranchId,String Name, String Notes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.Branches.Any(b => b.ID == BranchId))
                {
                    Branch branch = db.Branches.FirstOrDefault(b => b.ID == BranchId);
                    branch.ID = BranchId;
                    branch.Name = Name;
                    branch.Notes = Notes;
                    branch.MB = SiteInfo.LoggedUserID;
                    branch.MD = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Branch branch = new Branch
                    {
                        Name = Name,
                        Notes = Notes,
                        CB = SiteInfo.LoggedUserID,
                        CD = DateTime.Now,

                    };
                    db.Branches.Add(branch);
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult EditBranch(int branchId)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                var branch = db.Branches.FirstOrDefault(b => b.ID == branchId);

                return Json(new { data = new { branch.Name, branch.Notes } }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult BranchReport(FormCollection formCollection)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("BranchesReport", "Reports");
        }
    }
}