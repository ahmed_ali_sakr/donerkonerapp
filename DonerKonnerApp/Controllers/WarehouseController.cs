﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Inventory;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class WarehouseController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Warehouse
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            return View();
        }

        public ActionResult GetWarehouses()

        {
            try
            {
                var warehouses = db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Select(a => new
                {
                    Warehouseid = a.ID,
                    WarehouseName = a.Name,
                    Notes = a.Notes
                    
                }).ToList();

                return Json(new { data = warehouses }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }


        [HttpPost]
        public ActionResult DeleteWarehouse(int Warehouseid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                Warehouse warehouse = db.Warehouses.FirstOrDefault(a => a.ID == Warehouseid);
                db.Warehouses.Remove(warehouse);
                db.SaveChanges();
                return Json(new { data = warehouse }, JsonRequestBehavior.AllowGet);

            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        public ActionResult EditWarehouse(int Warehouseid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                var warehouse = db.Warehouses.FirstOrDefault(u => u.ID == Warehouseid);

                return Json(new { data = new { warehouse.Name, warehouse.Notes } }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { isValid = false });
            }
        }
        [HttpPost]
        public ActionResult Save(String Name, String Notes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.Warehouses.Any(u => u.Name == Name))
                {
                    Warehouse warehouse = db.Warehouses.FirstOrDefault(u => u.Name == Name);
                    warehouse.Name = Name;
                    warehouse.Notes = Notes;
                    warehouse.MB = SiteInfo.LoggedUserID;
                    warehouse.MD = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Warehouse warehouse = new Warehouse
                    {
                        Name = Name,
                        Notes = Notes,
                        BranchID = SiteInfo.LoggedBranchNo,
                        CB = SiteInfo.LoggedUserID,
                        CD = DateTime.Now,

                    };
                    db.Warehouses.Add(warehouse);
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult WarehouseReport(FormCollection formCollection)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("WarehouseReport", "Reports");
        }
    }
}