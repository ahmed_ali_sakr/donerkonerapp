﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Report;
using DevExpress.Web.Mvc;
using DevExpress.XtraReports.UI;
using DonerKonner.Helpers;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class ReportsController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Reports
        protected ReportInfo GetReport(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            string typeName = "Agi.DonerKonner.Reports." + id.Trim().Replace("-", ".");
            
            Type type = Type.GetType(typeName, false, true);
            if (type == null)
                type = Type.GetType(typeName, false, true);

            if (type == null)
                return null;

            //string reportTitle = Session["UnitReport"] as string;
            XtraReport report;
            //if (reportTitle.IsNullOrWhiteSpace())
            //    report = Activator.CreateInstance(type) as XtraReport;
            //else
                report = Activator.CreateInstance(type) as XtraReport;

            if (report == null)
                return null;


            return new ReportInfo(id, report);
        }

        public ActionResult Index(string id, string reportTitle = "UnitReport")
        {
            
            Session["UnitReport"] = reportTitle;
            ReportInfo info = GetReport(id);
            //info.Title = reportTitle;
            if (info == null)
                return HttpNotFound();

            return View(info);
        }

        public ActionResult ViewReport(string id)
        {
            ReportInfo info = GetReport(id);
            var user = db.Users.Find(SiteInfo.LoggedUserID);
            if (info == null)
                return HttpNotFound();
            FormCollection formCollection = new FormCollection();
            formCollection = Session["parameters"] as FormCollection;
           

            
            if (formCollection != null)
            {
                foreach (var key in formCollection.AllKeys)
                {
                    string name = key.ToString();
                    if (info.Report.Parameters[key.ToString()] != null)
                    {
                        try
                        {
                            info.Report.Parameters[key.ToString()].Value = formCollection[key];
                            if (info.Report.Parameters[key.ToString()].Type == typeof(DateTime))
                                info.Report.Parameters[key.ToString()].Value = Convert.ToDateTime(formCollection[key]);

                            if (info.Report.Parameters[key.ToString()].Type == typeof(DateTime?))
                            {
                                if (string.IsNullOrWhiteSpace(formCollection[key]))
                                    info.Report.Parameters[key.ToString()].Value = null;
                                else
                                    info.Report.Parameters[key.ToString()].Value = Convert.ToDateTime(formCollection[key]);
                            }

                            if (info.Report.Parameters[key.ToString()].Type == typeof(int))
                                info.Report.Parameters[key.ToString()].Value = Convert.ToInt32(formCollection[key]);

                            if (info.Report.Parameters[key.ToString()].Type == typeof(int?))
                            {
                                if (string.IsNullOrWhiteSpace(formCollection[key]))
                                    info.Report.Parameters[key.ToString()].Value = null;
                                else
                                    info.Report.Parameters[key.ToString()].Value = Convert.ToInt32(formCollection[key]);
                            }

                            if (info.Report.Parameters[key.ToString()].Type == typeof(string))
                                info.Report.Parameters[key.ToString()].Value = formCollection[key].ToString();

                            if (info.Report.Parameters[key.ToString()].Type == typeof(bool))
                                info.Report.Parameters[key.ToString()].Value = Convert.ToBoolean(formCollection[key]);

                            if (info.Report.Parameters[key.ToString()].Type == typeof(bool?))
                            {
                                if (string.IsNullOrWhiteSpace(formCollection[key]))
                                    info.Report.Parameters[key.ToString()].Value = null;
                                else
                                    info.Report.Parameters[key.ToString()].Value = Convert.ToBoolean(formCollection[key]);
                            }
                        }
                        catch
                        {
                            info.Report.Parameters[key.ToString()].Value = formCollection[key];
                        }

                        info.Report.Parameters[key.ToString()].Visible = false;
                    }
                }
            }

            this.PopulateDataSource(info.Report);

            return PartialView(info);
        }

        public ActionResult ExportReport(string id)
        {
            ReportInfo info = GetReport(id);
            if (info == null)
                return HttpNotFound();

            if (Request == null || Request.Form == null)
                return HttpNotFound();

            FileResult result = null;
           
            return DocumentViewerExtension.ExportTo(info.Report, Request);
        }

        public virtual void PopulateDataSource(XtraReport report)
        {

        }
    }
}