﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Inventory;
using Agi.DonerKonner.DataAccess.ViewModel;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class AdjustmentController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Adjustment
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");
            return View();
        }
        public ActionResult AdjustmentList()
        {
            try
            {
                var data = (from Adjustment in db.InvAdjustments
                            join AdjustmentItems in db.InvAdjustmentItems on Adjustment.ID equals AdjustmentItems.InvAdjustmentID
                            where (Adjustment.BranchID == SiteInfo.LoggedBranchNo)
                            group Adjustment by new { Adjustment.ID, Adjustment.Warehouse, Adjustment.TransNo, Adjustment.TransDate, Adjustment.Notes } into grouped
                            select new
                            {
                                ID = grouped.Key.ID,
                                WarehouseName = grouped.Key.Warehouse.Name,
                                TransNo = grouped.Key.TransNo,
                                TransDate = grouped.Key.TransDate,
                                Count = grouped.Count(),
                                Notes = grouped.Key.Notes,


                            }).ToList().Select(a => new
                            {
                                ID = a.ID,
                                WarehouseName = a.WarehouseName,
                                TransNo = a.TransNo,
                                TransDate = a.TransDate.ToString("dd/MM/yyyy"),
                                itemsCount = a.Count,
                                Notes = a.Notes,
                            });

                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public void FillDropDown(InvAdjustment invAdjustment)
        {

            if (db.InvAdjustments.Any(i => i.ID == invAdjustment.ID))
            {
                var data = db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo && !db.InvAdjustments.Select(b => b.WarehouseID).Contains(a.ID)).ToList();
                data.Add(db.Warehouses.FirstOrDefault(a => a.ID == invAdjustment.WarehouseID));
                ViewBag.WarehouseID = new SelectList(data, "ID", "Name", invAdjustment.WarehouseID);

            }
            else
            {
                ViewBag.WarehouseID = new SelectList(db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo && !db.InvAdjustments.Select(b => b.WarehouseID).Contains(a.ID)).ToList(), "ID", "Name", null);
                Session["InvAdjustitemGridData"] = new List<PhCountItemGrid>();
            }


        }

        public ActionResult CreatAdjustment()
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");
                ViewBag.isAdd = true;

            InvAdjustment invAdjustment = new InvAdjustment();

            ViewBag.TransDate = db.GetServerDate();
            ViewBag.TransNo = db.InvAdjustments.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Count() == 0 ? 1 : db.InvAdjustments.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Max(a => a.TransNo) + 1;

            FillDropDown(invAdjustment);
            return View();
        }
        [HttpPost]
        public ActionResult CreatAdjustment(Boolean AddandNew, int WarehouseId, int TransNo, DateTime TransDate, string PhNotes)
        {
            try
            {
                if (db.InvAdjustments.Any(it => it.TransDate == TransDate && it.BranchID == SiteInfo.LoggedBranchNo) || TransDate > db.GetServerDate())
                {

                    return Json(new { IsValid = false, message = "Date is InValid" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    InvAdjustment invAdjustment = new InvAdjustment
                    {
                        BranchID = SiteInfo.LoggedBranchNo,
                        WarehouseID = WarehouseId,
                        TransNo = TransNo,
                        TransDate = TransDate,
                        Notes = PhNotes,
                        CB = SiteInfo.LoggedUserID,
                        CD = db.GetServerDate()
                    };
                    db.InvAdjustments.Add(invAdjustment);
                    db.SaveChanges();
                    var invadjustment = db.InvAdjustments.FirstOrDefault(i => i.TransDate == TransDate);
                    List<PhCountItemGrid> list = (List<PhCountItemGrid>)Session["InvAdjustitemGridData"];
                    List<InvAdjustmentItem> invAdjustmentItems = new List<InvAdjustmentItem>();
                    foreach (var item in list)
                    {
                        var unit = db.Units.FirstOrDefault(o => o.Name == item.UnitName);
                        invAdjustmentItems.Add(new InvAdjustmentItem
                        {

                            InvAdjustmentID = invadjustment.ID,
                            ItemID = item.ItemId,
                            UnitID = unit.ID,
                            Qty = item.Qty,
                            Notes = item.Notes,
                            CB = SiteInfo.LoggedUserID,
                            CD = db.GetServerDate()

                        });

                    }
                    db.InvAdjustmentItems.AddRange(invAdjustmentItems);
                    db.SaveChanges();
                    Session["InvAdjustitemGridData"] = null;

                    if (AddandNew)
                    {
                        ViewBag.TransNo = db.InvAdjustments.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Count() == 0 ? 1 : db.InvAdjustments.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Max(a => a.TransNo) + 1;
                        ViewBag.TransDate = db.GetServerDate();
                        InvAdjustment invAdjustment1 = new InvAdjustment();
                        FillDropDown(invAdjustment1);
                        return Json(new { isValid = true });
                    }
                    else
                    {
                        return Json(new { isValid = true });
                    }

                }
            }
            catch (Exception ex)
            {
                return Json(new { isValid = true });
            }



        }
        public ActionResult fillDataTable()
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");

            if (Session["InvAdjustitemGridData"] == null)
            {
                return Json(new { data = new List<PhCountItemGrid>() }, JsonRequestBehavior.AllowGet);
            }
            List<PhCountItemGrid> AdjustmentList = (List<PhCountItemGrid>)Session["InvAdjustitemGridData"];
            return Json(new { data = AdjustmentList }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Additem()
        {
            ViewBag.ItemList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Name", null);
            ViewBag.CodeList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Code", null);
            ViewBag.iscreate = true;
            Guid guid = Guid.NewGuid();
            String guidst = guid.ToString();
            ViewBag.guidval = guidst;

            return Json(new { IsValid = false, page = RenderRazorViewToString("_ItemPopUp", null) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddItem(string guid, Boolean saveandnewFlag, int ItemId, string ItemName, string ItemCode, string UnitName, decimal Qty, string Notes)
        {
            if (((List<PhCountItemGrid>)Session["InvAdjustitemGridData"]).Any(it => it.ItemId == ItemId))
            {
                ViewBag.ItemList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Name", null);
                ViewBag.CodeList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Code", null);
                ViewBag.guid = guid;
                return Json(new { IsValid = false, saveandnewFlag, message = "This Item is Already in The Grid" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<PhCountItemGrid> list = (List<PhCountItemGrid>)Session["InvAdjustitemGridData"];
                list.Add(new PhCountItemGrid
                {
                    guid = guid,
                    ItemId = ItemId,
                    ItemName = ItemName,
                    ItemCode = ItemCode,
                    UnitName = UnitName,
                    Qty = Qty,
                    Notes = Notes
                }); ;
                Session["InvAdjustitemGridData"] = list;

                return Json(new { IsValid = true, saveandnewFlag }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult bindItemsDataDll(int ItemId)
        {
            try
            {
                Item item = db.Items.FirstOrDefault(o => o.ID == ItemId && o.HasStock == true);

                var unitName = item.Unit.Name;

                return Json(new { unitName }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult Edititem(string guid)
        {
            ViewBag.ItemList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Name", null);
            ViewBag.CodeList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Code", null);
            ViewBag.iscreate = false;

            PhCountItemGrid phCountItemGrid = ((List<PhCountItemGrid>)Session["InvAdjustitemGridData"]).FirstOrDefault(it => it.guid == guid);
            ViewBag.guidval = guid;
            int itemID = phCountItemGrid.ItemId;
            string itemName = phCountItemGrid.ItemName.ToString();
            string itemCode = phCountItemGrid.ItemCode.ToString();
            decimal qty = phCountItemGrid.Qty;
            string notes = phCountItemGrid.Notes.ToString();

            return Json(new { IsValid = false, ItemId = itemID, Qty = qty, Notes = notes, page = RenderRazorViewToString("_ItemPopUp", null) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edititem(string guid, Boolean saveandnewFlag, int ItemId, string ItemName, string ItemCode, string UnitName, decimal Qty, string Notes)
        {
            if (((List<PhCountItemGrid>)Session["InvAdjustitemGridData"]).Any(it => it.ItemId == ItemId && it.guid != guid))
            {
                ViewBag.ItemList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Name", null);
                ViewBag.CodeList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Code", null);
                ViewBag.guidval = guid;
                return Json(new { IsValid = false, saveandnewFlag, ItemId, Qty, Notes, message = "This Item is Already in The Grid" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                PhCountItemGrid phCountItemGrid = ((List<PhCountItemGrid>)Session["InvAdjustitemGridData"]).FirstOrDefault(it => it.guid == guid);
                phCountItemGrid.guid = guid;

                phCountItemGrid.ItemId = ItemId;
                phCountItemGrid.ItemName = ItemName.ToString();
                phCountItemGrid.ItemCode = ItemCode.ToString();
                phCountItemGrid.UnitName = UnitName.ToString();
                phCountItemGrid.Qty = Qty;
                phCountItemGrid.Notes = Notes.ToString();
                return Json(new { IsValid = true, saveandnewFlag = true }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ConfirmationDeletePopup(string guid)
        {
            ViewBag.guid = guid;
            ViewBag.TransNo = 1;
            ViewBag.del = true;
            return Json(new { IsValid = false, page = RenderRazorViewToString("_DeletePopup", null) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConfirmationDeletePhPopup(int TransNo)
        {
            ViewBag.guid = "1";
            ViewBag.TransNo = TransNo;
            ViewBag.del = false;
            return Json(new { IsValid = false, page = RenderRazorViewToString("_DeletePopup", null) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteItem(string guid)
        {
            try
            {
                guid = (guid ?? "").Trim().ToLower();

                List<PhCountItemGrid> list = (List<PhCountItemGrid>)Session["InvAdjustitemGridData"];

                PhCountItemGrid phCountItemGrid = list.Where(it => !string.IsNullOrWhiteSpace(guid) && (it.ItemCode ?? "").Trim().ToLower() == guid).FirstOrDefault();

                list.Remove(phCountItemGrid);

                Session["InvAdjustitemGridData"] = list;
                return Json(new { data = list }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult EditAdjustment(int TransNo)
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");

            ViewBag.isAdd = false;
            InvAdjustment invAdjustment;

            invAdjustment = db.InvAdjustments.FirstOrDefault(a => a.TransNo == TransNo);
            ViewBag.TransDate = invAdjustment.TransDate;
            List<InvAdjustmentItem> list = db.InvAdjustmentItems.Where(a => a.InvAdjustmentID == invAdjustment.ID).ToList();
            List<PhCountItemGrid> itemList = new List<PhCountItemGrid>();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    Guid guid = Guid.NewGuid();
                    String guidst = guid.ToString();
                    itemList.Add(new PhCountItemGrid
                    {
                        guid = guidst,
                        ItemId = item.ItemID,
                        ItemName = item.Item.Name,
                        ItemCode = item.Item.Code,
                        UnitName = item.Unit.Name,
                        Qty = item.Qty,
                        Notes = item.Notes
                    });
                }
                Session["InvAdjustitemGridData"] = itemList;
            }
            FillDropDown(invAdjustment);

            ViewBag.AddandNew = false;
            return View("CreatAdjustment", invAdjustment);

        }
        [HttpPost]
        public ActionResult EditAdjustment(Boolean AddandNew, int WarehouseId, int TransNo, DateTime TransDate, string PhNotes)
        {
            try
            {
                if (db.InvAdjustments.Any(it => it.TransDate == TransDate && it.BranchID == SiteInfo.LoggedBranchNo) || TransDate > db.GetServerDate())
                {
                    InvAdjustment invAdjustment = db.InvAdjustments.Where(it => it.TransDate == TransDate).FirstOrDefault();
                    if (invAdjustment.TransNo != TransNo)
                    {
                        return Json(new { IsValid = false, message = "Date is InValid" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        invAdjustment.BranchID = SiteInfo.LoggedBranchNo;
                        invAdjustment.WarehouseID = WarehouseId;
                        invAdjustment.TransNo = TransNo;
                        invAdjustment.TransDate = TransDate;
                        invAdjustment.Notes = PhNotes;
                        invAdjustment.MB = SiteInfo.LoggedUserID;
                        invAdjustment.MD = db.GetServerDate();

                        db.InvAdjustmentItems.RemoveRange(db.InvAdjustmentItems.Where(it => it.InvAdjustmentID == invAdjustment.ID));

                        db.SaveChanges();

                        List<PhCountItemGrid> list = (List<PhCountItemGrid>)Session["InvAdjustitemGridData"];
                        List<InvAdjustmentItem> invAdjustmentItem = new List<InvAdjustmentItem>();
                        foreach (var item in list)
                        {
                            var unit = db.Units.FirstOrDefault(o => o.Name == item.UnitName);
                            invAdjustmentItem.Add(new InvAdjustmentItem
                            {

                                InvAdjustmentID = invAdjustment.ID,
                                ItemID = item.ItemId,
                                UnitID = unit.ID,
                                Qty = item.Qty,
                                Notes = item.Notes,
                                CB = SiteInfo.LoggedUserID,
                                CD = db.GetServerDate()

                            });

                        }
                        db.InvAdjustmentItems.AddRange(invAdjustmentItem);
                        db.SaveChanges();
                        Session["InvAdjustitemGridData"] = null;

                        if (AddandNew)
                        {
                            ViewBag.TransNo = db.InvAdjustments.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Count() == 0 ? 1 : db.InvAdjustments.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Max(a => a.TransNo) + 1;
                            ViewBag.TransDate = db.GetServerDate();
                            InvAdjustment invAdjustment1 = new InvAdjustment();
                            FillDropDown(invAdjustment1);
                            return Json(new { isValid = true });
                        }
                        else
                        {
                            return Json(new { isValid = true });
                        }
                    }
                }

                return Json(new { isValid = true });
            }
            catch (Exception ex)
            {
                return Json(new { isValid = true });
            }



        }
        [HttpPost]
        public ActionResult DeletePhItem(int TransNo)
        {
            try
            {
                InvAdjustment invAdjustment = db.InvAdjustments.FirstOrDefault(it => it.TransNo == TransNo);
                db.InvAdjustments.Remove(invAdjustment);
                db.SaveChanges();
                return Json(new { }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult AdjustmentReport(FormCollection formCollection, int TransNo)
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");

            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("TransNo", TransNo.ToString());
            formCollection.Add("BranchId", SiteInfo.LoggedBranchNo.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("AdjustmentItemReport", "Reports");
        }



    }
}
