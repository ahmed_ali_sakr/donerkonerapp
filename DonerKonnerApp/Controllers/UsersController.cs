﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Admin;
using Agi.DonerKonner.DataAccess.ViewModel;
using DonerKonner.Helpers;

namespace Agi.DonerKonner.Controllers
{
    public class UsersController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();

        // GET: Users
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            return View();
        }
        public ActionResult UsersList()
        {
            try
            {
                var userslist = db.Users.Select(a => new
                {
                    ID = a.ID,
                    FullName = a.FullName,
                    LoginName = a.LoginName,
                    IsActive = a.IsActive ? "Active" : "Not Active"

                });
                return Json(new { data = userslist }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult AddUser()
        {
            var userSecurityRoleVM = (from SCR in db.securityRoles
                                      where (SCR.IsActive == true)
                                      group SCR by new { SCR.ID, SCR.Name } into grouped
                                      select new UserSecurityRoleViewModel
                                      {
                                          SecurityRoleID = grouped.Key.ID,
                                          Name = grouped.Key.Name,
                                          IsChecked = false,
                                          IsActive = false,
                                          IsDefault = false
                                      }).ToList();
            var userBranshsVM = (from B in db.Branches
                                 group B by new { B.ID, B.Name } into grouped
                                 select new UserBranchsViewModel
                                 {
                                     BranchID = grouped.Key.ID,
                                     Name = grouped.Key.Name,
                                     IsChecked = false,
                                     IsActive = false,
                                     IsDefault = false
                                 }).ToList();
            var userViewModel = new UserViewModel
            {
                UserSecurityRolesVM = userSecurityRoleVM,
                UserBranchsVM = userBranshsVM
            };
            ViewBag.Isadd = "AddUser";
            return Json(new { isValid = true, page = RenderRazorViewToString("_AddUser", userViewModel) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddUser(UserViewModel userViewModel)
        {
            try
            {
                var usersecurityvm = userViewModel.UserSecurityRolesVM.Where(m => m.IsChecked == true).ToList();
                var userbranchvm = userViewModel.UserBranchsVM.Where(m => m.IsChecked == true).ToList();
                if (userViewModel.user.FullName == null || userViewModel.user.LoginName == null || userViewModel.user.Password == null || userViewModel.ConfirmPassword == null)
                {
                    return Json(new { isValid = false, formmsg = "Enter all form data", flag = 6 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (db.Users.Any(a => a.FullName == userViewModel.user.FullName))
                    {
                        return Json(new { isValid = false, msg5 = "This FullName is Already Existed", flag = 5 }, JsonRequestBehavior.AllowGet);
                    }
                    if (db.Users.Any(a => a.LoginName == userViewModel.user.LoginName))
                    {
                        return Json(new { isValid = false, msg1 = "This LoginName is Already Existed", flag = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    if (userViewModel.ConfirmPassword != userViewModel.user.Password)
                    {
                        return Json(new { isValid = false, msg2 = "This Password not matched", flag = 2 }, JsonRequestBehavior.AllowGet);
                    }

                    if (usersecurityvm.Count == 0)
                    {
                        return Json(new { isValid = false, msg3 = "You must choose at least one from SecurityRoles list", flag = 3 }, JsonRequestBehavior.AllowGet);
                    }
                    if (userbranchvm.Count == 0)
                    {
                        return Json(new { isValid = false, msg4 = "You must choose at least one from Branches list", flag = 4 }, JsonRequestBehavior.AllowGet);
                    }
                }
                var newuser = new User
                {
                    FullName = userViewModel.user.FullName,
                    LoginName = userViewModel.user.LoginName,
                    Password = userViewModel.user.Password,
                    IsActive = userViewModel.user.IsActive,
                    Notes = userViewModel.user.Notes,
                    CB = SiteInfo.LoggedUserID,
                    CD = db.GetServerDate()
                };
                db.Users.Add(newuser);
                db.SaveChanges();
                var addeduser = db.Users.Where(a => a.LoginName == userViewModel.user.LoginName).FirstOrDefault();
                for (var i = 0; i < usersecurityvm.Count; i++)
                {
                    var usrole = new UserSecurityRole
                    {
                        UserID = addeduser.ID,
                        SecurityRoleID = usersecurityvm[i].SecurityRoleID,
                        IsActive = usersecurityvm[i].IsActive,
                        IsDefault = usersecurityvm[i].IsDefault,
                        CB = SiteInfo.LoggedUserID,
                        CD = db.GetServerDate()
                    };
                    db.UserSecurityRoles.Add(usrole);
                }
                for (var i = 0; i < userbranchvm.Count; i++)
                {
                    var usbranch = new UserBranch
                    {
                        UserID = addeduser.ID,
                        BranchID = userbranchvm[i].BranchID,
                        IsActive = userbranchvm[i].IsActive,
                        IsDefault = userbranchvm[i].IsDefault,
                        Notes = "",
                        CB = SiteInfo.LoggedUserID,
                        CD = db.GetServerDate()
                    };
                    db.UserBranches.Add(usbranch);
                }
                db.SaveChanges();

                return Json(new { isValid = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { isValid = false, formmsg = "Enter all form data", flag = 6 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditUser(int ID)
        {
            List<UserSecurityRole> USCR = db.UserSecurityRoles.Where(a => a.UserID == ID).ToList();
            List<UserBranch> USB = db.UserBranches.Where(a => a.UserID == ID).ToList();

            var userSecurityRoleVM = (from SCR in db.securityRoles
                                      where (SCR.IsActive == true)
                                      select new UserSecurityRoleViewModel
                                      {
                                          SecurityRoleID = SCR.ID,
                                          Name = SCR.Name
                                      }).ToList().Select(o => new UserSecurityRoleViewModel
                                      {
                                          SecurityRoleID = o.SecurityRoleID,
                                          Name = o.Name,
                                          IsChecked = USCR != null && USCR.Count() > 0 && USCR.Any(a => a.SecurityRoleID == o.SecurityRoleID) ? true : false,
                                          IsActive = USCR != null && USCR.Count() > 0 && USCR.Any(a => a.SecurityRoleID == o.SecurityRoleID) ? USCR.FirstOrDefault(a => a.SecurityRoleID == o.SecurityRoleID).IsActive : false,
                                          IsDefault = USCR != null && USCR.Count() > 0 && USCR.Any(a => a.SecurityRoleID == o.SecurityRoleID) ? USCR.FirstOrDefault(a => a.SecurityRoleID == o.SecurityRoleID).IsDefault : false,
                                      }).ToList();

            var userBranshsVM = (from B in db.Branches
                                 select new UserBranchsViewModel
                                 {
                                     BranchID = B.ID,
                                     Name = B.Name
                                 }).ToList().Select(o => new UserBranchsViewModel
                                 {
                                     BranchID = o.BranchID,
                                     Name = o.Name,
                                     IsChecked = USB != null && USB.Count() > 0 && USB.Any(a => a.BranchID == o.BranchID) ? true : false,
                                     IsActive = USB != null && USB.Count() > 0 && USB.Any(a => a.BranchID == o.BranchID) ? USB.FirstOrDefault(a => a.BranchID == o.BranchID).IsActive : false,
                                     IsDefault = USB != null && USB.Count() > 0 && USB.Any(a => a.BranchID == o.BranchID) ? USB.FirstOrDefault(a => a.BranchID == o.BranchID).IsDefault : false,
                                 }).ToList();
            var userViewModel = new UserViewModel
            {
                user = db.Users.Where(a => a.ID == ID).FirstOrDefault(),
                ConfirmPassword = db.Users.Where(a => a.ID == ID).FirstOrDefault().Password,
                UserSecurityRolesVM = userSecurityRoleVM,
                UserBranchsVM = userBranshsVM
            };
            ViewBag.Isadd = "EditUser";
            return Json(new { IsValid = true, page = RenderRazorViewToString("_AddUser", userViewModel) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditUser(UserViewModel userViewModel)
        {
            try
            {
                User user = db.Users.Where(a => a.ID == userViewModel.user.ID).FirstOrDefault();
                var usersecurityvm = userViewModel.UserSecurityRolesVM.Where(m => m.IsChecked == true).ToList();
                var userbranchvm = userViewModel.UserBranchsVM.Where(m => m.IsChecked == true).ToList();
                bool passchanged = false;
                if (userViewModel.user.FullName == null || userViewModel.user.LoginName == null)
                {
                    return Json(new { isValid = false, formmsg = "Enter all form data", flag = 6 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (db.Users.Any(a => a.FullName == userViewModel.user.FullName && a.ID != user.ID))
                    {
                        return Json(new { isValid = false, msg5 = "This FullName is Already Existed", flag = 5 }, JsonRequestBehavior.AllowGet);
                    }
                    if (db.Users.Any(a => a.LoginName == userViewModel.user.LoginName && a.ID != user.ID))
                    {
                        return Json(new { isValid = false, msg1 = "This LoginName is Already Existed", flag = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    if (userViewModel.user.Password != null)
                    {
                        if (userViewModel.ConfirmPassword != null)
                        {
                            if (userViewModel.ConfirmPassword != userViewModel.user.Password)
                            {
                                return Json(new { isValid = false, msg2 = "This Password not matched", flag = 2 }, JsonRequestBehavior.AllowGet);
                            }
                            else if (userViewModel.ConfirmPassword == userViewModel.user.Password)
                            {
                                passchanged = true;
                            }
                        }
                        else { return Json(new { isValid = false, msg2 = "This Password not matched", flag = 2 }, JsonRequestBehavior.AllowGet); }
                    }
                    if (usersecurityvm.Count == 0)
                    {
                        return Json(new { isValid = false, msg3 = "You must choose at least one from SecurityRoles list", flag = 3 }, JsonRequestBehavior.AllowGet);
                    }
                    if (userbranchvm.Count == 0)
                    {
                        return Json(new { isValid = false, msg4 = "You must choose at least one from Branches list", flag = 4 }, JsonRequestBehavior.AllowGet);
                    }
                }

                user.FullName = userViewModel.user.FullName;
                user.LoginName = userViewModel.user.LoginName;
                user.Password = passchanged ? userViewModel.user.Password : user.Password;
                user.IsActive = userViewModel.user.IsActive;
                user.Notes = userViewModel.user.Notes;
                user.MB = SiteInfo.LoggedUserID;
                user.MD = db.GetServerDate();
                db.SaveChanges();

                var addeduser = db.Users.Where(a => a.LoginName == userViewModel.user.LoginName).FirstOrDefault();
                List<UserBranch> userBranch = db.UserBranches.Where(a => a.UserID == userViewModel.user.ID).ToList();
                db.UserBranches.RemoveRange(userBranch);
                List<UserSecurityRole> userSecurityrole = db.UserSecurityRoles.Where(a => a.UserID == userViewModel.user.ID).ToList();
                db.UserSecurityRoles.RemoveRange(userSecurityrole);
                for (var i = 0; i < usersecurityvm.Count; i++)
                {
                    var usrole = new UserSecurityRole
                    {
                        UserID = addeduser.ID,
                        SecurityRoleID = usersecurityvm[i].SecurityRoleID,
                        IsActive = usersecurityvm[i].IsActive,
                        IsDefault = usersecurityvm[i].IsDefault,
                        CB = SiteInfo.LoggedUserID,
                        CD = db.GetServerDate()
                    };
                    db.UserSecurityRoles.Add(usrole);
                }
                for (var i = 0; i < userbranchvm.Count; i++)
                {
                    var usbranch = new UserBranch
                    {
                        UserID = addeduser.ID,
                        BranchID = userbranchvm[i].BranchID,
                        IsActive = userbranchvm[i].IsActive,
                        IsDefault = userbranchvm[i].IsDefault,
                        Notes = "",
                        CB = SiteInfo.LoggedUserID,
                        CD = db.GetServerDate()
                    };
                    db.UserBranches.Add(usbranch);
                }
                db.SaveChanges();
                return Json(new { isValid = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { isValid = false, formmsg = "Enter all form data", flag = 6 }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeletePopup(int ID)
        {
            User user = db.Users.Where(a => a.ID == ID).FirstOrDefault();
            return Json(new { isValid = true, page = RenderRazorViewToString("_DeleteUser", user) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteUser(int ID)
        {
            User user = db.Users.Where(a => a.ID == ID).FirstOrDefault();
            db.Users.Remove(user);
            db.SaveChanges();
            return Json(new { isValid = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
