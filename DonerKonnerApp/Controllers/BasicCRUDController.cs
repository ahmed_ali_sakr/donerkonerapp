﻿using Agi.DonerKonner.DataAccess;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class BasicCRUDController : Controller
    {
        private DonerKonnerContext db = new DonerKonnerContext();

        // GET: BasicCRUD
        public string RenderRazorViewToString(string viewName, object model)
        {
            var keys = ViewData.Select(o => new { o.Key, o.Value }).ToList();
            var modelStat = ViewData.ModelState.Select(o => new { o.Key, o.Value });
            ViewData = new ViewDataDictionary(model);
            foreach (var item in keys)
                ViewData[item.Key] = item.Value;
            if (model != null)
            {
                foreach (var item in modelStat)
                    ViewData.ModelState.Add(item.Key, item.Value);
            }
            ControllerContext.Controller.ViewData = ViewData;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);

                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewContext.Controller.ViewData = ViewData;

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
                //var ss = RenderViewToString(ControllerContext, viewName, model, true);
                //return ss;
            }
        }

        public bool GetUserTicket(int userID)
        {
            //var userSecurityRole = db.UserSecurityRoles.Where(o => o.UserID == userID).FirstOrDefault();
            var SystenFunction = (from SUF in db.SecurityRoleFunctions
                                  join SF in db.SystemFunctions on SUF.SystemFunctionID equals SF.ID
                                  where (SUF.SecurityRoleID == SiteInfo.LoggedSecurityRoleID)
                                  select new
                                  {
                                      url = SF.URL.ToString().ToLower()
                                  }).ToList();
           
            string InsertedUrl = "/" + this.Request.Url.Segments[1].ToString().ToLower() + this.Request.Url.Segments[2].ToString().ToLower();
            
            if (SystenFunction.Any(o => o.url == InsertedUrl))
            {
                return true;
            }
            else
            {
                SiteInfo.LoggedUserID = -1;
                SiteInfo.LoggedLoginName = "";
                SiteInfo.LoggedUserName = "";
                SiteInfo.LoggedSecurityRoleID = -1;
                SiteInfo.LoggedSecurityRoleName = "";
                SiteInfo.LoggedBranchNo = -1;
                SiteInfo.LoggedBranchName = "";
                return false;
            }

        }
        public bool GetUserAllTicket(int userID)
        {
            //var userSecurityRole = db.UserSecurityRoles.Where(o => o.UserID == userID).FirstOrDefault();
            var SystenFunction = (from SUF in db.SecurityRoleFunctions
                                  join SF in db.SystemFunctions on SUF.SystemFunctionID equals SF.ID
                                  where (SUF.SecurityRoleID == SiteInfo.LoggedSecurityRoleID)
                                  select new
                                  {
                                      url = SF.URL.ToString().ToLower()
                                  }).ToList();

            string InsertedUrl = this.Request.RawUrl.ToString().ToLower();

            if (SystenFunction.Any(o => o.url == InsertedUrl))
            {
                return true;
            }
            else
            {
                SiteInfo.LoggedUserID = -1;
                SiteInfo.LoggedLoginName = "";
                SiteInfo.LoggedUserName = "";
                SiteInfo.LoggedSecurityRoleID = -1;
                SiteInfo.LoggedSecurityRoleName = "";
                SiteInfo.LoggedBranchNo = -1;
                SiteInfo.LoggedBranchName = "";
                return false;
            }

        }
        public bool GetUserTicketView(string InsertedUrl)
        {
            //var userSecurityRole = db.UserSecurityRoles.Where(o => o.UserID == SiteInfo.LoggedUserID).FirstOrDefault();
            var SystenFunction = (from SUF in db.SecurityRoleFunctions
                                  join SF in db.SystemFunctions on SUF.SystemFunctionID equals SF.ID
                                  where (SUF.SecurityRoleID==SiteInfo.LoggedSecurityRoleID)
                                  select new
                                  {
                                      url = SF.URL.ToString().ToLower()
                                  }).ToList();


            if (SystenFunction.Any(o => o.url == InsertedUrl.ToLower()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public class UserTicket
        {
            public bool IsLogged { get; set; }
            public bool CanView { get; set; }
            public int? SystemFunctionID { get; set; }
            public int? SecurityRoleID { get; set; }
            public string UnauthorizedPartialView { get; set; }
        }
    }
}