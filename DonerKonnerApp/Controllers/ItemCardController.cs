﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agi.DonerKonner.DataAccess;
using DonerKonner.Helpers;
using Agi.DonerKonner.DataAccess.ViewModel;
using Agi.DonerKonner.DataAccess.Inventory;


namespace Agi.DonerKonner.Controllers
{
    public class ItemCardController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: ItemCard
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            ViewBag.DateTime = db.GetServerDate();
            List<Item> ItemsList = db.Items.ToList();
            List<ItemGroup> GroupList = db.ItemGroups.ToList();
            ViewBag.ItemList = new SelectList(ItemsList,"ID","Name");
            ViewBag.GroupList = new SelectList(GroupList, "ID","Name");
            

            return View();
        }
        public ActionResult bindItemsDataDll(int GroupId) {
            try
            {
               var  items = db.Items.Where(o => o.ItemGroupID == GroupId && o.HasStock == true).Select(o=>new {ID=o.ID,Name=o.Name }).ToList();

                

                return Json(new { items }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult bindGroupDataDll(int ItemId)
        {
            try
            {
                var itemGroup = db.Items.Where(o => o.ID == ItemId).Select(o => new { ID = o.ID, Name = o.Name, ItemGroupID=o.ItemGroupID }).ToList();


           
                return Json(new { itemGroup }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult ItemCardReport(string FromDate, string ToDate)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            FormCollection formCollection = new FormCollection();
           formCollection.Add("Organization_Name", "Doner Konner");
           formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
           formCollection.Add("FromDate", FromDate.ToString());
           formCollection.Add("ToDate", ToDate.ToString());
           formCollection.Add("BranchId", SiteInfo.LoggedBranchNo.ToString());
           formCollection.Add("Branch_Name", SiteInfo.LoggedBranchName.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("ItemCardReport", "Reports");
        }


    }
}