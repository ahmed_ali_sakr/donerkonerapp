﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Inventory;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class VendorController : BasicCRUDController
    {
        // GET: Vendor
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Unit
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            return View();
        }
        public ActionResult GetVendors()

        {
            try
            {
                var vendors = db.Vendors.Select(a => new
                {
                    Vendorid = a.ID,
                    VendorName = a.Name,
                    Notes = a.Notes
                    

                }).ToList();

                return Json(new { data = vendors }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        [HttpPost]
        public ActionResult DeleteVendor(int Vendorid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                Vendor vendor = db.Vendors.FirstOrDefault(a => a.ID == Vendorid);
                db.Vendors.Remove(vendor);
                db.SaveChanges();
                return Json(new { data = vendor }, JsonRequestBehavior.AllowGet);

            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        [HttpPost]
        public ActionResult Save(String Name, String Notes)
        {
            try
            {
                if (db.Vendors.Any(u => u.Name == Name))
                {
                    Vendor vendor = db.Vendors.FirstOrDefault(u => u.Name == Name);
                    vendor.Name = Name;
                    vendor.Notes = Notes;
                    vendor.MB = SiteInfo.LoggedUserID;
                    vendor.MD = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Vendor vendor = new Vendor
                    {
                        Name = Name,
                        Notes = Notes,
                        CB = SiteInfo.LoggedUserID,
                        CD = DateTime.Now,

                    };
                    db.Vendors.Add(vendor);
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult EditVendor(int Vendorid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                var vendor = db.Vendors.FirstOrDefault(u => u.ID == Vendorid);

                return Json(new { data = new { vendor.Name, vendor.Notes } }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult VendorReport(FormCollection formCollection)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("VendorReport", "Reports");
        }
    }
}