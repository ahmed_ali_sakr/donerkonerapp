﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Custody;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class CustodyItemController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: CustodyItem
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            return View();
        }
        public ActionResult GetCitems()
        {
            try
            {
                var Citems = db.CustodyItems.Select(a => new
                {
                    Citemid = a.ID,
                    CitemName = a.Name,
                    Notes = a.Notes

                }).ToList();

                return Json(new { data = Citems }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        [HttpPost]
        public ActionResult DeleteCitems(int Citemid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                CustodyItem custodyItem = db.CustodyItems.FirstOrDefault(a => a.ID == Citemid);
                db.CustodyItems.Remove(custodyItem);
                db.SaveChanges();
                return Json(new { data = custodyItem }, JsonRequestBehavior.AllowGet);

            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }
        [HttpPost]
        public ActionResult Save(String Name, String Notes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.CustodyItems.Any(u => u.Name == Name))
                {
                    CustodyItem custodyItem = db.CustodyItems.FirstOrDefault(u => u.Name == Name);
                    custodyItem.Name = Name;
                    custodyItem.Notes = Notes;
                    custodyItem.MB = SiteInfo.LoggedUserID;
                    custodyItem.MD = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    CustodyItem custodyItem = new CustodyItem
                    {
                        Name = Name,
                        Notes = Notes,
                        CB = SiteInfo.LoggedUserID,
                        CD = DateTime.Now,

                    };
                    db.CustodyItems.Add(custodyItem);
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult EditcCitem(int Citemid)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                var custodyItem = db.CustodyItems.FirstOrDefault(u => u.ID == Citemid);

                return Json(new { data = new { custodyItem.Name, custodyItem.Notes } }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult CustodyItemReport(FormCollection formCollection)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("CustodyItemReport", "Reports");
        }
    }
}