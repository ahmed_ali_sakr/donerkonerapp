﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Inventory;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class UnitController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Unit
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            return View();
        }
        public ActionResult GetUnits()

        {
            try
            {
                var units = db.Units.Select(a => new
                {
                    UnitName = a.Name,
                    Notes = a.Notes,
                    UnitNo = a.ID

                }).ToList();

                return Json(new { data = units }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        [HttpPost]
        public ActionResult DeleteUnit(int UnitNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                Unit unit = db.Units.FirstOrDefault(a => a.ID == UnitNo);
                db.Units.Remove(unit);
                db.SaveChanges();
                return Json(new { data = unit }, JsonRequestBehavior.AllowGet);

            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        [HttpPost]
        public ActionResult Save(int UnitId, String Name, String Notes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.Units.Any(u => u.ID == UnitId))
                {
                    Unit unit = db.Units.FirstOrDefault(u => u.ID == UnitId);
                    unit.ID = UnitId;
                    unit.Name = Name;
                    unit.Notes = Notes;
                    unit.MB = SiteInfo.LoggedUserID;
                    unit.MD = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Unit unit = new Unit
                    {
                        Name = Name,
                        Notes = Notes,
                        CB = SiteInfo.LoggedUserID,
                        CD = DateTime.Now,

                    };
                    db.Units.Add(unit);
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult EditUnit(int unitId)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                var unit = db.Units.FirstOrDefault(u => u.ID == unitId);

                return Json(new { data = new { unit.Name, unit.Notes } }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult UnitReport(FormCollection formCollection)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("UnitReport", "Reports");
        }
    }
}