﻿using DonerKonner.Helpers;
using System.Linq;
using System.Web.Mvc;
using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Admin;
using System.Collections.Generic;
using Agi.DonerKonner.DataAccess.Inventory;
using Agi.DonerKonner.DataAccess.ViewModel;
using System;

namespace Agi.DonerKonner.Controllers
{
    public class HomeController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            int i = SiteInfo.LoggedSecurityRoleID;
            return View();
        }

        public ActionResult changebranch()
        {
            ViewBag.BranchID = new SelectList(db.Branches.Where(a=> db.UserBranches.Where(b=>b.UserID==SiteInfo.LoggedUserID && b.IsActive == true).Select(b=>b.BranchID).Contains(a.ID)).ToList(), "ID", "Name",SiteInfo.LoggedBranchNo);
            return PartialView("_ChangeBranch");
        }
        public ActionResult changesecurityrole()
        {
            ViewBag.SecurityRoleId = new SelectList(db.securityRoles.Where(a => db.UserSecurityRoles.Where(b => b.UserID == SiteInfo.LoggedUserID && b.IsActive == true).Select(b => b.SecurityRoleID).Contains(a.ID)).ToList(), "ID", "Name", SiteInfo.LoggedSecurityRoleID);
            return PartialView("_ChangeSecurityRole");
        }
        public ActionResult Login()         
        { 
            if (SiteInfo.IsLogged)
            {
                return RedirectToAction("index", "Home");
            }
            
            //ViewBag.BranchID = new SelectList(db.Branches.ToList(), "ID", "Name");
            

            var logindata = new login {
                
            };
            return View();
        }

        public ActionResult ChangeBranchAction(int BranchID)
        {
            
            try
            {

                ViewBag.BranchID = new SelectList(db.Branches.Where(a => db.UserBranches.Where(b => b.UserID == SiteInfo.LoggedUserID && b.IsActive==true).Select(b => b.BranchID).Contains(a.ID)).ToList(), "ID", "Name");
                SiteInfo.LoggedBranchNo = BranchID;
                SiteInfo.LoggedBranchName = db.Branches.FirstOrDefault(a=>a.ID==BranchID).Name;

                //SiteInfo.LoggedSecurityRoleID = 1; //ToDo: Fixed security role 1 till implement it
                if (Request.QueryString["BackUrl"] != null)
                {
                    return RedirectToAction(Request.QueryString["BackUrl"].ToString());
                }
                //return RedirectToAction("Index", "Home");
                return Json(new { data = BranchID }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult ChangeSecurityRoleAction(int SecurityRoleId)
        {

            try
            {

                ViewBag.SecurityRoleId = new SelectList(db.securityRoles.Where(a => db.UserSecurityRoles.Where(b => b.UserID == SiteInfo.LoggedUserID && b.IsActive == true).Select(b => b.SecurityRoleID).Contains(a.ID)).ToList(), "ID", "Name", SiteInfo.LoggedSecurityRoleID);
                SiteInfo.LoggedSecurityRoleID = SecurityRoleId;
                SiteInfo.LoggedSecurityRoleName = db.securityRoles.FirstOrDefault(a => a.ID == SecurityRoleId).Name;

                //SiteInfo.LoggedSecurityRoleID = 1; //ToDo: Fixed security role 1 till implement it
                if (Request.QueryString["BackUrl"] != null)
                {
                    return RedirectToAction(Request.QueryString["BackUrl"].ToString());
                }
                //return RedirectToAction("Index", "Home");
                return Json(new { data = SecurityRoleId }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(login model)
        {

            //ViewBag.BranchID = new SelectList(db.Branches.ToList(), "ID", "Name");

            //var finsdUser = db.UserBranches.FirstOrDefault(u => u.BranchID == model.BranchID && u.User.LoginName.ToLower() == model.username.ToLower() && u.User.Password == model.password);
            var finsdUser = db.Users.FirstOrDefault(u => u.LoginName.ToLower() == model.username.ToLower() && u.Password == model.password);
            if (finsdUser == null)
            {
                ModelState.AddModelError("", "Error in username or password");
                return View();
            }

            SiteInfo.LoggedUserID = finsdUser.ID;
            SiteInfo.LoggedLoginName = finsdUser.LoginName;
            SiteInfo.LoggedUserName = finsdUser.FullName;
            
            SiteInfo.LoggedBranchNo = db.UserBranches.Any(a=>a.UserID== finsdUser.ID&&a.IsDefault==true)? db.UserBranches.Where(a => a.UserID == finsdUser.ID && a.IsDefault == true).FirstOrDefault().BranchID : db.UserBranches.Where(a => a.UserID == finsdUser.ID && a.IsActive == true).FirstOrDefault().BranchID;
            
            SiteInfo.LoggedBranchName = db.UserBranches.Any(a => a.UserID == finsdUser.ID && a.IsDefault == true) ? db.UserBranches.Where(a => a.UserID == finsdUser.ID && a.IsDefault == true).FirstOrDefault().Branch.Name : db.UserBranches.Where(a => a.UserID == finsdUser.ID && a.IsActive == true).FirstOrDefault().Branch.Name;
            
            SiteInfo.LoggedSecurityRoleID = db.UserSecurityRoles.Any(a => a.UserID == finsdUser.ID && a.IsDefault == true) ? db.UserSecurityRoles.Where(a => a.UserID == finsdUser.ID && a.IsDefault == true).FirstOrDefault().SecurityRoleID : db.UserSecurityRoles.Where(a => a.UserID == finsdUser.ID && a.IsActive == true).FirstOrDefault().SecurityRoleID;
            
            SiteInfo.LoggedSecurityRoleName = db.UserSecurityRoles.Any(a => a.UserID == finsdUser.ID && a.IsDefault == true) ? db.UserSecurityRoles.Where(a => a.UserID == finsdUser.ID && a.IsDefault == true).FirstOrDefault().SecurityRole.Name : db.UserSecurityRoles.Where(a => a.UserID == finsdUser.ID && a.IsActive == true).FirstOrDefault().SecurityRole.Name;
            if (Request.QueryString["BackUrl"] != null)
            {
                return RedirectToAction(Request.QueryString["BackUrl"].ToString());
            }
            return RedirectToAction("Index", "Home");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            SiteInfo.LoggedUserID = -1;
            SiteInfo.LoggedLoginName = "";
            SiteInfo.LoggedUserName = "";
            SiteInfo.LoggedSecurityRoleID = -1;
            SiteInfo.LoggedSecurityRoleName = "";
            SiteInfo.LoggedBranchNo = -1;
            SiteInfo.LoggedBranchName = "";

            //ToDo: Fixed security role 1 till implement it

            return RedirectToAction("Login", "Home");
        }

        
    }
}
