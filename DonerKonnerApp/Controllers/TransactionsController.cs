﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Inventory;
using Agi.DonerKonner.DataAccess.ViewModel;
using Agi.DonerKonner.Globalization;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class TransactionsController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();

        // GET: Transactions
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            return View();
        }

        public ActionResult getTrans()
        {
            try
            {
                if (!SiteInfo.IsLogged)
                    return RedirectToAction("Login", "Home");

                var vv = (from inv in db.InvOpenings
                          join itm in db.InvOpeningItems on inv.ID equals itm.InvOpeningID
                          where (inv.BranchID == SiteInfo.LoggedBranchNo)
                          select new
                          {
                              WarhouseName = inv.Warehouse.Name,
                              TransNo = inv.TransNo,
                              TransDate = inv.TransDate,
                              Notes = inv.Notes,
                              Amount=itm.UnitPrice*itm.Qty
                          }
                          
                          ).ToList();

                var match = (from t in vv
                         group t by new  { t.TransNo, t.TransDate,t.WarhouseName, t.Notes} into g
                         select new
                         {
                             WarhouseName = g.Key.WarhouseName,
                             TransNo = g.Key.TransNo,
                             TransDate = string.Format("{0:d/M/yyyy }", g.Key.TransDate) ,
                             Notes = g.Key.Notes,
                             TotAmount = g.Sum(a=>a.Amount),
                             itemsCount = g.Count()
                         }
                       );

                


                return Json(new { data = match }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }
        }
        public ActionResult Transactions()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            List<InvOpening> match = db.InvOpenings.Where(a=>a.BranchID==SiteInfo.LoggedBranchNo).ToList();
            return View(match);
        }

        public ActionResult AddTrans()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            InvOpening invOpening;
            ViewBag.title = Strings.Add+" "+Strings.InventoryOpenningBalance;
            ViewBag.pageheader = Strings.Homw+" > "+ Strings.InventoryOpenningBalance+" > "+ Strings.Add + " " + Strings.InventoryOpenningBalance;
            if (Request.QueryString["Transno"] != null)
            {
                int transno = int.Parse(Request.QueryString["Transno"].ToString());
                invOpening = db.InvOpenings.FirstOrDefault(a => a.TransNo == transno);

            }
            else
            {
                invOpening = new InvOpening
                {
                    TransNo =db.InvOpenings.Where(a=>a.BranchID==SiteInfo.LoggedBranchNo).Count()==0 ? 1: db.InvOpenings.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Max(a => a.TransNo) + 1,
                    TransDate = DateTime.Now.Date,
                };
            }

            BindDlls(invOpening);
            return View(invOpening);
        }

        public ActionResult UpdateTransView(string Transno)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            InvOpening invOpening;
            int transno = int.Parse(Request.QueryString["Transno"].ToString());
            invOpening = db.InvOpenings.FirstOrDefault(a => a.TransNo == transno);
            ViewBag.title = Strings.Edit + " " + Strings.InventoryOpenningBalance;
            ViewBag.pageheader = Strings.Homw + " > " + Strings.InventoryOpenningBalance + " > " + Strings.Edit + " " + Strings.InventoryOpenningBalance;

            

            BindDlls(invOpening);
            return View("AddTrans", invOpening);
        }


        public void BindDlls(InvOpening invOpening)
        {
            
            if (invOpening.ID != 0)
            {
                var data = db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo && !db.InvOpenings.Select(b => b.WarehouseID).Contains(a.ID)).ToList();
                data.Add(db.Warehouses.FirstOrDefault(a => a.ID == invOpening.WarehouseID));
                ViewBag.WarehouseID = new SelectList(data, "ID", "Name", invOpening.WarehouseID);

            }
            else
            {
                ViewBag.WarehouseID = new SelectList(db.Warehouses.Where(a=>a.BranchID==SiteInfo.LoggedBranchNo && !db.InvOpenings.Select(b=>b.WarehouseID).Contains(a.ID) ).ToList(), "ID", "Name", null);
                Session["InvOpenningGridData"] = new List<GridData>();
            }

            ViewBag.GroupID = new SelectList(db.ItemGroups.ToList(), "ID", "Name", null);
            
        }

        public ActionResult BindItemsDll(int? groupId)
        {
            try
            {
                var list = db.Items.Where(o => o.ItemGroupID == groupId && o.HasStock==true).ToList();

                //return Json(list);
                return Json(new { data = new SelectList(list, "Code", "Name", null) }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult BindItemsDataDll(string itemId)
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");
            try
            {
                if (itemId == null)
                    return Json(new { isValid = false });
                string unit = db.Items.FirstOrDefault(a => a.Code == itemId).Unit.Name;
                string name = db.Items.FirstOrDefault(a => a.Code == itemId).Name;
                string code = db.Items.FirstOrDefault(a => a.Code == itemId).Code;
                //var list = db.Units.Where(o => o.ID == db.Items.FirstOrDefault(a=>a.ID==itemId).UnitID).ToList();

                //return Json(list);
                return Json(new { code, name ,unit}, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult BindAddToGrid(int groupId, string itemCode, string ItemName, decimal Price, decimal Quantity, string unit,string Notes)
        {
            
            try
            {
                List<GridData> l = new List<GridData>();
                if (Session["InvOpenningGridData"] == null || ((List<GridData>)Session["InvOpenningGridData"]).Count < 1)
                {
                    l.Add(new GridData
                    {
                        GroupID = groupId,
                        ItemCode = itemCode,
                        ItemName = ItemName,
                        Price = Price,
                        Unit = unit,
                        Quantity = Quantity,
                        Notes = Notes
                    });
                    Session["InvOpenningGridData"] = l;
                }
                else
                {
                    l = (List<GridData>)Session["InvOpenningGridData"];
                    if (!l.Select(i => i.ItemCode).Contains(itemCode))
                    {
                        l.Add(new GridData
                        {
                            GroupID = groupId,
                            ItemCode = itemCode,
                            ItemName = ItemName,
                            Price = Price,
                            Unit = unit,
                            Quantity = Quantity,
                            Notes = Notes
                        });
                    }
                    else
                    {
                        foreach (var item in l)
                        {
                            if (item.ItemCode == itemCode)
                            {
                                item.Quantity = Quantity;
                                item.Price = Price;
                                item.Notes = Notes;
                            }
                        }
                        //GridData d = l.FirstOrDefault(a => a.ItemCode == itemId);
                        //d.Quantity = Quantity;
                        //d.Price = Price;
                        //l.Remove(l.FirstOrDefault(a => a.ItemCode == itemId));
                        //l.Add(d);

                    }
                    Session["InvOpenningGridData"] = l;
                }

                return Json(new { data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult fillDaataTable()
        {
            if (Session["InvOpenningGridData"] == null)
            {
                return Json(new { data = new List<GridData>() }, JsonRequestBehavior.AllowGet);
            }
            List<GridData> l = (List<GridData>)Session["InvOpenningGridData"];
            return Json(new { data = l }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult GetData(int? itemId)
        //{
        //    try
        //    {
        //        var list = db.InvOpenings.ToList();
        //        //var list = db.Units.Where(o => o.ID == db.Items.FirstOrDefault(a=>a.ID==itemId).UnitID).ToList();

        //        //return Json(list);
        //        return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        return Json(new { isValid = false });
        //    }
        //}

        public void Save(int code, DateTime date, int warehousID, string Notes)
        {
            try
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    if (db.InvOpenings.Any(inv => inv.TransNo == code))
                    {
                        InvOpening op = db.InvOpenings.FirstOrDefault(inv => inv.TransNo == code);
                        op.TransDate = date;
                        op.WarehouseID = warehousID;
                        op.Notes = Notes;
                        op.MD = DateTime.Now;
                        op.MB = SiteInfo.LoggedUserID;
                        op.BranchID = SiteInfo.LoggedBranchNo;
                        db.InvOpeningItems.RemoveRange(db.InvOpeningItems.Where(it => it.InvOpeningID == op.ID));
                        db.SaveChanges();
                        List<GridData> l = (List<GridData>)Session["InvOpenningGridData"];
                        List<InvOpeningItem> opitems = new List<InvOpeningItem>();
                        foreach (var item in l)
                        {
                            opitems.Add(new InvOpeningItem
                            {
                                InvOpeningID = op.ID,
                                ItemID =db.Items.FirstOrDefault(a=>a.Code== item.ItemCode).ID,
                                UnitID = db.Units.FirstOrDefault(a => a.Name == item.Unit).ID,
                                Qty = item.Quantity,
                                UnitPrice = item.Price,
                                Notes = item.Notes,
                                CD = DateTime.Now,
                                CB = SiteInfo.LoggedUserID
                            });

                        }

                        db.InvOpeningItems.AddRange(opitems);
                        db.SaveChanges();
                    }
                    else
                    {
                        InvOpening op = new InvOpening
                        {
                            TransNo = code,
                            TransDate = date,
                            WarehouseID = warehousID,
                            Notes = Notes,
                            CD = DateTime.Now,
                            CB = SiteInfo.LoggedUserID,
                            BranchID = SiteInfo.LoggedBranchNo,
                        };
                        db.InvOpenings.Add(op);
                        db.SaveChanges();
                        List<GridData> l = (List<GridData>)Session["InvOpenningGridData"];
                        List<InvOpeningItem> opitems = new List<InvOpeningItem>();
                        foreach (var item in l)
                        {
                            opitems.Add(new InvOpeningItem
                            {
                                InvOpeningID = op.ID,
                                ItemID = db.Items.FirstOrDefault(a => a.Code == item.ItemCode).ID,
                                UnitID = db.Units.FirstOrDefault(a => a.Name == item.Unit).ID,
                                Qty = item.Quantity,
                                UnitPrice = item.Price,
                                Notes = item.Notes,
                                CD = DateTime.Now,
                                CB = SiteInfo.LoggedUserID
                            });

                        }

                        db.InvOpeningItems.AddRange(opitems);
                    }
                    db.SaveChanges();
                    Session["InvOpenningGridData"] = null;
                    trans.Commit();
                }
            }
            catch (Exception ex)
            {

            }
        }


        public ActionResult DeleteTrans(int Transno)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                InvOpening v = db.InvOpenings.FirstOrDefault(a => a.TransNo == Transno);
                db.InvOpenings.Remove(v);
                db.SaveChanges();
                return Json(new { data = v }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult UpdateTrans(int Transno)
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");
            try
            {
                InvOpening v = db.InvOpenings.FirstOrDefault(a => a.TransNo == Transno);
                List<InvOpeningItem> list = db.InvOpeningItems.Where(a => a.InvOpeningID == v.ID).ToList();
                List<GridData> l = new List<GridData>();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        var result = db.Items.FirstOrDefault(a => a.ID == item.ItemID);
                        l.Add(new GridData
                        {
                            GroupID = result.ItemGroupID,
                            ItemCode = result.Code,
                            ItemName = item.Item.Name,
                            Price = item.UnitPrice,
                            Unit = item.Unit.Name,
                            Quantity = item.Qty,
                            Notes = item.Notes,
                        });
                    }
                    Session["InvOpenningGridData"] = l;
                }


                return Json(new { isValid = true });
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult showpopup()
        {
            return View();
        }

        public ActionResult UpdateitemsGrid(string ItemID)
        {
            
            try
            {
                if (Session["InvOpenningGridData"] != null)
                {
                    GridData gridDatas = ((List<GridData>)Session["InvOpenningGridData"]).FirstOrDefault(it => it.ItemCode == ItemID);
                    ViewBag.GroupID = new SelectList(db.ItemGroups.ToList(), "ID", "Name",null);
                    return Json(new { data = gridDatas }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {

            }
            return Json(new { isValid = false });
        }

        public void DeleteFromGrid(string ItemID)
        {
            if (Session["InvOpenningGridData"] != null)
            {
                List<GridData> gridDatas = (List<GridData>)Session["InvOpenningGridData"];
                gridDatas.Remove(gridDatas.FirstOrDefault(it => it.ItemCode == ItemID));
                Session["InvOpenningGridData"] = gridDatas;
            }
        }
        public ActionResult TransReport(FormCollection formCollection,int TransNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("TransNo", TransNo.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("rpt_InvOpenning", "Reports");
        }
    }
}