﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Inventory;
using Agi.DonerKonner.DataAccess.ViewModel;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class PhysicalCountController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: PhysicalCount
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            return View();
        }
        public ActionResult PhCountList()
        {
            
            try
            {
                var data = (from phc in db.InvPhysicalCounts
                            join phci in db.invPhysicalCountItems on phc.ID equals phci.InvPhysicalCountID
                            where (phc.BranchID == SiteInfo.LoggedBranchNo)
                            group phc by new { phc.ID, phc.Warehouse, phc.TransNo, phc.TransDate, phc.Notes } into grouped
                            select new
                            {
                                ID = grouped.Key.ID,
                                WarehouseName = grouped.Key.Warehouse.Name,
                                TransNo = grouped.Key.TransNo,
                                TransDate = grouped.Key.TransDate,
                                Count = grouped.Count(),
                                Notes = grouped.Key.Notes,


                            }).ToList().Select(a => new
                            {
                                ID = a.ID,
                                WarehouseName = a.WarehouseName,
                                TransNo = a.TransNo,
                                TransDate = a.TransDate.ToString("dd/MM/yyyy"),
                                itemsCount = a.Count,
                                Notes = a.Notes,
                            });

                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public void BindDlls(InvPhysicalCount invPhysicalCount)
        {

            if (db.InvPhysicalCounts.Any(i=> i.ID == invPhysicalCount.ID))
            {
                var data = db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo && !db.InvPhysicalCounts.Select(b => b.WarehouseID).Contains(a.ID)).ToList();
                data.Add(db.Warehouses.FirstOrDefault(a => a.ID == invPhysicalCount.WarehouseID));
                ViewBag.WarehouseID = new SelectList(data, "ID", "Name", invPhysicalCount.WarehouseID);

            }
            else
            {
                ViewBag.WarehouseID = new SelectList(db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo && !db.InvPhysicalCounts.Select(b => b.WarehouseID).Contains(a.ID)).ToList(), "ID", "Name", null);
                Session["InvPhCountitemGridData"] = new List<PhCountItemGrid>();
            }


        }

        public ActionResult Additem()
        {
            ViewBag.ItemList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Name", null);
            ViewBag.CodeList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Code", null);
            ViewBag.iscreate = true;
            Guid guid = Guid.NewGuid();
            String guidst = guid.ToString();
            ViewBag.guidval = guidst;

            return Json(new { IsValid = false, page = RenderRazorViewToString("ItemPopUp", null) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddItem(string guid, Boolean saveandnewFlag, int ItemId, string ItemName, string ItemCode, string UnitName, decimal Qty, string Notes)
        {
            if (((List<PhCountItemGrid>)Session["InvPhCountitemGridData"]).Any(it => it.ItemId == ItemId))
            {
                ViewBag.ItemList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Name", null);
                ViewBag.CodeList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Code", null);
                ViewBag.guid = guid;
                return Json(new { IsValid = false, saveandnewFlag, message = "This Item is Already in The Grid" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<PhCountItemGrid> list = (List<PhCountItemGrid>)Session["InvPhCountitemGridData"];
                list.Add(new PhCountItemGrid
                {
                    guid = guid,
                    ItemId = ItemId,
                    ItemName = ItemName,
                    ItemCode = ItemCode,
                    UnitName = UnitName,
                    Qty = Qty,
                    Notes = Notes
                }); ;
                Session["InvPhCountitemGridData"] = list;

                return Json(new { IsValid = true, saveandnewFlag }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult Edititem(string guid)
        {
            ViewBag.ItemList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Name", null);
            ViewBag.CodeList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Code", null);
            ViewBag.iscreate = false;

            PhCountItemGrid phCountItemGrid = ((List<PhCountItemGrid>)Session["InvPhCountitemGridData"]).FirstOrDefault(it => it.guid == guid);
            ViewBag.guidval = guid;
            int itemID = phCountItemGrid.ItemId;
            string itemName = phCountItemGrid.ItemName.ToString();
            string itemCode = phCountItemGrid.ItemCode.ToString();
            decimal qty = phCountItemGrid.Qty;
            string notes = phCountItemGrid.Notes.ToString();

            return Json(new { IsValid = false, ItemId = itemID, Qty = qty, Notes = notes, page = RenderRazorViewToString("ItemPopUp", null) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edititem(string guid, Boolean saveandnewFlag, int ItemId, string ItemName, string ItemCode, string UnitName, decimal Qty, string Notes)
        {
            if (((List<PhCountItemGrid>)Session["InvPhCountitemGridData"]).Any(it => it.ItemId == ItemId && it.guid != guid))
            {
                ViewBag.ItemList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Name", null);
                ViewBag.CodeList = new SelectList(db.Items.Where(o => o.HasStock == true).ToList(), "ID", "Code", null);
                ViewBag.guidval = guid;
                return Json(new { IsValid = false, saveandnewFlag, ItemId, Qty, Notes, message = "This Item is Already in The Grid" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                PhCountItemGrid phCountItemGrid = ((List<PhCountItemGrid>)Session["InvPhCountitemGridData"]).FirstOrDefault(it => it.guid == guid);
                phCountItemGrid.guid = guid;

                phCountItemGrid.ItemId = ItemId;
                phCountItemGrid.ItemName = ItemName.ToString();
                phCountItemGrid.ItemCode = ItemCode.ToString();
                phCountItemGrid.UnitName = UnitName.ToString();
                phCountItemGrid.Qty = Qty;
                phCountItemGrid.Notes = Notes.ToString();
                return Json(new { IsValid = true, saveandnewFlag = true }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult fillDataTable()
        {
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");


            if (Session["InvPhCountitemGridData"] == null)
            {
                return Json(new { data = new List<PhCountItemGrid>() }, JsonRequestBehavior.AllowGet);
            }
            List<PhCountItemGrid> PhCountList = (List<PhCountItemGrid>)Session["InvPhCountitemGridData"];
            return Json(new { data = PhCountList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult bindItemsDataDll(int ItemId)
        {
            try
            {
                Item item = db.Items.FirstOrDefault(o => o.ID == ItemId && o.HasStock == true);

                var unitName = item.Unit.Name;

                return Json(new { unitName }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult ConfirmationDeletePopup(string guid)
        {
            
            ViewBag.guid = guid;
            ViewBag.TransNo = 1;
            ViewBag.del = true;
            return Json(new { IsValid = false, page = RenderRazorViewToString("DeletePopup", null) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConfirmationDeletePhPopup(int TransNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            ViewBag.guid = "1";
            ViewBag.TransNo = TransNo;
            ViewBag.del = false;
            return Json(new { IsValid = false, page = RenderRazorViewToString("DeletePopup", null) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteItem(string guid)
        {
            try
            {
                guid = (guid ?? "").Trim().ToLower();

                List<PhCountItemGrid> list = (List<PhCountItemGrid>)Session["InvPhCountitemGridData"];
              
                PhCountItemGrid phCountItemGrid = list.Where(it => !string.IsNullOrWhiteSpace(guid) && (it.ItemCode ?? "").Trim().ToLower() == guid).FirstOrDefault();
                
                list.Remove(phCountItemGrid);

                Session["InvPhCountitemGridData"] = list;
                return Json(new { data = list }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        [HttpPost]
        public ActionResult DeletePhItem(int TransNo)
        {
            
            try
            {
                InvPhysicalCount invPhysicalCount = db.InvPhysicalCounts.FirstOrDefault(it => it.TransNo == TransNo);
                db.InvPhysicalCounts.Remove(invPhysicalCount);
                db.SaveChanges();
                return Json(new { }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult AddPhCount()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            ViewBag.isAdd = true;
            //InvPhysicalCount invPhysicalCount;

            //if (Request.QueryString["Transno"] != null)
            //{
            //    int transno = int.Parse(Request.QueryString["Transno"].ToString());
            //    invPhysicalCount = db.InvPhysicalCounts.FirstOrDefault(a => a.TransNo == transno);

            //}
            //else
            //{

            InvPhysicalCount invPhysicalCount = new InvPhysicalCount();
            //{
            ViewBag.TransNo = db.InvPhysicalCounts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Count() == 0 ? 1 : db.InvPhysicalCounts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Max(a => a.TransNo) + 1;
            ViewBag.TransDate = db.GetServerDate();
            
            //};
            //}

            BindDlls(invPhysicalCount);
            return View();

        }
        [HttpPost]
        public ActionResult AddPhCount(Boolean AddandNew ,int WarehouseId, int TransNo, DateTime TransDate, string PhNotes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.InvPhysicalCounts.Any(it => it.TransDate == TransDate && it.BranchID == SiteInfo.LoggedBranchNo) || TransDate > db.GetServerDate())
                {

                    return Json(new { IsValid = false, message = "Date is InValid" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    InvPhysicalCount invPhysicalCount = new InvPhysicalCount
                    {
                        BranchID = SiteInfo.LoggedBranchNo,
                        WarehouseID = WarehouseId,
                        TransNo = TransNo,
                        TransDate = TransDate,
                        Notes = PhNotes,
                        CB = SiteInfo.LoggedUserID,
                        CD = db.GetServerDate()
                    };
                    db.InvPhysicalCounts.Add(invPhysicalCount);
                    db.SaveChanges();
                    var invphcount = db.InvPhysicalCounts.FirstOrDefault(i => i.TransDate == TransDate);
                    List<PhCountItemGrid> list = (List<PhCountItemGrid>)Session["InvPhCountitemGridData"];
                    List<InvPhysicalCountItem> invPhysicalCountItems = new List<InvPhysicalCountItem>();
                    foreach (var item in list)
                    {
                        var unit = db.Units.FirstOrDefault(o => o.Name == item.UnitName);
                        invPhysicalCountItems.Add(new InvPhysicalCountItem
                        {

                            InvPhysicalCountID = invphcount.ID,
                            ItemID = item.ItemId,
                            UnitID = unit.ID,
                            Qty = item.Qty,
                            Notes = item.Notes,
                            CB = SiteInfo.LoggedUserID,
                            CD = db.GetServerDate()

                        });

                    }
                    db.invPhysicalCountItems.AddRange(invPhysicalCountItems);
                    db.SaveChanges();
                    Session["InvPhCountitemGridData"] = null;

                    if (AddandNew) {
                        ViewBag.TransNo = db.InvPhysicalCounts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Count() == 0 ? 1 : db.InvPhysicalCounts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Max(a => a.TransNo) + 1;
                        ViewBag.TransDate = db.GetServerDate();
                        InvPhysicalCount invPhysicalCountt = new InvPhysicalCount();
                        BindDlls(invPhysicalCountt);
                        return Json(new { isValid = true });
                    }
                    else
                    {
                        return Json(new { isValid = true});
                    }
                    
                }
            }
            catch (Exception ex)
            {
                return Json(new { isValid = true });
            }

            

        }

        public ActionResult EditPhCount(int TransNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            ViewBag.isAdd = false;
            InvPhysicalCount invPhysicalCount;
    
            invPhysicalCount = db.InvPhysicalCounts.FirstOrDefault(a => a.TransNo == TransNo);
            ViewBag.TransDate = invPhysicalCount.TransDate;
            List<InvPhysicalCountItem> list = db.invPhysicalCountItems.Where(a => a.InvPhysicalCountID == invPhysicalCount.ID).ToList();
            List<PhCountItemGrid> itemList = new List<PhCountItemGrid>();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    Guid guid = Guid.NewGuid();
                    String guidst = guid.ToString();
                    itemList.Add(new PhCountItemGrid
                    {
                    guid = guidst,
                    ItemId = item.ItemID,
                    ItemName = item.Item.Name,
                    ItemCode = item.Item.Code,
                    UnitName = item.Unit.Name,
                    Qty = item.Qty,
                    Notes = item.Notes
                    });
                }
                Session["InvPhCountitemGridData"] = itemList;
            }
            BindDlls(invPhysicalCount);
            
            ViewBag.AddandNew = false;
            return View("AddPhCount" , invPhysicalCount );

        }
        [HttpPost]
        public ActionResult EditPhCount(Boolean AddandNew, int WarehouseId, int TransNo, DateTime TransDate, string PhNotes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.InvPhysicalCounts.Any(it => it.TransDate == TransDate && it.BranchID == SiteInfo.LoggedBranchNo) || TransDate > db.GetServerDate())
                {
                    InvPhysicalCount invPhysicalCount = db.InvPhysicalCounts.Where(it => it.TransDate == TransDate).FirstOrDefault();
                    if (invPhysicalCount.TransNo != TransNo)
                    {
                        return Json(new { IsValid = false, message = "Date is InValid" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        invPhysicalCount.BranchID = SiteInfo.LoggedBranchNo;
                        invPhysicalCount.WarehouseID = WarehouseId;
                        invPhysicalCount.TransNo = TransNo;
                        invPhysicalCount.TransDate = TransDate;
                        invPhysicalCount.Notes = PhNotes;
                        invPhysicalCount.MB = SiteInfo.LoggedUserID;
                        invPhysicalCount.MD = db.GetServerDate();

                        db.invPhysicalCountItems.RemoveRange(db.invPhysicalCountItems.Where(it => it.InvPhysicalCountID == invPhysicalCount.ID));

                        db.SaveChanges();

                        List<PhCountItemGrid> list = (List<PhCountItemGrid>)Session["InvPhCountitemGridData"];
                        List<InvPhysicalCountItem> invPhysicalCountItems = new List<InvPhysicalCountItem>();
                        foreach (var item in list)
                        {
                            var unit = db.Units.FirstOrDefault(o => o.Name == item.UnitName);
                            invPhysicalCountItems.Add(new InvPhysicalCountItem
                            {

                                InvPhysicalCountID = invPhysicalCount.ID,
                                ItemID = item.ItemId,
                                UnitID = unit.ID,
                                Qty = item.Qty,
                                Notes = item.Notes,
                                CB = SiteInfo.LoggedUserID,
                                CD = db.GetServerDate()

                            });

                        }
                        db.invPhysicalCountItems.AddRange(invPhysicalCountItems);
                        db.SaveChanges();
                        Session["InvPhCountitemGridData"] = null;

                        if (AddandNew)
                        {
                            ViewBag.TransNo = db.InvPhysicalCounts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Count() == 0 ? 1 : db.InvPhysicalCounts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Max(a => a.TransNo) + 1;
                            ViewBag.TransDate = db.GetServerDate();
                            InvPhysicalCount invPhysicalCountt = new InvPhysicalCount();
                            BindDlls(invPhysicalCountt);
                            return Json(new { isValid = true });
                        }
                        else
                        {
                            return Json(new { isValid = true });
                        }
                    }
                }
                
                return Json(new { isValid = true });
            }
            catch (Exception ex)
            {
                return Json(new { isValid = true });
            }



        }

        public ActionResult PhysicalCountReport(FormCollection formCollection, int TransNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("TransNo", TransNo.ToString());
            formCollection.Add("BranchId", SiteInfo.LoggedBranchNo.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("PhysicalCountItemReort", "Reports");
        }
    }
}