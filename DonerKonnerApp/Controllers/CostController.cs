﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.ViewModel;
using DonerKonner.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class CostController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();

        // GET: Cost
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            ViewBag.DateTime = db.GetServerDate();
            
            return View();
        }

        public ActionResult Search(string FromDate, string ToDate)
        {
            try
            {
                DateTime fDate = DateTime.Now;
                DateTime tDate = DateTime.Now;
                DateTime.TryParse(FromDate, out fDate);
                DateTime.TryParse(ToDate, out tDate);

                int BranchID = SiteInfo.LoggedBranchNo;
                List<CostViewModel> costReportList = db.CostReportList(fDate, tDate, BranchID);


                return Json(new { data = costReportList }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult CostReport(string FromDate, string ToDate)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            FormCollection formCollection = new FormCollection();
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("Branch_Name", SiteInfo.LoggedBranchName.ToString());
            formCollection.Add("FromDate", FromDate.ToString());
            formCollection.Add("ToDate", ToDate.ToString());
            formCollection.Add("BranchID", SiteInfo.LoggedBranchNo.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("CostReport", "Reports");
        }

        public ActionResult ExportToExcel(string FromDate, string ToDate)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            DateTime fDate = DateTime.Now;
            DateTime tDate = DateTime.Now;
            DateTime.TryParse(FromDate, out fDate);
            DateTime.TryParse(ToDate, out tDate);

            int BranchID = SiteInfo.LoggedBranchNo;
            List<CostViewModel> FileData = db.CostReportList(fDate, tDate, BranchID);
            try
            {

                DataTable Dt = new DataTable();
                Dt.Columns.Add("SN", typeof(int));
                Dt.Columns.Add("Code", typeof(string));
                Dt.Columns.Add("Name", typeof(string));
                Dt.Columns.Add("Price", typeof(decimal));
                Dt.Columns.Add("PriceNoVat", typeof(decimal));
                Dt.Columns.Add("RawMaterial", typeof(decimal));
                Dt.Columns.Add("RawMaterialPercent", typeof(decimal));
                Dt.Columns.Add("Packing", typeof(decimal));
                Dt.Columns.Add("PackingPercent", typeof(decimal));
                Dt.Columns.Add("Total", typeof(decimal));
                Dt.Columns.Add("TotalPercent", typeof(decimal));
                Dt.Columns.Add("Revenue", typeof(decimal));
                Dt.Columns.Add("RevenuePercent", typeof(decimal));
                

                int count = 1;
                foreach (var data in FileData)
                {
                    DataRow row = Dt.NewRow();
                    row[0] = count;
                    row[1] = data.Code;
                    row[2] = data.Name;
                    row[3] = data.Price;
                    row[4] = data.PriceNoVat;
                    row[5] = data.RawMaterial;
                    row[6] = data.RawMaterialPercent;
                    row[7] = data.Packing;
                    row[8] = data.PackingPercent;
                    row[9] = data.Total;
                    row[10] = data.TotalPercent;
                    row[11] = data.Revenue;
                    row[12] = data.RevenuePercent;
                    count++;
                    Dt.Rows.Add(row);

                }
                var memoryStream = new MemoryStream();
                using (var excelPackage = new ExcelPackage(memoryStream))
                {
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Sheet1");

                    count += 6;
                    worksheet.Cells["B6"].LoadFromDataTable(Dt, true);
                    worksheet.DefaultRowHeight = 20;
                    worksheet.Column(1).Width = 1.5;

                    #region Header
                    int RowIndex = 0;
                    int ColIndex = 12;

                    Image img = Image.FromFile(Server.MapPath("\\Images\\DonerKonner.png"));
                    ExcelPicture pic = worksheet.Drawings.AddPicture("DonerKonner.png", img);
                    pic.SetPosition(RowIndex, 0, ColIndex, 12);
                    pic.SetSize(130, 50);

                    var mercell2 = worksheet.Cells["B1:C1"];
                    mercell2.Merge = true;
                    mercell2.Value = "Donner Konner";
                    mercell2.Style.Font.Size = 13;
                    mercell2.Style.Font.Bold = true;
                    mercell2.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                    var mercell3 = worksheet.Cells["G1:I1"];
                    mercell3.Merge = true;
                    mercell3.Value = "Branch Name: " + " " + SiteInfo.LoggedBranchName;
                    mercell3.Style.Font.Size = 13;
                    mercell3.Style.Font.Bold = true;
                    mercell3.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells["B2:N2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells["B2:N2"].Style.Border.Bottom.Color.SetColor(Color.DarkGray);

                    var mercell = worksheet.Cells["G4:I4"];
                    mercell.Merge = true;
                    mercell.Style.Font.Bold = true;
                    mercell.Style.Font.Size = 15;
                    mercell.Value = "Cost Report";
                    mercell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    #endregion

                    #region Table
                    worksheet.Cells["B6:N6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                    worksheet.Cells["B6:N6"].Style.Font.Bold = true;
                    worksheet.Cells["B6:N6"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells["B6:N6"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(173, 216, 230));
                    worksheet.Cells["B6:N" + count].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:N" + count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:N" + count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:N" + count].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B7:N" + count].Style.Border.Top.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:N" + count].Style.Border.Bottom.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:N" + count].Style.Border.Right.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:N" + count].Style.Border.Left.Color.SetColor(Color.DarkGray);

                    worksheet.Cells["F6"].Value = "Price (No Vat)";
                    worksheet.Cells["G6"].Value = "Raw Material";
                    worksheet.Cells["H6"].Value = "%";
                    worksheet.Cells["J6"].Value = "%";
                    worksheet.Cells["L6"].Value = "%";
                    worksheet.Cells["N6"].Value = "%";

                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 8;
                    worksheet.Column(8).Width = 10;
                    worksheet.Column(10).Width = 10;
                    worksheet.Column(12).Width = 10;
                    worksheet.Column(14).Width = 10;
                    
                    worksheet.Cells["N7:N" + count].Style.Numberformat.Format = "#,###,##";

                    worksheet.Cells["B6:N" + count].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


                    #endregion

                    worksheet.DefaultColWidth = 18;
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    worksheet.Column(5).AutoFit();
                    worksheet.Column(6).AutoFit();
                    worksheet.Column(7).AutoFit();
                    worksheet.Column(9).AutoFit();
                    worksheet.Column(11).AutoFit();
                    worksheet.Column(13).AutoFit();

                    Session["DownloadExcel_CostReport"] = excelPackage.GetAsByteArray();
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ActionResult Download()
        {


            if (Session["DownloadExcel_CostReport"] != null)
            {
                byte[] data = Session["DownloadExcel_CostReport"] as byte[];
                return File(data, "application/octet-stream", "CostReport.xlsx");
            }
            else
            {
                return new EmptyResult();
            }
        }
    }
}