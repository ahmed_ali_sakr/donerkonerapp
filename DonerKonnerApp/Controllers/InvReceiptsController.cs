﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Enum;
using Agi.DonerKonner.DataAccess.Inventory;
using Agi.DonerKonner.DataAccess.ViewModel;
using Agi.DonerKonner.Globalization;
using Agi.DonerKonner.Helpers;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class InvReceiptsController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: InvReceipt
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            return View();
        }
        public ActionResult getTrans(float val)
        {
            try
            {
                if (!SiteInfo.IsLogged)
                    return RedirectToAction("Login", "Home");
                var vv = (from inv in db.InvReceipts
                          join itm in db.InvReceiptItems on inv.ID equals itm.InvReceiptID
                          where (inv.BranchID == SiteInfo.LoggedBranchNo)
                          select new
                          {
                              WarhouseName = inv.Warehouse.Name,
                              TransNo = inv.TransNo,
                              TransDate = inv.TransDate,
                              Notes = inv.Notes,
                              Amount = (itm.UnitPrice * itm.Qty ),
                              Vat=inv.VatValue
                          }

                          ).ToList();

                var match = (from t in vv
                             group t by new { t.TransNo, t.TransDate, t.WarhouseName,t.Vat, t.Notes } into g
                             select new
                             {
                                 WarhouseName = g.Key.WarhouseName,
                                 TransNo = g.Key.TransNo,
                                 TransDate = string.Format("{0:d/M/yyyy }", g.Key.TransDate),
                                 Notes = g.Key.Notes,
                                 TAmount = (g.Sum(a => a.Amount))+ (g.Key.Vat),
                                 itemsCount = g.Count()
                             }
                       );
                //var match = db.InvReceipts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Select(a => new
                //{
                //    WarhouseName = a.Warehouse.Name,
                //    TransNo = a.TransNo,
                //    TransDate = a.TransDate,
                //    Notes = a.Notes,
                //    itemsCount = db.InvReceiptItems.Where(i => i.InvReceiptID == a.ID).Count()
                //}).ToList()
                //.Select(b => new
                //{
                //    WarhouseName = b.WarhouseName,
                //    TransNo = b.TransNo,
                //    TransDate = string.Format("{0:d/M/yyyy }", b.TransDate),
                //    itemsCount = b.itemsCount,
                //    Notes = b.Notes
                //}).ToList();


                return Json(new { data = match }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }
        }

        public ActionResult DeleteTrans(int Transno)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                InvReceipt v = db.InvReceipts.FirstOrDefault(a => a.TransNo == Transno);
                db.InvReceipts.Remove(v);
                db.SaveChanges();
                return Json(new { data = v }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult UpdateTrans(int Transno)
        {
            
            try
            {
                InvReceipt v = db.InvReceipts.FirstOrDefault(a => a.TransNo == Transno);
                List<InvReceiptItem> list = db.InvReceiptItems.Where(a => a.InvReceiptID == v.ID).ToList();
                List<GridData> l = new List<GridData>();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        var result = db.Items.FirstOrDefault(a => a.ID == item.ItemID);
                        l.Add(new GridData
                        {
                            GroupID = result.ItemGroupID,
                            ItemCode = result.Code,
                            ItemName = item.Item.Name,
                            Price = item.UnitPrice,
                            Unit = item.Unit.Name,
                            Quantity = item.Qty,
                            Notes = item.Notes,


                        });
                    }
                    Session["InvReceiptGridData"] = l;
                }


                return Json(new { isValid = true });
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult AddTrans()
        {

            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            InvReceipt invReceipt;
            ViewBag.title = Strings.Add + " " + Strings.InvReceipts;
            ViewBag.pageheader = Strings.Homw + " > " + Strings.InvReceipts + " > " + Strings.Add + " " + Strings.InvReceipts;
            invReceipt = new InvReceipt
            {
                    TransNo = db.InvReceipts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Count() == 0 ? 1 : db.InvReceipts.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Max(a => a.TransNo) + 1,
                    TransDate = db.GetServerDate(),
                    Vat=14
                };
             BindDlls(invReceipt);
            return View(invReceipt);
        }
        public void BindDlls(InvReceipt invReceipt)
        {
            
            var BuyByID = EnumHelper.GetEnumList(typeof(BuyByEnum));
            if (invReceipt.ID != 0)
            {
                var data = db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo && !db.InvReceipts.Select(b => b.WarehouseID).Contains(a.ID)).ToList();
                data.Add(db.Warehouses.FirstOrDefault(a => a.ID == invReceipt.WarehouseID));
               
                ViewBag.WarehouseID = new SelectList(data, "ID", "Name", invReceipt.WarehouseID);

                ViewBag.BuyByID = new SelectList(BuyByID, "Value", "Text", invReceipt.BuyByID);
                ViewBag.VendorID = new SelectList(db.Vendors.ToList(), "ID", "Name", invReceipt.VendorID);

            }
            else
            {
                ViewBag.WarehouseID = new SelectList(db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo && !db.InvOpenings.Select(b => b.WarehouseID).Contains(a.ID)).ToList(), "ID", "Name", null);
                Session["InvReceiptGridData"] = new List<GridData>();
                ViewBag.BuyByID = new SelectList(BuyByID, "Value", "Text", null);
                ViewBag.VendorID = new SelectList(db.Vendors.ToList(), "ID", "Name",null);
            }

            ViewBag.GroupID = new SelectList(db.ItemGroups.ToList(), "ID", "Name", null);
            //Session["InvReceiptGridData"] = new List<GridData>();
            /// ViewBag.ItemID2 = new SelectList(db.Items.ToList(), "ID", "Name", null);
        }
        public ActionResult fillDaataTable()
        {
            
            if (Session["InvReceiptGridData"] == null)
            {
                return Json(new { data = new List<GridData>() }, JsonRequestBehavior.AllowGet);
            }
            List<GridData> l = (List<GridData>)Session["InvReceiptGridData"];
            ViewBag.totalQTY = l.Sum(a => a.Quantity);
            return Json(new { data = l }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BindItemsDll(int? groupId)
        {
            try
            {
                var list = db.Items.Where(o => o.ItemGroupID == groupId && o.HasStock == true).ToList();

                //return Json(list);
                return Json(new { data = new SelectList(list, "Code", "Name", null) }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult BindItemsDataDll(string itemId)
        {
            
            try
            {
                if (itemId == null)
                    return Json(new { isValid = false });
                string unit = db.Items.FirstOrDefault(a => a.Code == itemId).Unit.Name;
                string name = db.Items.FirstOrDefault(a => a.Code == itemId).Name;
                string code = db.Items.FirstOrDefault(a => a.Code == itemId).Code;
                //var list = db.Units.Where(o => o.ID == db.Items.FirstOrDefault(a=>a.ID==itemId).UnitID).ToList();

                //return Json(list);
                return Json(new { code, name, unit }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult BindAddToGrid(int groupId, string itemCode, string ItemName, decimal Price, decimal Quantity, string unit, string Notes)
        {
            
            try
            {
                List<GridData> l = new List<GridData>();
                if (Session["InvReceiptGridData"] == null || ((List<GridData>)Session["InvReceiptGridData"]).Count < 1)
                {
                    l.Add(new GridData
                    {
                        GroupID = groupId,
                        ItemCode = itemCode,
                        ItemName = ItemName,
                        Price = Price,
                        Unit = unit,
                        Quantity = Quantity,
                        Notes = Notes
                    });
                    Session["InvReceiptGridData"] = l;
                }
                else
                {
                    l = (List<GridData>)Session["InvReceiptGridData"];
                    if (!l.Select(i => i.ItemCode).Contains(itemCode))
                    {
                        l.Add(new GridData
                        {
                            GroupID = groupId,
                            ItemCode = itemCode,
                            ItemName = ItemName,
                            Price = Price,
                            Unit = unit,
                            Quantity = Quantity,
                            Notes = Notes
                        });
                    }
                    else
                    {
                        foreach (var item in l)
                        {
                            if (item.ItemCode == itemCode)
                            {
                                item.Quantity = Quantity;
                                item.Price = Price;
                                item.Notes = Notes;
                            }
                        }
                        //GridData d = l.FirstOrDefault(a => a.ItemCode == itemId);
                        //d.Quantity = Quantity;
                        //d.Price = Price;
                        //l.Remove(l.FirstOrDefault(a => a.ItemCode == itemId));
                        //l.Add(d);

                    }
                    Session["InvReceiptGridData"] = l;
                }

                return Json(new { data = l }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult Save(int code, DateTime date, int warehousID, string Notes,int BuyByID, int? VendorID,int Vat,int VatValue)
        {
            
            try
            {
                if(Session["InvReceiptGridData"]==null || ((List<GridData>)Session["InvReceiptGridData"]).Count == 0)
                {
                    return Json(new { isValid = false });
                }
                using (var trans = db.Database.BeginTransaction())
                {
                    if (db.InvReceipts.Any(inv => inv.TransNo == code))
                    {
                        InvReceipt op = db.InvReceipts.FirstOrDefault(inv => inv.TransNo == code);
                        op.TransDate = date;
                        op.WarehouseID = warehousID;
                        op.Notes = Notes;
                        op.MD = db.GetServerDate();
                        op.MB = SiteInfo.LoggedUserID;
                        op.BranchID = SiteInfo.LoggedBranchNo;
                        op.BuyByID = BuyByID;
                        op.VendorID = VendorID;
                        op.Vat = Vat;
                        op.VatValue = VatValue;
                        db.InvReceiptItems.RemoveRange(db.InvReceiptItems.Where(it => it.InvReceiptID == op.ID));
                        db.SaveChanges();
                        List<GridData> l = (List<GridData>)Session["InvReceiptGridData"];
                        List<InvReceiptItem> opitems = new List<InvReceiptItem>();
                        foreach (var item in l)
                        {
                            opitems.Add(new InvReceiptItem
                            {
                                InvReceiptID = op.ID,
                                ItemID = db.Items.FirstOrDefault(a => a.Code == item.ItemCode).ID,
                                UnitID = db.Units.FirstOrDefault(a => a.Name == item.Unit).ID,
                                Qty = item.Quantity,
                                UnitPrice = item.Price,
                                Notes = item.Notes,
                                CD = db.GetServerDate(),
                                CB = SiteInfo.LoggedUserID
                            });

                        }

                        db.InvReceiptItems.AddRange(opitems);
                        db.SaveChanges();
                    }
                    else
                    {
                        InvReceipt op = new InvReceipt
                        {
                            TransNo = code,
                            TransDate = date,
                            WarehouseID = warehousID,
                            Notes = Notes,
                            CD = db.GetServerDate(),
                            CB = SiteInfo.LoggedUserID,
                            BranchID = SiteInfo.LoggedBranchNo,
                            BuyByID = BuyByID,
                            VendorID = VendorID,
                            Vat = Vat,
                            VatValue = VatValue
                    };
                        db.InvReceipts.Add(op);
                        db.SaveChanges();
                        List<GridData> l = (List<GridData>)Session["InvReceiptGridData"];
                        List<InvReceiptItem> opitems = new List<InvReceiptItem>();
                        foreach (var item in l)
                        {
                            opitems.Add(new InvReceiptItem
                            {
                                InvReceiptID = op.ID,
                                ItemID = db.Items.FirstOrDefault(a => a.Code == item.ItemCode).ID,
                                UnitID = db.Units.FirstOrDefault(a => a.Name == item.Unit).ID,
                                Qty = item.Quantity,
                                UnitPrice = item.Price,
                                Notes = item.Notes,
                                CD = db.GetServerDate(),
                                CB = SiteInfo.LoggedUserID
                            });

                        }

                        db.InvReceiptItems.AddRange(opitems);
                    }
                    db.SaveChanges();
                    Session["InvReceiptGridData"] = null;
                    trans.Commit();
                }
                return Json(new { isValid = true },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult UpdateitemsGrid(string ItemID)
        {
            
            try
            {
                if (Session["InvReceiptGridData"] != null)
                {
                    GridData gridDatas = ((List<GridData>)Session["InvReceiptGridData"]).FirstOrDefault(it => it.ItemCode == ItemID);
                    ViewBag.GroupID = new SelectList(db.ItemGroups.ToList(), "ID", "Name", null);
                    return Json(new { data = gridDatas }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {

            }
            return Json(new { isValid = false });
        }

        public ActionResult UpdateTransView(string Transno)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");
            InvReceipt invReceipt;
            int transno = int.Parse(Request.QueryString["Transno"].ToString());
            invReceipt = db.InvReceipts.FirstOrDefault(a => a.TransNo == transno);
            ViewBag.title = Strings.Edit + " " + Strings.InvReceipts;
            ViewBag.pageheader = Strings.Homw + " > " + Strings.InvReceipts + " > " + Strings.Edit + " " + Strings.InvReceipts;



            BindDlls(invReceipt);
            return View("AddTrans", invReceipt);
        }

        public ActionResult DeleteFromGrid(string ItemID)
        {
            
            try
            {
                if (Session["InvReceiptGridData"] != null)
                {
                    List<GridData> gridDatas = (List<GridData>)Session["InvReceiptGridData"];
                    gridDatas.Remove(gridDatas.FirstOrDefault(it => it.ItemCode == ItemID));
                    Session["InvReceiptGridData"] = gridDatas;
                    return Json(new { data = gridDatas }, JsonRequestBehavior.AllowGet);
                }else
                    return Json(new { isValid = false });
            }
            catch
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult TransReport(FormCollection formCollection, int TransNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            if (!SiteInfo.IsLogged)
                return RedirectToAction("Login", "Home");
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("TransNo", TransNo.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("InvReceipt", "Reports");
        }
    }
}