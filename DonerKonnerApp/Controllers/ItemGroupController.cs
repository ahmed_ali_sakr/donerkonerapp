﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Inventory;
using DonerKonner.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agi.DonerKonner.Controllers
{
    public class ItemGroupController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: Unit
        public ActionResult Index()
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            return View();
        }
        public ActionResult GetItemGroups()

        {
            try
            {
                var itemGroups = db.ItemGroups.Select(a => new
                {
                    itemgroupCode = a.Code,
                    itemgroupName = a.Name,
                    Notes = a.Notes,
                    itemgroupNo = a.ID

                }).ToList();

                return Json(new { data = itemGroups }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        [HttpPost]
        public ActionResult DeleteItemGroup(int itemgroupNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                ItemGroup itemgroup = db.ItemGroups.FirstOrDefault(a => a.ID == itemgroupNo);
                db.ItemGroups.Remove(itemgroup);
                db.SaveChanges();
                return Json(new { data = itemgroup }, JsonRequestBehavior.AllowGet);

            }
            catch (System.Exception ex)
            {

                return Json(new { isValid = false });
            }

        }

        [HttpPost]
        public ActionResult Save(String Code, String Name, String Notes)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                if (db.ItemGroups.Any(u => u.Name == Name || u.Code == Code))
                {
                    ItemGroup itemgroup = db.ItemGroups.FirstOrDefault(u => u.Name == Name);
                    itemgroup.Code = Code;
                    itemgroup.Name = Name;
                    itemgroup.Notes = Notes;
                    itemgroup.MB = SiteInfo.LoggedUserID;
                    itemgroup.MD = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ItemGroup itemgroup = new ItemGroup
                    {
                        Code = Code,
                        Name = Name,
                        Notes = Notes,
                        CB = SiteInfo.LoggedUserID,
                        CD = DateTime.Now,

                    };
                    db.ItemGroups.Add(itemgroup);
                    db.SaveChanges();
                    return Json(new { }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { isValid = false });
            }
        }

        public ActionResult EditItemGroup(int itemgroupNo)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            try
            {
                var itemGroup = db.ItemGroups.FirstOrDefault(u => u.ID == itemgroupNo);

                return Json(new { data = new { itemGroup.Code, itemGroup.Name, itemGroup.Notes } }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { isValid = false });
            }
        }
        public ActionResult ItemGroupReport(FormCollection formCollection)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            Session["parameters"] = formCollection;
            return RedirectToAction("ItemGroupReport", "Reports");
        }
    }
}