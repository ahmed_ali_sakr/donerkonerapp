﻿using Agi.DonerKonner.DataAccess;
using Agi.DonerKonner.DataAccess.Inventory;
using Agi.DonerKonner.DataAccess.ViewModel;
using DonerKonner.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Drawing;
using OfficeOpenXml.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Utils;
using DevExpress.XtraSpreadsheet.Model;
using System.Net;

namespace Agi.DonerKonner.Controllers
{
    public class InventoryCountController : BasicCRUDController
    {
        private DonerKonnerContext db = new DonerKonnerContext();
        // GET: InventoryCount
        public ActionResult Index(bool flag)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserAllTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            ViewBag.DateTime = db.GetServerDate();
            var data = db.Warehouses.Where(a => a.BranchID == SiteInfo.LoggedBranchNo).Select(x => new { ID = x.ID, Name = x.Name }).ToList();
            ViewBag.WarehouseID = new SelectList(data, "ID", "Name", null);
            ViewBag.Flag = flag;
            return View();
        }
        public ActionResult Search(string AsOfDate, int? WareHouseID)
        {
            try
            {
                
                DateTime asofdate;
                DateTime.TryParse(AsOfDate, out asofdate);
                List<InventoryCountReportViewModel> inventoryCountViewModel = db.InventoryCountReport(asofdate, WareHouseID);
                return Json(new { data = inventoryCountViewModel }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult InventoryCountReport(string AsOfDate, int? WareHouseID)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            bool showCountOrVal = true;
            FormCollection formCollection = new FormCollection();
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("Branch_Name", SiteInfo.LoggedBranchName.ToString());
            formCollection.Add("AsOfDate", AsOfDate);
            formCollection.Add("WareHouseID", WareHouseID.ToString());
            formCollection.Add("showCountOrValue", showCountOrVal.ToString());
            formCollection.Add("ReportName", "Inventory Count Report");
            Session["parameters"] = formCollection;
            return RedirectToAction("InventoryCountReport", "Reports");
        }
        public ActionResult InventoryValueReport(string AsOfDate, int? WareHouseID)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }
            bool showCountOrVal = false;
            FormCollection formCollection = new FormCollection();
            formCollection.Add("Organization_Name", "Doner Konner");
            formCollection.Add("Logged_Name", SiteInfo.LoggedUserName.ToString());
            formCollection.Add("Branch_Name", SiteInfo.LoggedBranchName.ToString());
            formCollection.Add("AsOfDate", AsOfDate);
            formCollection.Add("WareHouseID", WareHouseID.ToString());
            formCollection.Add("showCountOrValue", showCountOrVal.ToString());
            formCollection.Add("ReportName", "Inventory Value Report");
            Session["parameters"] = formCollection;
            return RedirectToAction("InventoryCountReport", "Reports");
        }
        [HttpPost]
        public ActionResult ExportToExcel(string AsOfDate, int? WareHouseID,bool Flag)
        {
            if (!SiteInfo.IsLogged)
            {
                return RedirectToAction("Login", "Home");
            }
            else if (!GetUserAllTicket(SiteInfo.LoggedUserID))
            {
                return PartialView("_Unauthorized");
            }

            DateTime asofdate;
            DateTime.TryParse(AsOfDate, out asofdate);
            List<InventoryCountReportViewModel> FileData = db.InventoryCountReport(asofdate, WareHouseID).ToList();
            try
            {

                DataTable Dt = new DataTable();
                Dt.Columns.Add("SN", typeof(int));
                Dt.Columns.Add("ItemGroup", typeof(string));
                Dt.Columns.Add("ItemCode", typeof(string));
                Dt.Columns.Add("ItemName", typeof(string));
                Dt.Columns.Add("Unit", typeof(string));
                Dt.Columns.Add("Qty", typeof(int));
                if (Flag)
                {
                    Dt.Columns.Add("Ave. Price", typeof(int));
                    Dt.Columns.Add("Amout", typeof(int));
                }
               
                int count = 1;
                foreach (var data in FileData)
                {
                    DataRow row = Dt.NewRow();
                    row[0] = count;
                    row[1] = data.ItemGroup;
                    row[2] = data.ItemCode;
                    row[3] = data.ItemName;
                    row[4] = data.Unit;
                    row[5] = data.Qty;
                    if (Flag)
                    {
                        row[6] = data.AvePrice;
                        row[7] = data.Amount;
                    }
                    count++;
                    Dt.Rows.Add(row);
                   
                }
                var memoryStream = new MemoryStream();
                using (var excelPackage = new ExcelPackage(memoryStream))
                {
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
                    
                     count += 6;
                     worksheet.Cells["B6"].LoadFromDataTable(Dt, true);
                     worksheet.DefaultRowHeight = 20;
                     worksheet.Column(1).Width = 1.5;

                    #region Header
                    int RowIndex = 0;
                    

                    Image img = Image.FromFile(Server.MapPath("\\Images\\DonerKonner.png"));
                    ExcelPicture pic = worksheet.Drawings.AddPicture("DonerKonner.png", img);
                    var mercell3= worksheet.Cells[""];
                    if (Flag)
                    {
                        int ColIndex = 8;
                        pic.SetPosition(RowIndex, 0, ColIndex, 8);
                        mercell3 = worksheet.Cells["D1:F1"];
                    }
                    else 
                    {
                        int ColIndex = 6;
                        pic.SetPosition(RowIndex, 0, ColIndex, 6);
                        mercell3 = worksheet.Cells["D1:E1"];
                    }
                    
                    pic.SetSize(115, 50);

                    var mercell2 = worksheet.Cells["B1:C1"];
                    mercell2.Merge = true;
                    mercell2.Value = "Donner Konner";
                    mercell2.Style.Font.Size = 13;
                    mercell2.Style.Font.Bold = true;
                    mercell2.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                    
                    mercell3.Merge = true;
                    mercell3.Value = "Branch Name: " + " " + SiteInfo.LoggedBranchName;
                    mercell3.Style.Font.Size = 13;
                    mercell3.Style.Font.Bold = true;
                    mercell3.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells["B2:I2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    worksheet.Cells["B2:I2"].Style.Border.Bottom.Color.SetColor(Color.DarkGray);

                    var mercell = worksheet.Cells["D4:F4"];
                    mercell.Merge = true;
                    mercell.Style.Font.Bold = true;
                    mercell.Style.Font.Size = 15;
                    mercell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    if (Flag)
                    {
                        mercell.Value = "Inventory Value Report";
                    }
                    else
                    {
                        mercell.Value = "Inventory Count Report";
                    }

                    #endregion

                    #region Table
                    worksheet.Cells["B6:I6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                    worksheet.Cells["B6:I6"].Style.Font.Bold = true;
                    worksheet.Cells["B6:I6"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    worksheet.Cells["B6:I6"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(173, 216, 230));
                    worksheet.Cells["B6:I" + count].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:I" + count].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:I" + count].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B6:I" + count].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["B7:I" + count].Style.Border.Top.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:I" + count].Style.Border.Bottom.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:I" + count].Style.Border.Right.Color.SetColor(Color.DarkGray);
                    worksheet.Cells["B7:I" + count].Style.Border.Left.Color.SetColor(Color.DarkGray);

                    worksheet.Column(2).Width = 10;
                    worksheet.Column(5).Width = 13;
                    worksheet.Column(6).Width = 13;
                    worksheet.Column(7).Width = 13;
                    
                    worksheet.Cells["H7:I" + count].Style.Numberformat.Format = "#,###,##";

                    worksheet.Cells["B6:I" + count].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    if (Flag) 
                    {
                        worksheet.Cells["B16:H16"].Merge = true;
                        worksheet.Cells["B16:H16"].Value = "Total";
                        worksheet.Cells["B16:H16"].Style.Font.Size = 11;
                        worksheet.Cells["B16:H16"].Style.Font.Bold = true;
                        worksheet.Cells["B16:H16"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                        worksheet.Cells["I16"].Formula = "=SUM(I7:I15)";
                    }
                    

                    #endregion

                    //#region Footer
                    //Image photo = Image.FromFile(Server.MapPath("\\Images\\agi_logo.png"));
                    //ExcelPicture mg = worksheet.Drawings.AddPicture("agi_logo.png", photo);
                    //mg.SetPosition((count+1),(count+1), 1, 1);
                    //mg.SetSize(70, 30);
                    //worksheet.Cells["C"+(count+2)+":H"+(count+2)].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                    //worksheet.Cells["C" + (count + 2) + ":H" + (count + 2)].Style.Border.Bottom.Color.SetColor(Color.DarkGray);

                    //var cel1 = worksheet.Cells["B"+(count+4)+":C"+(count+4)];
                    //cel1.Merge = true;
                    //cel1.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //cel1.Style.Font.Size = 10;
                    //cel1.Value = "Printed by: " + SiteInfo.LoggedUserName;
                    //cel1.Style.Font.Bold = true;



                    //var cel2 = worksheet.Cells["G" + (count + 4) + ":H" + (count + 4)];
                    //cel2.Merge = true;
                    //cel2.Style.Font.Size = 10;
                    //cel2.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //cel2.Value = "Printed on: " + DateTime.Now;
                    //cel2.Style.Font.Bold = true;
                    //#endregion

                    worksheet.DefaultColWidth = 18;
                    worksheet.Column(3).AutoFit();
                    worksheet.Column(4).AutoFit();
                    if (Flag)
                    {
                        worksheet.Column(8).AutoFit();
                        worksheet.Column(9).AutoFit();
                    }
                    else
                    {
                        worksheet.Column(8).Width = 0;
                        worksheet.Column(9).Width = 0;
                    }


                    Session["DownloadExcel_InventoryCountReport"] = excelPackage.GetAsByteArray();
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
       
        public ActionResult Download()
        {

            if (Session["DownloadExcel_InventoryCountReport"] != null)
            {
                byte[] data = Session["DownloadExcel_InventoryCountReport"] as byte[];

                    return File(data, "application/octet-stream", "InventoryReport.xlsx");
                
            }
            else
            {
                return new EmptyResult();
            }
        }
    }
}