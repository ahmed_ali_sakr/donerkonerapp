﻿using Agi.DonerKonner.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.Models
{
    public class InvOpenningData
    {
        public int Code { get; set; }
        public DateTime Date { get; set; }
        public string Warehouse { get; set; }
        public string InvNotes { get; set; }
        public string Group { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public decimal QTY { get; set; }
        public decimal UnitPrice { get; set; }
        public string ItmNotes { get; set; }
        public string CB { get; set; }


        public static List<InvOpenningData> getInvoiceOpenningData(int TransNo)
        {
            try
            {
                DonerKonnerContext db = new DonerKonnerContext();
                List<InvOpenningData> Match = (from inv in db.InvOpenings
                                               join itm in db.InvOpeningItems on inv.ID equals itm.InvOpeningID
                                               join us in db.Users on inv.CB equals us.ID
                                               where(inv.TransNo== TransNo)
                                               select new InvOpenningData
                                               {
                                                   Code = inv.TransNo,
                                                   Date = inv.TransDate,
                                                   Warehouse = inv.Warehouse.Name,
                                                   InvNotes = inv.Notes,
                                                   Group = itm.Item.ItemGroup.Name,
                                                   ItemCode = itm.Item.Code,
                                                   ItemName = itm.Item.Name,
                                                   UnitPrice = itm.UnitPrice,
                                                   QTY = itm.Qty,
                                                   ItmNotes = itm.Notes,
                                                   CB=us.LoginName
                                               }
                       ).ToList();
                return Match;
            }
            catch (Exception ex)
            {
                return new List<InvOpenningData>();
            }
        }
    }
}