﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Agi.DonerKonner.Models
{
    public class Login
    {
        [Required(ErrorMessage = "UserName is required"), MaxLength(100)]
        [Index("Users_LoginName_UQ", IsUnique = true)]
        public String LoginName { get; set; }

        [Required(ErrorMessage = "Password is required"), MaxLength(100)]
        [DataType(DataType.Password)]
        public String Password { get; set; }
    }
}