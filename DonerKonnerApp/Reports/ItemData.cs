﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Agi.DonerKonner.Reports
{
    public partial class ItemData : DevExpress.XtraReports.UI.XtraReport
    {
        public ItemData()
        {
            InitializeComponent();
        }

        private void rpt_InvOpenning_DataSourceDemanded(object sender, EventArgs e)
        {
            int ItemID = (int)this.Parameters["ItemID"].Value;
            getItemDetailsTableAdapter.Fill(reportDataSet1.getItemDetails, ItemID);
            //int TransNo=(int)this.Parameters["TransNo"].Value;
            // objectDataSource1.DataSource = Models.InvOpenningData.getInvoiceOpenningData(TransNo);
        }
    }
}
