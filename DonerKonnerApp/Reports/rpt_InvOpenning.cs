﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Agi.DonerKonner.Reports
{
    public partial class rpt_InvOpenning : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_InvOpenning()
        {
            InitializeComponent();
            //objectDataSource1.DataSource = Models.InvOpenningData.getInvoiceOpenningData();
            //int TransNo = (int)this.Parameters["TransNo"].Value;
            //objectDataSource1.DataSource = Models.InvOpenningData.getInvoiceOpenningData(TransNo);
        }

        private void rpt_InvOpenning_DataSourceDemanded(object sender, EventArgs e)
        {
            int TransNo=(int)this.Parameters["TransNo"].Value;
            objectDataSource1.DataSource = Models.InvOpenningData.getInvoiceOpenningData(TransNo);
        }
    }
}
