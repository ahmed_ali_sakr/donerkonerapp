﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Agi.DonerKonner.Reports
{
    public partial class InvReceipt : DevExpress.XtraReports.UI.XtraReport
    {
        public InvReceipt()
        {
            InitializeComponent();
        }

        private void rpt_InvOpenning_DataSourceDemanded(object sender, EventArgs e)
        {
            int TransNo = (int)this.Parameters["TransNo"].Value;
            getInvReceiptsDetailsTableAdapter.Fill(reportDataSet1.getInvReceiptsDetails, TransNo);
            //int TransNo=(int)this.Parameters["TransNo"].Value;
            // objectDataSource1.DataSource = Models.InvOpenningData.getInvoiceOpenningData(TransNo);
        }
    }
}
