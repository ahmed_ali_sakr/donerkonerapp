﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class CostViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal PriceNoVat { get; set; }
        public decimal RawMaterial { get; set; }
        public decimal RawMaterialPercent { get; set; }
        public decimal Packing { get; set; }
        public decimal PackingPercent { get; set; }
        public decimal Total { get; set; }
        public decimal TotalPercent { get; set; }
        public decimal Revenue { get; set; }
        public decimal RevenuePercent { get; set; }
    }
}