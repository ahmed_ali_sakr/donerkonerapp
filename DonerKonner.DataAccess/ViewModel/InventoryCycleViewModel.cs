﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class InventoryCycleViewModel
    {
        public string ItemGroup { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int Opening { get; set; }
        public int Recieved { get; set; }
        public int Balance { get; set; }
        public int Used { get; set; }
        public int Sold { get; set; }
        public int Difference { get; set; }
    }
}