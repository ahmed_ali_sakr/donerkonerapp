﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class UserSecurityRoleViewModel
    {
        public int SecurityRoleID { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
    }
}