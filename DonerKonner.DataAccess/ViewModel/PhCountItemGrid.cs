﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class PhCountItemGrid
    {
        public string guid { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public string UnitName { get; set; }
        public decimal Qty { get; set; }
        public string Notes { get; set; }
    }
}