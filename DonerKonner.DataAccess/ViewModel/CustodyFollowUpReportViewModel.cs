﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class CustodyFollowUpReportViewModel
    {
        public DateTime TrnsDate { get; set; }
        public string TansDateString { get { return TrnsDate.ToString("dd/MM/yyyy"); } }
        public string CustodyItem { get; set; }
        public string Description { get; set; }
        public double? PreviousAmount { get; set; }
        public decimal? NewAmount { get; set; }
        public decimal? Payment { get; set; }
        public double? CurrentAmount { get; set; }
        
    }
   
}