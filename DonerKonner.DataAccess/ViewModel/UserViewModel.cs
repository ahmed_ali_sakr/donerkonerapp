﻿using Agi.DonerKonner.DataAccess.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class UserViewModel
    {
        public User user { get; set; }
        public String ConfirmPassword { get; set; }
        public List<UserBranchsViewModel> UserBranchsVM { get; set; }
        public List<UserSecurityRoleViewModel> UserSecurityRolesVM { get; set; }
    }

}