﻿using Agi.DonerKonner.DataAccess.Admin;
using Agi.DonerKonner.DataAccess.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class TransactionData
    {

        public string Code { get; set; }
        public DateTime Date { get; set; }
        public int? WarehouseID { get; set; }
        public int? ItemId { get; set; }
        public int? GroupId { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }

    public class AddTransactionData
    {

        public string Code { get; set; }
        public DateTime Date { get; set; }
        public List<Warehouse> WarehouseID { get; set; }
        public List<Item> ItemId { get; set; }
        public List<ItemGroup>  GroupId { get; set; }
        public int Quantity { get; set; }
        public string Unit { get; set; }
        public double Price { get; set; }
    }

    public class GridData
    {
        public int GroupID { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public string Notes { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
    }

    public class login
    {
        public string username { get; set; }

        public string password { get; set; }

        //public int? BranchID { get; set; }
    }
}