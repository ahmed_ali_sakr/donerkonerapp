﻿using Agi.DonerKonner.DataAccess.Custody;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class Expenses
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string CustodyName { get; set; }
        public int CustodyId { get; set; }
        public string Notes { get; set; }
    }
}