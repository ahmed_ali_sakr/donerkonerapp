﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class UserBranchsViewModel
    {
        public int BranchID { get; set; }
        public String Name { get; set; }
        public bool IsChecked { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
    }
}