﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.ViewModel
{
    public class InventoryCountReportViewModel
    {
        //public DateTime AsOfDate { get; set; }
        public string ItemGroup { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public int Qty { get; set; }
        public int AvePrice { get; set; }
        public int Amount { get; set; }
    }
}