﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.Enum
{
    public enum BuyByEnum { 
        Vender=1,
        Custody=2
    }
    public enum CategoryEnum
    {
        RowMatrial=1,
        PackingMatrial=2,
        Others=3
    }
}