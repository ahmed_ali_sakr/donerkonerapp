﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Agi.DonerKonner.DataAccess.Admin;

namespace Agi.DonerKonner.DataAccess.Custody
{
    public class CustodyExpensesItem
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        #region Custody Expenses
        [Index("CustodyExpensesItems_CustodyExpensesID_UQ", IsUnique = false, Order = 1)]
        public int CustodyExpensesID { get; set; }

        [ForeignKey("CustodyExpensesID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual CustodyExpenses CustodyExpenses { get; set; }
        #endregion

        #region Custody Item
        [Index("CustodyExpensesItems_CustodyExpensesID_UQ", IsUnique = false, Order = 2)]
        public int CustodyItemID { get; set; }

        [ForeignKey("CustodyItemID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual CustodyItem CustodyItem { get; set; }
        #endregion

        public decimal Amount { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }

        #region audit
        public int CB { get; set; }

        [ForeignKey("CB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Creator { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }

        [ForeignKey("MB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Modificator { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion
    }
}