﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Agi.DonerKonner.DataAccess.Admin;
using Agi.DonerKonner.DataAccess.Custody;
using Agi.DonerKonner.DataAccess.Inventory;
using Agi.DonerKonner.DataAccess.ViewModel;

namespace Agi.DonerKonner.DataAccess
{
    public class DonerKonnerContext : DbContext
    {
        #region ServerDate
        public DateTime GetServerDate()
        {
            return this.Database.SqlQuery<DateTime>("SELECT GETDATE()").SingleOrDefault();
        }
        #endregion
        public List<CustodyFollowUpReportViewModel> CustodyFollowupReport(DateTime FromDate ,DateTime ToDate , int BranchID)
        {
            return this.Database.SqlQuery<CustodyFollowUpReportViewModel>("exec CustodyFollowupReport @p0 , @p1 , @p2", FromDate, ToDate, BranchID).ToList();
        }
       

        public List<CostViewModel> CostReportList(DateTime FromDate, DateTime ToDate, int BranchID)
        {
            return this.Database.SqlQuery<CostViewModel>("EXEC GetCostReport @p0 , @p1 , @p2", FromDate, ToDate, BranchID).ToList();
        }

        public List<InventoryCountReportViewModel> InventoryCountReport(DateTime AsOfDate, int? WareHouseID)
        {
            return this.Database.SqlQuery<InventoryCountReportViewModel>("exec InventoryCountReport @p0 , @p1", AsOfDate, WareHouseID).ToList();
        }
        public List<InventoryCycleViewModel> InventoryCycleReport(DateTime FromDate, DateTime ToDate, int BranchID)
        {
            return this.Database.SqlQuery<InventoryCycleViewModel>("exec InventoryCycleReport @p0 , @p1 , @p2", FromDate, ToDate , BranchID).ToList();
        }

        #region Administration
        public DbSet<User> Users { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<UserBranch> UserBranches { get; set; }
        public DbSet<SecurityRole> securityRoles { get; set; }
        public DbSet<SecurityRoleFunction> SecurityRoleFunctions { get; set; }
        public DbSet<SystemFunction> SystemFunctions { get; set; }
        public DbSet<UserSecurityRole> UserSecurityRoles { get; set; }
        

        #endregion

        #region Inventory
        public DbSet<Unit> Units { get; set; }
        public DbSet<ItemGroup> ItemGroups { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemRecipe> ItemRecipes { get; set; }
        public DbSet<ItemComponent> ItemComponents { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<InvOpening> InvOpenings { get; set; }
        public DbSet<InvOpeningItem> InvOpeningItems { get; set; }
        public DbSet<InvReceipt> InvReceipts { get; set; }
        public DbSet<InvReceiptItem> InvReceiptItems { get; set; }
        public DbSet<InvPhysicalCount> InvPhysicalCounts { get; set; }
        public DbSet<InvPhysicalCountItem> invPhysicalCountItems { get; set; }
        public DbSet<InvAdjustment> InvAdjustments { get; set; }
        public DbSet<InvAdjustmentItem> InvAdjustmentItems { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        #endregion

        #region Custody
        public DbSet<CustodyItem> CustodyItems { get; set; }
        public DbSet<CustodyDelivey> CustodyDeliveys { get; set; }
        public DbSet<CustodyExpenses> CustodyExpensess { get; set; }
        public DbSet<CustodyExpensesItem> CustodyExpensesItems { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustodyDelivey>().Property(o => o.Amount).HasPrecision(18, 3);
            modelBuilder.Entity<InvOpeningItem>().Property(o => o.Qty).HasPrecision(18, 3);
            modelBuilder.Entity<InvOpeningItem>().Property(o => o.UnitPrice).HasPrecision(18, 3);
            
            modelBuilder.Entity<SystemFunction>().HasOptional(e => e.DependOn).WithMany().HasForeignKey(o => o.DependOnID);

            base.OnModelCreating(modelBuilder);
        }

    }

}