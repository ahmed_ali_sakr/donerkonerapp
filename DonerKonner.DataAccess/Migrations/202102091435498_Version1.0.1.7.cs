﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1017 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SecurityRoleFunctions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SecurityRoleID = c.Int(nullable: false),
                        SystemFunctionID = c.Int(nullable: false),
                        CB = c.Int(),
                        CD = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SecurityRoles", t => t.SecurityRoleID, cascadeDelete: true)
                .ForeignKey("dbo.SystemFunctions", t => t.SystemFunctionID, cascadeDelete: true)
                .Index(t => new { t.SecurityRoleID, t.SystemFunctionID }, unique: true, name: "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            
            CreateTable(
                "dbo.SecurityRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        IsActive = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Name, unique: true, name: "SecurityRule_Name_UQ");
            
            CreateTable(
                "dbo.SystemFunctions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ParentID = c.Int(),
                        Code = c.String(nullable: false, maxLength: 200),
                        Name = c.String(nullable: false, maxLength: 200),
                        URL = c.String(),
                        SortKey = c.Int(nullable: false),
                        DependOnID = c.Int(),
                        IsHidden = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SystemFunctions", t => t.DependOnID)
                .ForeignKey("dbo.SystemFunctions", t => t.ParentID)
                .Index(t => t.ParentID)
                .Index(t => t.Code, unique: true, name: "SystemFunction_Code_UQ")
                .Index(t => t.Name, unique: true, name: "SystemFunction_Name_UQ")
                .Index(t => t.DependOnID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SystemFunctions", "ParentID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SystemFunctions", "DependOnID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SecurityRoleFunctions", "SecurityRoleID", "dbo.SecurityRoles");
            DropIndex("dbo.SystemFunctions", new[] { "DependOnID" });
            DropIndex("dbo.SystemFunctions", "SystemFunction_Name_UQ");
            DropIndex("dbo.SystemFunctions", "SystemFunction_Code_UQ");
            DropIndex("dbo.SystemFunctions", new[] { "ParentID" });
            DropIndex("dbo.SecurityRoles", "SecurityRule_Name_UQ");
            DropIndex("dbo.SecurityRoleFunctions", "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            DropTable("dbo.SystemFunctions");
            DropTable("dbo.SecurityRoles");
            DropTable("dbo.SecurityRoleFunctions");
        }
    }
}
