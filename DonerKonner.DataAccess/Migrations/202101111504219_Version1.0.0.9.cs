﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1009 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CustodyDeliveys", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InvOpeningItems", "Qty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.InvOpeningItems", "UnitPrice", c => c.Decimal(nullable: false, precision: 18, scale: 3));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InvOpeningItems", "UnitPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.InvOpeningItems", "Qty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.CustodyDeliveys", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
