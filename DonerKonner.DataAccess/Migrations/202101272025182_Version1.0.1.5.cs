﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1015 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.InvPhysicalCounts", "InvPhysicalCounts_BranchID_TransNo_UQ");
            DropIndex("dbo.InvPhysicalCounts", "InvOpenings_BranchID_TransNo_UQ");
            CreateIndex("dbo.InvPhysicalCounts", new[] { "BranchID", "TransNo", "TransDate" }, unique: true, name: "InvPhysicalCounts_BranchID_TransNo_TransDate_UQ");
        }
        
        public override void Down()
        {
            DropIndex("dbo.InvPhysicalCounts", "InvPhysicalCounts_BranchID_TransNo_TransDate_UQ");
            CreateIndex("dbo.InvPhysicalCounts", "TransNo", unique: true, name: "InvOpenings_BranchID_TransNo_UQ");
            CreateIndex("dbo.InvPhysicalCounts", "BranchID", unique: true, name: "InvPhysicalCounts_BranchID_TransNo_UQ");
        }
    }
}
