﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version10110 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.SecurityRoleFunctions", "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            CreateTable(
                "dbo.SystemFunctions",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        ParentID = c.Int(),
                        Code = c.String(nullable: false, maxLength: 200),
                        Name = c.String(nullable: false, maxLength: 200),
                        URL = c.String(),
                        SortKey = c.Int(nullable: false),
                        DependOnID = c.Int(),
                        IsHidden = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SystemFunctions", t => t.DependOnID)
                .ForeignKey("dbo.SystemFunctions", t => t.ParentID)
                .Index(t => t.ParentID)
                .Index(t => t.Code, unique: true, name: "SystemFunction_Code_UQ")
                .Index(t => t.Name, unique: true, name: "SystemFunction_Name_UQ")
                .Index(t => t.DependOnID);
            
            AddColumn("dbo.SecurityRoleFunctions", "SystemFunctionID", c => c.Int(nullable: false));
            CreateIndex("dbo.SecurityRoleFunctions", new[] { "SecurityRoleID", "SystemFunctionID" }, unique: true, name: "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            AddForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions", "ID", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SystemFunctions", "ParentID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SystemFunctions", "DependOnID", "dbo.SystemFunctions");
            DropIndex("dbo.SystemFunctions", new[] { "DependOnID" });
            DropIndex("dbo.SystemFunctions", "SystemFunction_Name_UQ");
            DropIndex("dbo.SystemFunctions", "SystemFunction_Code_UQ");
            DropIndex("dbo.SystemFunctions", new[] { "ParentID" });
            DropIndex("dbo.SecurityRoleFunctions", "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            DropColumn("dbo.SecurityRoleFunctions", "SystemFunctionID");
            DropTable("dbo.SystemFunctions");
            CreateIndex("dbo.SecurityRoleFunctions", "SecurityRoleID", unique: true, name: "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
        }
    }
}
