﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1007 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustodyDeliveys",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.CustodyExpensesItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CustodyExpensesItemID = c.Int(nullable: false),
                        CustodyItemItemID = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.CustodyExpenses", t => t.CustodyExpensesItemID, cascadeDelete: true)
                .ForeignKey("dbo.CustodyItems", t => t.CustodyItemItemID)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.CustodyExpensesItemID, unique: true, name: "CustodyExpensesItems_CustodyExpensesItemID_UQ")
                .Index(t => t.CustodyItemItemID, unique: true, name: "CustodyItemItems_CustodyItemItemID_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.CustodyExpenses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.Date, unique: true, name: "CustodyExpenses_Date_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustodyExpensesItems", "MB", "dbo.Users");
            DropForeignKey("dbo.CustodyExpensesItems", "CustodyItemItemID", "dbo.CustodyItems");
            DropForeignKey("dbo.CustodyExpensesItems", "CustodyExpensesItemID", "dbo.CustodyExpenses");
            DropForeignKey("dbo.CustodyExpenses", "MB", "dbo.Users");
            DropForeignKey("dbo.CustodyExpenses", "CB", "dbo.Users");
            DropForeignKey("dbo.CustodyExpensesItems", "CB", "dbo.Users");
            DropForeignKey("dbo.CustodyDeliveys", "MB", "dbo.Users");
            DropForeignKey("dbo.CustodyDeliveys", "CB", "dbo.Users");
            DropIndex("dbo.CustodyExpenses", new[] { "MB" });
            DropIndex("dbo.CustodyExpenses", new[] { "CB" });
            DropIndex("dbo.CustodyExpenses", "CustodyExpenses_Date_UQ");
            DropIndex("dbo.CustodyExpensesItems", new[] { "MB" });
            DropIndex("dbo.CustodyExpensesItems", new[] { "CB" });
            DropIndex("dbo.CustodyExpensesItems", "CustodyItemItems_CustodyItemItemID_UQ");
            DropIndex("dbo.CustodyExpensesItems", "CustodyExpensesItems_CustodyExpensesItemID_UQ");
            DropIndex("dbo.CustodyDeliveys", new[] { "MB" });
            DropIndex("dbo.CustodyDeliveys", new[] { "CB" });
            DropTable("dbo.CustodyExpenses");
            DropTable("dbo.CustodyExpensesItems");
            DropTable("dbo.CustodyDeliveys");
        }
    }
}
