﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1016 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.InvPhysicalCounts", "InvPhysicalCounts_BranchID_TransNo_TransDate_UQ");
            CreateTable(
                "dbo.InvAdjustmentItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        InvAdjustmentID = c.Int(nullable: false),
                        ItemID = c.Int(nullable: false),
                        UnitID = c.Int(nullable: false),
                        Qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.InvAdjustments", t => t.InvAdjustmentID, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemID)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Units", t => t.UnitID)
                .Index(t => new { t.InvAdjustmentID, t.ItemID }, unique: true, name: "InvAdjustmentItems_InvAdjustmentID_ItemID_UQ")
                .Index(t => t.UnitID)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.InvAdjustments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BranchID = c.Int(nullable: false),
                        WarehouseID = c.Int(nullable: false),
                        TransNo = c.Int(nullable: false),
                        TransDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Branches", t => t.BranchID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Warehouses", t => t.WarehouseID)
                .Index(t => new { t.BranchID, t.TransNo }, unique: true, name: "InvAdjustments_BranchID_TransNo_UQ")
                .Index(t => t.WarehouseID)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateIndex("dbo.InvPhysicalCounts", new[] { "BranchID", "TransDate" }, unique: true, name: "InvPhysicalCounts_BranchID_TransDate_UQ");
            CreateIndex("dbo.InvPhysicalCounts", new[] { "BranchID", "TransNo" }, unique: true, name: "InvPhysicalCounts_BranchID_TransNo_UQ");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvAdjustmentItems", "UnitID", "dbo.Units");
            DropForeignKey("dbo.InvAdjustmentItems", "MB", "dbo.Users");
            DropForeignKey("dbo.InvAdjustmentItems", "ItemID", "dbo.Items");
            DropForeignKey("dbo.InvAdjustmentItems", "InvAdjustmentID", "dbo.InvAdjustments");
            DropForeignKey("dbo.InvAdjustments", "WarehouseID", "dbo.Warehouses");
            DropForeignKey("dbo.InvAdjustments", "MB", "dbo.Users");
            DropForeignKey("dbo.InvAdjustments", "CB", "dbo.Users");
            DropForeignKey("dbo.InvAdjustments", "BranchID", "dbo.Branches");
            DropForeignKey("dbo.InvAdjustmentItems", "CB", "dbo.Users");
            DropIndex("dbo.InvPhysicalCounts", "InvPhysicalCounts_BranchID_TransNo_UQ");
            DropIndex("dbo.InvPhysicalCounts", "InvPhysicalCounts_BranchID_TransDate_UQ");
            DropIndex("dbo.InvAdjustments", new[] { "MB" });
            DropIndex("dbo.InvAdjustments", new[] { "CB" });
            DropIndex("dbo.InvAdjustments", new[] { "WarehouseID" });
            DropIndex("dbo.InvAdjustments", "InvAdjustments_BranchID_TransNo_UQ");
            DropIndex("dbo.InvAdjustmentItems", new[] { "MB" });
            DropIndex("dbo.InvAdjustmentItems", new[] { "CB" });
            DropIndex("dbo.InvAdjustmentItems", new[] { "UnitID" });
            DropIndex("dbo.InvAdjustmentItems", "InvAdjustmentItems_InvAdjustmentID_ItemID_UQ");
            DropTable("dbo.InvAdjustments");
            DropTable("dbo.InvAdjustmentItems");
            CreateIndex("dbo.InvPhysicalCounts", new[] { "BranchID", "TransNo", "TransDate" }, unique: true, name: "InvPhysicalCounts_BranchID_TransNo_TransDate_UQ");
        }
    }
}
