﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1000 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FullName = c.String(nullable: false, maxLength: 200),
                        LoginName = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        CB = c.Int(),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.FullName, unique: true, name: "Users_FullName_UQ")
                .Index(t => t.LoginName, unique: true, name: "Users_LoginName_UQ");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", "Users_LoginName_UQ");
            DropIndex("dbo.Users", "Users_FullName_UQ");
            DropTable("dbo.Users");
        }
    }
}
