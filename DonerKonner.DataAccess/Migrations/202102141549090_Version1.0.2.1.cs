﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1021 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserSecurityRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        SecurityRoleID = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsDefault = c.Boolean(nullable: false),
                        CB = c.Int(),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SecurityRoles", t => t.SecurityRoleID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => new { t.UserID, t.SecurityRoleID }, unique: true, name: "UserSecurityRoles_UserID_SecurityRoleID_UQ");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserSecurityRoles", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserSecurityRoles", "SecurityRoleID", "dbo.SecurityRoles");
            DropIndex("dbo.UserSecurityRoles", "UserSecurityRoles_UserID_SecurityRoleID_UQ");
            DropTable("dbo.UserSecurityRoles");
        }
    }
}
