﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1011 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.CustodyExpensesItems", "CustodyExpensesItems_CustodyExpensesItemID_UQ");
            DropIndex("dbo.CustodyExpensesItems", "CustodyItemItems_CustodyItemItemID_UQ");
            CreateIndex("dbo.CustodyExpensesItems", "CustodyExpensesItemID", name: "CustodyExpensesItems_CustodyExpensesItemID_UQ");
            CreateIndex("dbo.CustodyExpensesItems", "CustodyItemItemID", name: "CustodyItemItems_CustodyItemItemID_UQ");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CustodyExpensesItems", "CustodyItemItems_CustodyItemItemID_UQ");
            DropIndex("dbo.CustodyExpensesItems", "CustodyExpensesItems_CustodyExpensesItemID_UQ");
            CreateIndex("dbo.CustodyExpensesItems", "CustodyItemItemID", unique: true, name: "CustodyItemItems_CustodyItemItemID_UQ");
            CreateIndex("dbo.CustodyExpensesItems", "CustodyExpensesItemID", unique: true, name: "CustodyExpensesItems_CustodyExpensesItemID_UQ");
        }
    }
}
