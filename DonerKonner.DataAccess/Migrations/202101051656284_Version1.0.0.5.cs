﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1005 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InvReceipts", "VendorID", "dbo.Vendors");
            DropIndex("dbo.InvReceipts", new[] { "VendorID" });
            AlterColumn("dbo.InvReceipts", "VendorID", c => c.Int());
            CreateIndex("dbo.InvReceipts", "VendorID");
            AddForeignKey("dbo.InvReceipts", "VendorID", "dbo.Vendors", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvReceipts", "VendorID", "dbo.Vendors");
            DropIndex("dbo.InvReceipts", new[] { "VendorID" });
            AlterColumn("dbo.InvReceipts", "VendorID", c => c.Int(nullable: false));
            CreateIndex("dbo.InvReceipts", "VendorID");
            AddForeignKey("dbo.InvReceipts", "VendorID", "dbo.Vendors", "ID", cascadeDelete: true);
        }
    }
}
