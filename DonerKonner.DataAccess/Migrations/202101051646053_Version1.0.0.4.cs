﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1004 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InvReceiptItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        InvReceiptID = c.Int(nullable: false),
                        ItemID = c.Int(nullable: false),
                        UnitID = c.Int(nullable: false),
                        Qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.InvReceipts", t => t.InvReceiptID, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemID)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Units", t => t.UnitID)
                .Index(t => new { t.InvReceiptID, t.ItemID }, unique: true, name: "InvReceiptItems_InvReceiptID_ItemID_UQ")
                .Index(t => t.UnitID)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.InvReceipts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BranchID = c.Int(nullable: false),
                        WarehouseID = c.Int(nullable: false),
                        BuyByID = c.Int(nullable: false),
                        VendorID = c.Int(nullable: false),
                        TransNo = c.Int(nullable: false),
                        TransDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Branches", t => t.BranchID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Vendors", t => t.VendorID)
                .ForeignKey("dbo.Warehouses", t => t.WarehouseID)
                .Index(t => new { t.BranchID, t.TransNo }, unique: true, name: "InvReceipts_BranchID_TransNo_UQ")
                .Index(t => t.WarehouseID)
                .Index(t => t.VendorID)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.Name, unique: true, name: "Vendors_Name_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvReceiptItems", "UnitID", "dbo.Units");
            DropForeignKey("dbo.InvReceiptItems", "MB", "dbo.Users");
            DropForeignKey("dbo.InvReceiptItems", "ItemID", "dbo.Items");
            DropForeignKey("dbo.InvReceiptItems", "InvReceiptID", "dbo.InvReceipts");
            DropForeignKey("dbo.InvReceipts", "WarehouseID", "dbo.Warehouses");
            DropForeignKey("dbo.InvReceipts", "VendorID", "dbo.Vendors");
            DropForeignKey("dbo.Vendors", "MB", "dbo.Users");
            DropForeignKey("dbo.Vendors", "CB", "dbo.Users");
            DropForeignKey("dbo.InvReceipts", "MB", "dbo.Users");
            DropForeignKey("dbo.InvReceipts", "CB", "dbo.Users");
            DropForeignKey("dbo.InvReceipts", "BranchID", "dbo.Branches");
            DropForeignKey("dbo.InvReceiptItems", "CB", "dbo.Users");
            DropIndex("dbo.Vendors", new[] { "MB" });
            DropIndex("dbo.Vendors", new[] { "CB" });
            DropIndex("dbo.Vendors", "Vendors_Name_UQ");
            DropIndex("dbo.InvReceipts", new[] { "MB" });
            DropIndex("dbo.InvReceipts", new[] { "CB" });
            DropIndex("dbo.InvReceipts", new[] { "VendorID" });
            DropIndex("dbo.InvReceipts", new[] { "WarehouseID" });
            DropIndex("dbo.InvReceipts", "InvReceipts_BranchID_TransNo_UQ");
            DropIndex("dbo.InvReceiptItems", new[] { "MB" });
            DropIndex("dbo.InvReceiptItems", new[] { "CB" });
            DropIndex("dbo.InvReceiptItems", new[] { "UnitID" });
            DropIndex("dbo.InvReceiptItems", "InvReceiptItems_InvReceiptID_ItemID_UQ");
            DropTable("dbo.Vendors");
            DropTable("dbo.InvReceipts");
            DropTable("dbo.InvReceiptItems");
        }
    }
}
