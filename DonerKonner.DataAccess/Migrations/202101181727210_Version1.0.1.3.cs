﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1013 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.CustodyExpensesItems", "CustodyExpensesItems_CustodyExpensesItemID_UQ");
            DropIndex("dbo.CustodyExpensesItems", "CustodyItemItems_CustodyItemItemID_UQ");
            DropIndex("dbo.CustodyExpenses", "CustodyExpenses_Date_UQ");
            RenameColumn(table: "dbo.CustodyExpensesItems", name: "CustodyExpensesItemID", newName: "CustodyExpensesID");
            RenameColumn(table: "dbo.CustodyExpensesItems", name: "CustodyItemItemID", newName: "CustodyItemID");
            AddColumn("dbo.CustodyDeliveys", "BranchID", c => c.Int(nullable: false));
            AddColumn("dbo.CustodyExpenses", "BranchID", c => c.Int(nullable: false));
            CreateIndex("dbo.CustodyDeliveys", "BranchID");
            CreateIndex("dbo.CustodyExpensesItems", new[] { "CustodyExpensesID", "CustodyItemID" }, name: "CustodyExpensesItems_CustodyExpensesID_UQ");
            CreateIndex("dbo.CustodyExpenses", new[] { "BranchID", "Date" }, unique: true, name: "CustodyExpenses_BranchID_Date_UQ");
            AddForeignKey("dbo.CustodyDeliveys", "BranchID", "dbo.Branches", "ID");
            AddForeignKey("dbo.CustodyExpenses", "BranchID", "dbo.Branches", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustodyExpenses", "BranchID", "dbo.Branches");
            DropForeignKey("dbo.CustodyDeliveys", "BranchID", "dbo.Branches");
            DropIndex("dbo.CustodyExpenses", "CustodyExpenses_BranchID_Date_UQ");
            DropIndex("dbo.CustodyExpensesItems", "CustodyExpensesItems_CustodyExpensesID_UQ");
            DropIndex("dbo.CustodyDeliveys", new[] { "BranchID" });
            DropColumn("dbo.CustodyExpenses", "BranchID");
            DropColumn("dbo.CustodyDeliveys", "BranchID");
            RenameColumn(table: "dbo.CustodyExpensesItems", name: "CustodyItemID", newName: "CustodyItemItemID");
            RenameColumn(table: "dbo.CustodyExpensesItems", name: "CustodyExpensesID", newName: "CustodyExpensesItemID");
            CreateIndex("dbo.CustodyExpenses", "Date", unique: true, name: "CustodyExpenses_Date_UQ");
            CreateIndex("dbo.CustodyExpensesItems", "CustodyItemItemID", name: "CustodyItemItems_CustodyItemItemID_UQ");
            CreateIndex("dbo.CustodyExpensesItems", "CustodyExpensesItemID", name: "CustodyExpensesItems_CustodyExpensesItemID_UQ");
        }
    }
}
