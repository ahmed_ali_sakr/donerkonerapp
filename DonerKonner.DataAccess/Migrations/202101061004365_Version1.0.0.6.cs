﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1006 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustodyItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.Name, unique: true, name: "CustodyItems_Name_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustodyItems", "MB", "dbo.Users");
            DropForeignKey("dbo.CustodyItems", "CB", "dbo.Users");
            DropIndex("dbo.CustodyItems", new[] { "MB" });
            DropIndex("dbo.CustodyItems", new[] { "CB" });
            DropIndex("dbo.CustodyItems", "CustodyItems_Name_UQ");
            DropTable("dbo.CustodyItems");
        }
    }
}
