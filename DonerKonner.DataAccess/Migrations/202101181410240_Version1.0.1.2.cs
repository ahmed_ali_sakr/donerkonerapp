﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1012 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "CategoryItemId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "CategoryItemId");
        }
    }
}
