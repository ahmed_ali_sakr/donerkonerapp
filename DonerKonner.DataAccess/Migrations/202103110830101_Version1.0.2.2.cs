﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1022 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserBranches", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserBranches", "IsActive");
        }
    }
}
