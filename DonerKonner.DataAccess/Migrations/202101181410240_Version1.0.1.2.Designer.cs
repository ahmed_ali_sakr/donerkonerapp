﻿// <auto-generated />
namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class Version1012 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Version1012));
        
        string IMigrationMetadata.Id
        {
            get { return "202101181410240_Version1.0.1.2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
