﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SystemFunctions", "DependOnID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SystemFunctions", "ParentID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions");
            DropPrimaryKey("dbo.SystemFunctions");
            AlterColumn("dbo.SystemFunctions", "ID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.SystemFunctions", "ID");
            AddForeignKey("dbo.SystemFunctions", "DependOnID", "dbo.SystemFunctions", "ID");
            AddForeignKey("dbo.SystemFunctions", "ParentID", "dbo.SystemFunctions", "ID");
            AddForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SystemFunctions", "ParentID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SystemFunctions", "DependOnID", "dbo.SystemFunctions");
            DropPrimaryKey("dbo.SystemFunctions");
            AlterColumn("dbo.SystemFunctions", "ID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.SystemFunctions", "ID");
            AddForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SystemFunctions", "ParentID", "dbo.SystemFunctions", "ID");
            AddForeignKey("dbo.SystemFunctions", "DependOnID", "dbo.SystemFunctions", "ID");
        }
    }
}
