﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1019 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SystemFunctions", "DependOnID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SystemFunctions", "ParentID", "dbo.SystemFunctions");
            DropForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions");
            DropIndex("dbo.SecurityRoleFunctions", "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            DropIndex("dbo.SystemFunctions", new[] { "ParentID" });
            DropIndex("dbo.SystemFunctions", "SystemFunction_Code_UQ");
            DropIndex("dbo.SystemFunctions", "SystemFunction_Name_UQ");
            DropIndex("dbo.SystemFunctions", new[] { "DependOnID" });
            CreateIndex("dbo.SecurityRoleFunctions", "SecurityRoleID", unique: true, name: "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            DropColumn("dbo.SecurityRoleFunctions", "SystemFunctionID");
            DropTable("dbo.SystemFunctions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SystemFunctions",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        ParentID = c.Int(),
                        Code = c.String(nullable: false, maxLength: 200),
                        Name = c.String(nullable: false, maxLength: 200),
                        URL = c.String(),
                        SortKey = c.Int(nullable: false),
                        DependOnID = c.Int(),
                        IsHidden = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.SecurityRoleFunctions", "SystemFunctionID", c => c.Int(nullable: false));
            DropIndex("dbo.SecurityRoleFunctions", "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            CreateIndex("dbo.SystemFunctions", "DependOnID");
            CreateIndex("dbo.SystemFunctions", "Name", unique: true, name: "SystemFunction_Name_UQ");
            CreateIndex("dbo.SystemFunctions", "Code", unique: true, name: "SystemFunction_Code_UQ");
            CreateIndex("dbo.SystemFunctions", "ParentID");
            CreateIndex("dbo.SecurityRoleFunctions", new[] { "SecurityRoleID", "SystemFunctionID" }, unique: true, name: "SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ");
            AddForeignKey("dbo.SecurityRoleFunctions", "SystemFunctionID", "dbo.SystemFunctions", "ID", cascadeDelete: true);
            AddForeignKey("dbo.SystemFunctions", "ParentID", "dbo.SystemFunctions", "ID");
            AddForeignKey("dbo.SystemFunctions", "DependOnID", "dbo.SystemFunctions", "ID");
        }
    }
}
