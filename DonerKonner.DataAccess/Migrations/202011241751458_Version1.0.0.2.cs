﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1002 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.Name, unique: true, name: "Users_Name_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.ItemGroups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 200),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.Code, unique: true, name: "ItemGroups_Code_UQ")
                .Index(t => t.Name, unique: true, name: "ItemGroups_Name_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 200),
                        UnitID = c.Int(nullable: false),
                        ItemGroupID = c.Int(nullable: false),
                        HasStock = c.Boolean(nullable: false),
                        IsPOSItem = c.Boolean(nullable: false),
                        IsBundle = c.Boolean(nullable: false),
                        IsDummy = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupID)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Units", t => t.UnitID)
                .Index(t => t.Code, unique: true, name: "Items_Code_UQ")
                .Index(t => t.Name, unique: true, name: "Items_Name_UQ")
                .Index(t => t.UnitID)
                .Index(t => t.ItemGroupID)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.Name, unique: true, name: "Units_Name_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.UserBranches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        BranchID = c.Int(nullable: false),
                        IsDefault = c.Boolean(nullable: false),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Branches", t => t.BranchID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => new { t.UserID, t.BranchID }, unique: true, name: "UserBranches_UserID_BranchID_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.Warehouses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BranchID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Branches", t => t.BranchID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => t.BranchID)
                .Index(t => t.Name, unique: true, name: "Warehouses_Name_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            AddColumn("dbo.Users", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Warehouses", "MB", "dbo.Users");
            DropForeignKey("dbo.Warehouses", "CB", "dbo.Users");
            DropForeignKey("dbo.Warehouses", "BranchID", "dbo.Branches");
            DropForeignKey("dbo.UserBranches", "UserID", "dbo.Users");
            DropForeignKey("dbo.UserBranches", "MB", "dbo.Users");
            DropForeignKey("dbo.UserBranches", "CB", "dbo.Users");
            DropForeignKey("dbo.UserBranches", "BranchID", "dbo.Branches");
            DropForeignKey("dbo.Items", "UnitID", "dbo.Units");
            DropForeignKey("dbo.Units", "MB", "dbo.Users");
            DropForeignKey("dbo.Units", "CB", "dbo.Users");
            DropForeignKey("dbo.Items", "MB", "dbo.Users");
            DropForeignKey("dbo.Items", "ItemGroupID", "dbo.ItemGroups");
            DropForeignKey("dbo.Items", "CB", "dbo.Users");
            DropForeignKey("dbo.ItemGroups", "MB", "dbo.Users");
            DropForeignKey("dbo.ItemGroups", "CB", "dbo.Users");
            DropForeignKey("dbo.Branches", "MB", "dbo.Users");
            DropForeignKey("dbo.Branches", "CB", "dbo.Users");
            DropIndex("dbo.Warehouses", new[] { "MB" });
            DropIndex("dbo.Warehouses", new[] { "CB" });
            DropIndex("dbo.Warehouses", "Warehouses_Name_UQ");
            DropIndex("dbo.Warehouses", new[] { "BranchID" });
            DropIndex("dbo.UserBranches", new[] { "MB" });
            DropIndex("dbo.UserBranches", new[] { "CB" });
            DropIndex("dbo.UserBranches", "UserBranches_UserID_BranchID_UQ");
            DropIndex("dbo.Units", new[] { "MB" });
            DropIndex("dbo.Units", new[] { "CB" });
            DropIndex("dbo.Units", "Units_Name_UQ");
            DropIndex("dbo.Items", new[] { "MB" });
            DropIndex("dbo.Items", new[] { "CB" });
            DropIndex("dbo.Items", new[] { "ItemGroupID" });
            DropIndex("dbo.Items", new[] { "UnitID" });
            DropIndex("dbo.Items", "Items_Name_UQ");
            DropIndex("dbo.Items", "Items_Code_UQ");
            DropIndex("dbo.ItemGroups", new[] { "MB" });
            DropIndex("dbo.ItemGroups", new[] { "CB" });
            DropIndex("dbo.ItemGroups", "ItemGroups_Name_UQ");
            DropIndex("dbo.ItemGroups", "ItemGroups_Code_UQ");
            DropIndex("dbo.Branches", new[] { "MB" });
            DropIndex("dbo.Branches", new[] { "CB" });
            DropIndex("dbo.Branches", "Users_Name_UQ");
            DropColumn("dbo.Users", "IsActive");
            DropTable("dbo.Warehouses");
            DropTable("dbo.UserBranches");
            DropTable("dbo.Units");
            DropTable("dbo.Items");
            DropTable("dbo.ItemGroups");
            DropTable("dbo.Branches");
        }
    }
}
