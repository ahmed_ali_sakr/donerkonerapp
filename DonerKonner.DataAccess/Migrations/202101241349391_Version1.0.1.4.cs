﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1014 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InvPhysicalCountItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        InvPhysicalCountID = c.Int(nullable: false),
                        ItemID = c.Int(nullable: false),
                        UnitID = c.Int(nullable: false),
                        Qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.InvPhysicalCounts", t => t.InvPhysicalCountID, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemID)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Units", t => t.UnitID)
                .Index(t => new { t.InvPhysicalCountID, t.ItemID }, unique: true, name: "InvPhysicalCountItems_InvPhysicalCountID_ItemID_UQ")
                .Index(t => t.UnitID)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.InvPhysicalCounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BranchID = c.Int(nullable: false),
                        WarehouseID = c.Int(nullable: false),
                        TransNo = c.Int(nullable: false),
                        TransDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Branches", t => t.BranchID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Warehouses", t => t.WarehouseID)
                .Index(t => t.BranchID, unique: true, name: "InvPhysicalCounts_BranchID_TransNo_UQ")
                .Index(t => t.WarehouseID)
                .Index(t => t.TransNo, unique: true, name: "InvOpenings_BranchID_TransNo_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InvPhysicalCountItems", "UnitID", "dbo.Units");
            DropForeignKey("dbo.InvPhysicalCountItems", "MB", "dbo.Users");
            DropForeignKey("dbo.InvPhysicalCountItems", "ItemID", "dbo.Items");
            DropForeignKey("dbo.InvPhysicalCountItems", "InvPhysicalCountID", "dbo.InvPhysicalCounts");
            DropForeignKey("dbo.InvPhysicalCounts", "WarehouseID", "dbo.Warehouses");
            DropForeignKey("dbo.InvPhysicalCounts", "MB", "dbo.Users");
            DropForeignKey("dbo.InvPhysicalCounts", "CB", "dbo.Users");
            DropForeignKey("dbo.InvPhysicalCounts", "BranchID", "dbo.Branches");
            DropForeignKey("dbo.InvPhysicalCountItems", "CB", "dbo.Users");
            DropIndex("dbo.InvPhysicalCounts", new[] { "MB" });
            DropIndex("dbo.InvPhysicalCounts", new[] { "CB" });
            DropIndex("dbo.InvPhysicalCounts", "InvOpenings_BranchID_TransNo_UQ");
            DropIndex("dbo.InvPhysicalCounts", new[] { "WarehouseID" });
            DropIndex("dbo.InvPhysicalCounts", "InvPhysicalCounts_BranchID_TransNo_UQ");
            DropIndex("dbo.InvPhysicalCountItems", new[] { "MB" });
            DropIndex("dbo.InvPhysicalCountItems", new[] { "CB" });
            DropIndex("dbo.InvPhysicalCountItems", new[] { "UnitID" });
            DropIndex("dbo.InvPhysicalCountItems", "InvPhysicalCountItems_InvPhysicalCountID_ItemID_UQ");
            DropTable("dbo.InvPhysicalCounts");
            DropTable("dbo.InvPhysicalCountItems");
        }
    }
}
