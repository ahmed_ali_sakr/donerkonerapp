﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1001 : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT USERS ON");

            Sql("INSERT INTO Users(ID , FullName , LoginName , Password , CB , CD ) VALUES(1,'System Administrator' , 'admin' , 'admin' , 1 , GetDate())");

            Sql("SET IDENTITY_INSERT USERS OFF");

            Sql("ALTER TABLE Users ADD CONSTRAINT Users_CB_FK FOREIGN KEY(CB) REFERENCES Users(ID)");

            Sql("ALTER TABLE Users ADD CONSTRAINT Users_MB_FK FOREIGN KEY(MB) REFERENCES Users(ID)");

        }

        public override void Down()
        {
        }
    }
}
