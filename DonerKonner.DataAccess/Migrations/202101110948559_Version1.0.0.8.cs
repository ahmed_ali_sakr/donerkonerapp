﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1008 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvReceipts", "Vat", c => c.Int(nullable: false));
            AddColumn("dbo.InvReceipts", "VatValue", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvReceipts", "VatValue");
            DropColumn("dbo.InvReceipts", "Vat");
        }
    }
}
