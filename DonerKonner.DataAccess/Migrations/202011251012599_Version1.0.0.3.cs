﻿namespace Agi.DonerKonner.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Version1003 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InvOpeningItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        InvOpeningID = c.Int(nullable: false),
                        ItemID = c.Int(nullable: false),
                        UnitID = c.Int(nullable: false),
                        Qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.InvOpenings", t => t.InvOpeningID, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.ItemID)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Units", t => t.UnitID)
                .Index(t => new { t.InvOpeningID, t.ItemID }, unique: true, name: "InvOpeningItems_InvOpeningID_ItemID_UQ")
                .Index(t => t.UnitID)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.InvOpenings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BranchID = c.Int(nullable: false),
                        WarehouseID = c.Int(nullable: false),
                        TransNo = c.Int(nullable: false),
                        TransDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Branches", t => t.BranchID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Warehouses", t => t.WarehouseID)
                .Index(t => new { t.BranchID, t.TransNo }, unique: true, name: "InvOpenings_BranchID_TransNo_UQ")
                .Index(t => t.WarehouseID)
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.ItemComponents",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ItemID = c.Int(nullable: false),
                        ComponentID = c.Int(nullable: false),
                        Qty = c.Int(nullable: false),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Items", t => t.ComponentID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Items", t => t.ItemID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.MB)
                .Index(t => new { t.ItemID, t.ComponentID }, unique: true, name: "ItemComponents_ItemID_ComponentID_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            CreateTable(
                "dbo.ItemRecipes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ItemID = c.Int(nullable: false),
                        RecipeID = c.Int(nullable: false),
                        Qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Notes = c.String(maxLength: 4000),
                        CB = c.Int(nullable: false),
                        CD = c.DateTime(nullable: false),
                        MB = c.Int(),
                        MD = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.CB)
                .ForeignKey("dbo.Items", t => t.ItemID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.MB)
                .ForeignKey("dbo.Items", t => t.RecipeID)
                .Index(t => new { t.ItemID, t.RecipeID }, unique: true, name: "ItemRecipes_ItemID_RecipeID_UQ")
                .Index(t => t.CB)
                .Index(t => t.MB);
            
            AddColumn("dbo.Branches", "Notes", c => c.String(maxLength: 4000));
            AddColumn("dbo.Users", "Notes", c => c.String(maxLength: 4000));
            AddColumn("dbo.ItemGroups", "Notes", c => c.String(maxLength: 4000));
            AddColumn("dbo.Items", "Notes", c => c.String(maxLength: 4000));
            AddColumn("dbo.Units", "Notes", c => c.String(maxLength: 4000));
            AddColumn("dbo.UserBranches", "Notes", c => c.String(maxLength: 4000));
            AddColumn("dbo.Warehouses", "Notes", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ItemRecipes", "RecipeID", "dbo.Items");
            DropForeignKey("dbo.ItemRecipes", "MB", "dbo.Users");
            DropForeignKey("dbo.ItemRecipes", "ItemID", "dbo.Items");
            DropForeignKey("dbo.ItemRecipes", "CB", "dbo.Users");
            DropForeignKey("dbo.ItemComponents", "MB", "dbo.Users");
            DropForeignKey("dbo.ItemComponents", "ItemID", "dbo.Items");
            DropForeignKey("dbo.ItemComponents", "CB", "dbo.Users");
            DropForeignKey("dbo.ItemComponents", "ComponentID", "dbo.Items");
            DropForeignKey("dbo.InvOpeningItems", "UnitID", "dbo.Units");
            DropForeignKey("dbo.InvOpeningItems", "MB", "dbo.Users");
            DropForeignKey("dbo.InvOpeningItems", "ItemID", "dbo.Items");
            DropForeignKey("dbo.InvOpeningItems", "InvOpeningID", "dbo.InvOpenings");
            DropForeignKey("dbo.InvOpenings", "WarehouseID", "dbo.Warehouses");
            DropForeignKey("dbo.InvOpenings", "MB", "dbo.Users");
            DropForeignKey("dbo.InvOpenings", "CB", "dbo.Users");
            DropForeignKey("dbo.InvOpenings", "BranchID", "dbo.Branches");
            DropForeignKey("dbo.InvOpeningItems", "CB", "dbo.Users");
            DropIndex("dbo.ItemRecipes", new[] { "MB" });
            DropIndex("dbo.ItemRecipes", new[] { "CB" });
            DropIndex("dbo.ItemRecipes", "ItemRecipes_ItemID_RecipeID_UQ");
            DropIndex("dbo.ItemComponents", new[] { "MB" });
            DropIndex("dbo.ItemComponents", new[] { "CB" });
            DropIndex("dbo.ItemComponents", "ItemComponents_ItemID_ComponentID_UQ");
            DropIndex("dbo.InvOpenings", new[] { "MB" });
            DropIndex("dbo.InvOpenings", new[] { "CB" });
            DropIndex("dbo.InvOpenings", new[] { "WarehouseID" });
            DropIndex("dbo.InvOpenings", "InvOpenings_BranchID_TransNo_UQ");
            DropIndex("dbo.InvOpeningItems", new[] { "MB" });
            DropIndex("dbo.InvOpeningItems", new[] { "CB" });
            DropIndex("dbo.InvOpeningItems", new[] { "UnitID" });
            DropIndex("dbo.InvOpeningItems", "InvOpeningItems_InvOpeningID_ItemID_UQ");
            DropColumn("dbo.Warehouses", "Notes");
            DropColumn("dbo.UserBranches", "Notes");
            DropColumn("dbo.Units", "Notes");
            DropColumn("dbo.Items", "Notes");
            DropColumn("dbo.ItemGroups", "Notes");
            DropColumn("dbo.Users", "Notes");
            DropColumn("dbo.Branches", "Notes");
            DropTable("dbo.ItemRecipes");
            DropTable("dbo.ItemComponents");
            DropTable("dbo.InvOpenings");
            DropTable("dbo.InvOpeningItems");
        }
    }
}
