﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Agi.DonerKonner.DataAccess.Admin;

namespace Agi.DonerKonner.DataAccess.Inventory
{
    public class Item
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required, MaxLength(50)]
        [Index("Items_Code_UQ", IsUnique = true)]
        public String Code { get; set; }

        [Required, MaxLength(200)]
        [Index("Items_Name_UQ", IsUnique = true)]
        public String Name { get; set; }
        public int CategoryItemId { get; set; }

        #region Unit
        public int UnitID { get; set; }

        [ForeignKey("UnitID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Unit Unit { get; set; }
        #endregion


        #region Item Group
        public int ItemGroupID { get; set; }

        [ForeignKey("ItemGroupID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual ItemGroup ItemGroup { get; set; }
        #endregion

        public bool HasStock { get; set; }
        public bool IsPOSItem { get; set; }
        public bool IsBundle { get; set; }
        public bool IsDummy { get; set; }
        public bool IsActive { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }
        #region audit
        public int CB { get; set; }

        [ForeignKey("CB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Creator { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }

        [ForeignKey("MB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Modificator { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion
    }
}