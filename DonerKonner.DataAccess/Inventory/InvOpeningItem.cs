﻿ using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Agi.DonerKonner.DataAccess.Admin;

namespace Agi.DonerKonner.DataAccess.Inventory
{
    public class InvOpeningItem
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        #region Inventory Opening
        [Index("InvOpeningItems_InvOpeningID_ItemID_UQ", IsUnique = true, Order = 1)]
        public int InvOpeningID { get; set; }

        [ForeignKey("InvOpeningID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual InvOpening InvOpening { get; set; }
        #endregion

        #region Item
        [Index("InvOpeningItems_InvOpeningID_ItemID_UQ", IsUnique = true, Order = 2)]
        public int ItemID { get; set; }

        [ForeignKey("ItemID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Item Item { get; set; }
        #endregion

        #region Unit
        public int UnitID { get; set; }

        [ForeignKey("UnitID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Unit Unit { get; set; }
        #endregion

        public decimal Qty { get; set; }

        public decimal UnitPrice { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }

        #region audit
        public int CB { get; set; }

        [ForeignKey("CB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Creator { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }

        [ForeignKey("MB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Modificator { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion
    }
}