﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Agi.DonerKonner.DataAccess.Admin;
namespace Agi.DonerKonner.DataAccess.Inventory
{
    public class InvPhysicalCount
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        #region Branch
        [Index("InvPhysicalCounts_BranchID_TransNo_UQ", IsUnique = true, Order = 1)]
        [Index("InvPhysicalCounts_BranchID_TransDate_UQ", IsUnique = true, Order = 1)]
        public int BranchID  { get; set; }

        [ForeignKey("BranchID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Branch Branch { get; set; }
        #endregion

        #region Warehouse
        public int WarehouseID { get; set; }

        [ForeignKey("WarehouseID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Warehouse Warehouse { get; set; }
        #endregion

        [Index("InvPhysicalCounts_BranchID_TransNo_UQ", IsUnique = true, Order = 2)]
        public int TransNo { get; set; }

        //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Index("InvPhysicalCounts_BranchID_TransDate_UQ", IsUnique = true, Order = 2)]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime TransDate { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }

        #region audit
        public int CB { get; set; }

        [ForeignKey("CB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Creator { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }

        [ForeignKey("MB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Modificator { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion
    }
}