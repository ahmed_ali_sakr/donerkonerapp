﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Agi.DonerKonner.DataAccess.Admin;
using System.ComponentModel;

namespace Agi.DonerKonner.DataAccess.Inventory
{
    public class Warehouse
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        #region Branch
        public int BranchID { get; set; }

        [ForeignKey("BranchID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Branch Branch { get; set; }
        #endregion

        [Required, MaxLength(200)]
        [Index("Warehouses_Name_UQ", IsUnique = true)]
        [DisplayName("WareHouse")]
        public String Name { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }

        #region audit
        public int CB { get; set; }

        [ForeignKey("CB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Creator { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }

        [ForeignKey("MB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Modificator { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion
    }
}