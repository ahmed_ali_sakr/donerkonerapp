﻿using Agi.DonerKonner.DataAccess.Admin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Agi.DonerKonner.DataAccess.Inventory
{
    public class InvReceipt
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        #region Branch
        [Index("InvReceipts_BranchID_TransNo_UQ", IsUnique = true, Order = 1)]
        public int BranchID { get; set; }

        [ForeignKey("BranchID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Branch Branch { get; set; }
        #endregion

        #region Warehouse
        public int WarehouseID { get; set; }

        [ForeignKey("WarehouseID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Warehouse Warehouse { get; set; }
        #endregion

        public int BuyByID { get; set; }

        #region BuyBy
        public int? VendorID { get; set; }

        [ForeignKey("VendorID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Vendor Vendor { get; set; }
        #endregion

        [Index("InvReceipts_BranchID_TransNo_UQ", IsUnique = true, Order = 2)]

        public int TransNo { get; set; }

        //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public DateTime TransDate { get; set; }
        public int Vat { get; set; }
        public int VatValue { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }

        #region audit
        public int CB { get; set; }

        [ForeignKey("CB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Creator { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }

        [ForeignKey("MB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Modificator { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion
    }
}