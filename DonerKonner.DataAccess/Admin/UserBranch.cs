﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;

namespace Agi.DonerKonner.DataAccess.Admin
{
    public class UserBranch
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        #region User
        [Index("UserBranches_UserID_BranchID_UQ", IsUnique = true , Order = 1)]
        public int UserID { get; set; }

        [ForeignKey("UserID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User User { get; set; }
        #endregion

        #region Branch
        [Index("UserBranches_UserID_BranchID_UQ", IsUnique = true, Order = 2)]
        public int BranchID { get; set; }

        [ForeignKey("BranchID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual Branch Branch { get; set; }
        #endregion
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }

        #region audit
        public int CB { get; set; }

        [ForeignKey("CB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Creator { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }

        [ForeignKey("MB"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User Modificator { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion

    }
}
