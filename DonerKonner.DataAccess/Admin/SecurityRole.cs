﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Agi.DonerKonner.DataAccess.Admin
{
    [Table("SecurityRoles")]
    public class SecurityRole
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required, MaxLength(200)]
        [Index("SecurityRule_Name_UQ", IsUnique = true)]
        public string Name { get; set; }
        public bool IsActive { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }

        #region audit
        public int? CB { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion

    }
}