﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Agi.DonerKonner.DataAccess.Admin
{
    [Table("SystemFunctions")]
    public class SystemFunction
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int? ParentID { get; set; }

        [ForeignKey("ParentID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual SystemFunction Parent { get; set; }

        [Required, MaxLength(200)]
        [Index("SystemFunction_Code_UQ", IsUnique = true)]
        public string Code { get; set; }

        [Required, MaxLength(200)]
        [Index("SystemFunction_Name_UQ", IsUnique = true)]
        public string Name { get; set; }

        public string URL { get; set; }

        public int SortKey { get; set; }

        public int? DependOnID { get; set; }

        //[ForeignKey("DependOnID"), ScriptIgnore(ApplyToOverrides = true)]
        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual SystemFunction DependOn { get; set; }

        [DefaultValue(false)]
        public bool IsHidden { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }
    }
}