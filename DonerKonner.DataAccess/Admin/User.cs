﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Agi.DonerKonner.DataAccess.Admin
{
    public class User
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required, MaxLength(200)]
        [Index("Users_FullName_UQ", IsUnique = true)]
        public String FullName { get; set; }

        [Required(ErrorMessage = "UserName is required"), MaxLength(100)]
        [Index("Users_LoginName_UQ", IsUnique = true)]
        public String LoginName { get; set; }

        [Required(ErrorMessage = "Password is required"), MaxLength(100)]
        [DataType(DataType.Password)]
        public String Password { get; set; }
        public bool IsActive { get; set; }

        [MaxLength(4000)]
        public string Notes { get; set; }

        #region audit
        public int? CB { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion
    }
}
