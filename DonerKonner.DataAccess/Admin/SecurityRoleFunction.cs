﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Agi.DonerKonner.DataAccess.Admin
{
    [Table("SecurityRoleFunctions")]
    public class SecurityRoleFunction
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [Index("SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ", IsUnique = true, Order = 1)]
        public int SecurityRoleID { get; set; }

        [ForeignKey("SecurityRoleID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual SecurityRole SecurityRole { get; set; }

        [Required]
        [Index("SecurityRoleFunctions_SecurityRoleID_SystemFunctionID_UQ", IsUnique = true, Order = 2)]
        public int SystemFunctionID { get; set; }

        [ForeignKey("SystemFunctionID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual SystemFunction SystemFunction { get; set; }

        #region audit
        public int? CB { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }
        #endregion

    }
}