﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Agi.DonerKonner.DataAccess.Admin
{
    public class UserSecurityRole
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        #region User
        [Required, Index("UserSecurityRoles_UserID_SecurityRoleID_UQ", IsUnique = true)]
        public int UserID { get; set; }
        [ForeignKey("UserID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual User User { get; set; }
        #endregion

        #region SecurityRole
        [Required, Index("UserSecurityRoles_UserID_SecurityRoleID_UQ", IsUnique = true, Order = 1)]
        public int SecurityRoleID { get; set; }
        [ForeignKey("SecurityRoleID"), ScriptIgnore(ApplyToOverrides = true)]
        public virtual SecurityRole SecurityRole { get; set; }
        #endregion

        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }

        #region audit
        public int? CB { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime CD { get; set; }

        public int? MB { get; set; }


        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}")]
        public DateTime? MD { get; set; }
        #endregion

    }
}